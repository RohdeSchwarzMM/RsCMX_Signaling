Bwp<BwParts>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.add.signaling.nradio.cell.bwp.repcap_bwParts_get()
	driver.add.signaling.nradio.cell.bwp.repcap_bwParts_set(repcap.BwParts.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:BWP

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:BWP



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Bwp.BwpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.signaling.nradio.cell.bwp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_Signaling_Nradio_Cell_Bwp_Csi.rst
	Add_Signaling_Nradio_Cell_Bwp_Harq.rst
	Add_Signaling_Nradio_Cell_Bwp_Srs.rst