Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:FGS:UECapability:MRDC:BANDs

.. code-block:: python

	ADD:SIGNaling:FGS:UECapability:MRDC:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Fgs.UeCapability.Mrdc.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: