Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:NSSB:OFFSet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:NSSB:OFFSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Nssb.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: