Mrdc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:EPS:UECapability:MRDC:ENRonly

.. code-block:: python

	[CONFigure]:SIGNaling:EPS:UECapability:MRDC:ENRonly



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Eps.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex: