Ncandidates
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:NCANdidates

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:NCANdidates



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ibwp.Coreset.Ncandidates.NcandidatesCls
	:members:
	:undoc-members:
	:noindex: