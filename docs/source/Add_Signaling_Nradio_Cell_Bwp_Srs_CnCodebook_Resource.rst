Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:SRS:CNCodebook:RESource

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:SRS:CNCodebook:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Bwp.Srs.CnCodebook.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: