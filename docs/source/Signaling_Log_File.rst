File
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Log.File.FileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.log.file.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Log_File_Latest.rst
	Signaling_Log_File_State.rst