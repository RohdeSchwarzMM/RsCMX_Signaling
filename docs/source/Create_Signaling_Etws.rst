Etws
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CREate:SIGNaling:ETWS:SECondary
	single: CREate:SIGNaling:ETWS

.. code-block:: python

	CREate:SIGNaling:ETWS:SECondary
	CREate:SIGNaling:ETWS



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Etws.EtwsCls
	:members:
	:undoc-members:
	:noindex: