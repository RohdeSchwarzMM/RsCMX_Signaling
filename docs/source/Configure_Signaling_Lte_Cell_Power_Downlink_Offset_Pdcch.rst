Pdcch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:PDCCh

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:PDCCh



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Offset.Pdcch.PdcchCls
	:members:
	:undoc-members:
	:noindex: