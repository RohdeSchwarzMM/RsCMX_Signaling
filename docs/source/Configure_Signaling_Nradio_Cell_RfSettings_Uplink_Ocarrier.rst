Ocarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:OCARrier

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:OCARrier



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Ocarrier.OcarrierCls
	:members:
	:undoc-members:
	:noindex: