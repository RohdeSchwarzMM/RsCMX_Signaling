TpControl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:TPControl

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:TPControl



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Control.TpControl.TpControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.power.control.tpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Power_Control_TpControl_Cloop.rst
	Configure_Signaling_Lte_Cell_Power_Control_TpControl_Pattern.rst