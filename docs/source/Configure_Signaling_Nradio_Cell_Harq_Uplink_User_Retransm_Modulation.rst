Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm:MODulation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm:MODulation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Uplink.User.Retransm.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: