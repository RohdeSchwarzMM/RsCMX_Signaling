Sdrx
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Sdrx.SdrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.cdrx.sdrx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Cdrx_Sdrx_Cycle.rst
	Configure_Signaling_Lte_Cell_Cdrx_Sdrx_Enable.rst
	Configure_Signaling_Lte_Cell_Cdrx_Sdrx_ScTimer.rst