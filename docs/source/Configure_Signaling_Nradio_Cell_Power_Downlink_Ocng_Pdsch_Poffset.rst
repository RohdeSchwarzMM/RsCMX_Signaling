Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDSCh:POFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDSCh:POFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.Pdsch.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: