Sindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:SASSignment:UL:SINDex

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:SASSignment:UL:SINDex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Sassignment.Uplink.Sindex.SindexCls
	:members:
	:undoc-members:
	:noindex: