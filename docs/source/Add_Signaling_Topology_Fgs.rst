Fgs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:TOPology:FGS

.. code-block:: python

	ADD:SIGNaling:TOPology:FGS



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Topology.Fgs.FgsCls
	:members:
	:undoc-members:
	:noindex: