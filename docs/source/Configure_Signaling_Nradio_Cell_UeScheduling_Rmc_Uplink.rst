Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:RMC:UL

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:RMC:UL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Rmc.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: