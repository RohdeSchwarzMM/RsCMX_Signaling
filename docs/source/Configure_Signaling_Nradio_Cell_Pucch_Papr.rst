Papr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUCCh:PAPR

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUCCh:PAPR



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pucch.Papr.PaprCls
	:members:
	:undoc-members:
	:noindex: