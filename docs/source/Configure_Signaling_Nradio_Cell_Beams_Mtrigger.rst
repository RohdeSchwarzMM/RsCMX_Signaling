Mtrigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAMs:MTRigger

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAMs:MTRigger



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beams.Mtrigger.MtriggerCls
	:members:
	:undoc-members:
	:noindex: