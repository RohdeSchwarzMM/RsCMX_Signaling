Uplane
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:SIGNaling:LOGGing:UPLane:UL
	single: DIAGnostic:SIGNaling:LOGGing:UPLane:DL

.. code-block:: python

	DIAGnostic:SIGNaling:LOGGing:UPLane:UL
	DIAGnostic:SIGNaling:LOGGing:UPLane:DL



.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Logging.Uplane.UplaneCls
	:members:
	:undoc-members:
	:noindex: