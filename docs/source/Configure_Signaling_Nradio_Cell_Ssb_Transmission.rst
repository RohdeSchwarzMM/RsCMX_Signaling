Transmission
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:TRANsmission

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:TRANsmission



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Transmission.TransmissionCls
	:members:
	:undoc-members:
	:noindex: