ReSelection
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.ReSelection.ReSelectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.reSelection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_ReSelection_Common.rst
	Configure_Signaling_Nradio_Cell_ReSelection_MinLevel.rst
	Configure_Signaling_Nradio_Cell_ReSelection_Priority.rst
	Configure_Signaling_Nradio_Cell_ReSelection_Search.rst
	Configure_Signaling_Nradio_Cell_ReSelection_Thresholds.rst
	Configure_Signaling_Nradio_Cell_ReSelection_Timer.rst