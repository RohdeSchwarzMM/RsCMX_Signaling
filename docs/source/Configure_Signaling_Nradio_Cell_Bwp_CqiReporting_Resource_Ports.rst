Ports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:RESource:PORTs

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:RESource:PORTs



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.CqiReporting.Resource.Ports.PortsCls
	:members:
	:undoc-members:
	:noindex: