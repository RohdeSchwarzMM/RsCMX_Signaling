Dbearer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:FGS:DBEarer:DNName
	single: [CONFigure]:SIGNaling:FGS:DBEarer:RLCMode

.. code-block:: python

	[CONFigure]:SIGNaling:FGS:DBEarer:DNName
	[CONFigure]:SIGNaling:FGS:DBEarer:RLCMode



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fgs.Dbearer.DbearerCls
	:members:
	:undoc-members:
	:noindex: