Tdd
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.tdd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Tdd_Subframe.rst