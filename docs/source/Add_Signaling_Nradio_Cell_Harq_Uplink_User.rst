User
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Harq.Uplink.User.UserCls
	:members:
	:undoc-members:
	:noindex: