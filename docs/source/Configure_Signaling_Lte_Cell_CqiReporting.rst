CqiReporting
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.CqiReporting.CqiReportingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.cqiReporting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_CqiReporting_Cindex.rst
	Configure_Signaling_Lte_Cell_CqiReporting_Findicator.rst
	Configure_Signaling_Lte_Cell_CqiReporting_PrEnable.rst
	Configure_Signaling_Lte_Cell_CqiReporting_Rmode.rst
	Configure_Signaling_Lte_Cell_CqiReporting_Rtype.rst
	Configure_Signaling_Lte_Cell_CqiReporting_Sancqi.rst