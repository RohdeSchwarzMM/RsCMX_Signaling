UserDefined
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Control.TpControl.Pattern.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.power.control.tpControl.pattern.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Power_Control_TpControl_Pattern_UserDefined_Mode.rst
	Configure_Signaling_Nradio_Cell_Power_Control_TpControl_Pattern_UserDefined_Pattern.rst