Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:NRADio:CELL

.. code-block:: python

	CREate:SIGNaling:NRADio:CELL



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Nradio.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: