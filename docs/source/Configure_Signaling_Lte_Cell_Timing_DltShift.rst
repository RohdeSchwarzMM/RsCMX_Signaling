DltShift
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TIMing:DLTShift

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TIMing:DLTShift



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Timing.DltShift.DltShiftCls
	:members:
	:undoc-members:
	:noindex: