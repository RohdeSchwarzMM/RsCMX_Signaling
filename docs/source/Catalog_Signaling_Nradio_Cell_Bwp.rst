Bwp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:NRADio:CELL:BWP

.. code-block:: python

	CATalog:SIGNaling:NRADio:CELL:BWP



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Nradio.Cell.Bwp.BwpCls
	:members:
	:undoc-members:
	:noindex: