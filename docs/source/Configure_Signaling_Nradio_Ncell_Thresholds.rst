Thresholds
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:NCELl:THResholds

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:NCELl:THResholds



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ncell.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: