Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Harq.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.harq.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Harq_Downlink_McsBehavior.rst
	Configure_Signaling_Lte_Cell_Harq_Downlink_PsOrder.rst
	Configure_Signaling_Lte_Cell_Harq_Downlink_ReTx.rst
	Configure_Signaling_Lte_Cell_Harq_Downlink_RvSequence.rst