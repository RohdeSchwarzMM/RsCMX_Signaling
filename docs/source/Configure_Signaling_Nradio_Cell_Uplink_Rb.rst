Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UL:RB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UL:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Uplink.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: