Sspacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:SSPacing

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:SSPacing



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Sspacing.SspacingCls
	:members:
	:undoc-members:
	:noindex: