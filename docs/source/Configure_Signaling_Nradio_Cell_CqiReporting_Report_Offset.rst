Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:REPort:OFFSet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:REPort:OFFSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Report.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: