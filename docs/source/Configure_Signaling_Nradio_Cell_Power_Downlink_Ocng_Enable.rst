Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: