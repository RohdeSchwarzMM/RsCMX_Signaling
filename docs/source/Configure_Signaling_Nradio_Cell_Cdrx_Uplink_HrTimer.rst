HrTimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:UL:HRTimer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:UL:HRTimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Uplink.HrTimer.HrTimerCls
	:members:
	:undoc-members:
	:noindex: