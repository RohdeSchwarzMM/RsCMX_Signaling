Papr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTA:PAPR

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTA:PAPR



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.Mta.Papr.PaprCls
	:members:
	:undoc-members:
	:noindex: