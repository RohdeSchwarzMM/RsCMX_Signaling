Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:COMBined:CFRequency:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:COMBined:CFRequency:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Combined.Cfrequency.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: