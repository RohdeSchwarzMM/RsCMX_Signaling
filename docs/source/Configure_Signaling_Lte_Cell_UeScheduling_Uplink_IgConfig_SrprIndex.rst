SrprIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:IGConfig:SRPRindex

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:IGConfig:SRPRindex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.IgConfig.SrprIndex.SrprIndexCls
	:members:
	:undoc-members:
	:noindex: