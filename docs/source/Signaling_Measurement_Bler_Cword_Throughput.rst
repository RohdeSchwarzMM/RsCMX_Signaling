Throughput
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:THRoughput

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:THRoughput



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Cword.Throughput.ThroughputCls
	:members:
	:undoc-members:
	:noindex: