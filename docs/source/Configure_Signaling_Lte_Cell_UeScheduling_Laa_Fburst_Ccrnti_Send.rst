Send
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:FBURst:CCRNti:SEND

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:FBURst:CCRNti:SEND



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.Fburst.Ccrnti.Send.SendCls
	:members:
	:undoc-members:
	:noindex: