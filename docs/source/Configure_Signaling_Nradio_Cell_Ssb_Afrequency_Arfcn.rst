Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:AFRequency:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:AFRequency:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Afrequency.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: