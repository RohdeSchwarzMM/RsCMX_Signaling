NrDl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:POFFset:NRDL

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:POFFset:NRDL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Poffset.NrDl.NrDlCls
	:members:
	:undoc-members:
	:noindex: