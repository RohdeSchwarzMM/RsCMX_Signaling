RaType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:DL:RATYpe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:DL:RATYpe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Downlink.RaType.RaTypeCls
	:members:
	:undoc-members:
	:noindex: