PsOrder
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:HARQ:DL:PSORder

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:HARQ:DL:PSORder



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Harq.Downlink.PsOrder.PsOrderCls
	:members:
	:undoc-members:
	:noindex: