Mrdc
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Fgs.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.fgs.ueCapability.mrdc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Fgs_UeCapability_Mrdc_Bands.rst