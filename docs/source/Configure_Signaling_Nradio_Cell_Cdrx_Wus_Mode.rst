Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:WUS:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:WUS:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Wus.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: