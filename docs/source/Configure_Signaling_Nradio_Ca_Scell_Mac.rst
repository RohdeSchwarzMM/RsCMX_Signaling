Mac
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:MAC

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:MAC



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Mac.MacCls
	:members:
	:undoc-members:
	:noindex: