CrSports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:ANTenna:CRSPorts

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:ANTenna:CRSPorts



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Antenna.CrSports.CrSportsCls
	:members:
	:undoc-members:
	:noindex: