PfOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PCYCle:PFOFfset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PCYCle:PFOFfset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pcycle.PfOffset.PfOffsetCls
	:members:
	:undoc-members:
	:noindex: