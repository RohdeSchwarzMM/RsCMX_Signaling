User
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Harq.Downlink.User.UserCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.harq.downlink.user.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Harq_Downlink_User_Ack.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Downlink_User_Dtx.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Downlink_User_MinOffset.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Downlink_User_Retransm.rst