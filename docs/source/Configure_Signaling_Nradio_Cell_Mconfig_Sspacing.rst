Sspacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MCONfig:SSPacing

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MCONfig:SSPacing



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Mconfig.Sspacing.SspacingCls
	:members:
	:undoc-members:
	:noindex: