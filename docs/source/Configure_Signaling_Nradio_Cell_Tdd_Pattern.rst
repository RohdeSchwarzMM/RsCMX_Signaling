Pattern<Pattern>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.signaling.nradio.cell.tdd.pattern.repcap_pattern_get()
	driver.configure.signaling.nradio.cell.tdd.pattern.repcap_pattern_set(repcap.Pattern.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.tdd.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Tdd_Pattern_Downlink.rst
	Configure_Signaling_Nradio_Cell_Tdd_Pattern_Enable.rst
	Configure_Signaling_Nradio_Cell_Tdd_Pattern_Periodicity.rst
	Configure_Signaling_Nradio_Cell_Tdd_Pattern_Uplink.rst