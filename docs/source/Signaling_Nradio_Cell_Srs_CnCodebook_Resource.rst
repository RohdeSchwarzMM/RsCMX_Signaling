Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource

.. code-block:: python

	DELete:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Srs.CnCodebook.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: