Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:REPort:PERiodicity

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:REPort:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.CqiReporting.Report.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: