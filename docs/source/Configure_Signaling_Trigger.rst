Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SIGNaling:TRIGger:SCOPe

.. code-block:: python

	CONFigure:SIGNaling:TRIGger:SCOPe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: