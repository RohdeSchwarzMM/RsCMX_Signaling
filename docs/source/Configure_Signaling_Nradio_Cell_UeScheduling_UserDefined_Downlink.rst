Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.ueScheduling.userDefined.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Downlink_Alevel.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Downlink_Bpid.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Downlink_McsTable.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Downlink_Padding.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Downlink_Ssid.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Downlink_VpMapping.rst