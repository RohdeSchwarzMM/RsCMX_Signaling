Bwp<BwParts>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.signaling.nradio.cell.bwp.repcap_bwParts_get()
	driver.configure.signaling.nradio.cell.bwp.repcap_bwParts_set(repcap.BwParts.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.BwpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_AsMode.rst
	Configure_Signaling_Nradio_Cell_Bwp_Bler.rst
	Configure_Signaling_Nradio_Cell_Bwp_CqiReporting.rst
	Configure_Signaling_Nradio_Cell_Bwp_Csi.rst
	Configure_Signaling_Nradio_Cell_Bwp_Dmrs.rst
	Configure_Signaling_Nradio_Cell_Bwp_Downlink.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq.rst
	Configure_Signaling_Nradio_Cell_Bwp_Nssb.rst
	Configure_Signaling_Nradio_Cell_Bwp_Power.rst
	Configure_Signaling_Nradio_Cell_Bwp_Pucch.rst
	Configure_Signaling_Nradio_Cell_Bwp_Pusch.rst
	Configure_Signaling_Nradio_Cell_Bwp_Smode.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs.rst
	Configure_Signaling_Nradio_Cell_Bwp_Sspacing.rst
	Configure_Signaling_Nradio_Cell_Bwp_Sue.rst
	Configure_Signaling_Nradio_Cell_Bwp_Tadvance.rst
	Configure_Signaling_Nradio_Cell_Bwp_Target.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling.rst
	Configure_Signaling_Nradio_Cell_Bwp_Uplink.rst