Mrdc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:FGS:UECapability:MRDC:BANDs

.. code-block:: python

	CATalog:SIGNaling:FGS:UECapability:MRDC:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Fgs.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex: