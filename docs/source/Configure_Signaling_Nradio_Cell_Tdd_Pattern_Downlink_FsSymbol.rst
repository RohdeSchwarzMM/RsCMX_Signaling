FsSymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:DL:FSSYmbol

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:DL:FSSYmbol



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.Pattern.Downlink.FsSymbol.FsSymbolCls
	:members:
	:undoc-members:
	:noindex: