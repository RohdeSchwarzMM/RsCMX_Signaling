Last
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:ELOG:LAST

.. code-block:: python

	SENSe:ELOG:LAST



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Elog.Last.LastCls
	:members:
	:undoc-members:
	:noindex: