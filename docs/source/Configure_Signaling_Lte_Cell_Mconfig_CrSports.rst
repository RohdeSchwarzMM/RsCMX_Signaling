CrSports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:MCONfig:CRSPorts

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:MCONfig:CRSPorts



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Mconfig.CrSports.CrSportsCls
	:members:
	:undoc-members:
	:noindex: