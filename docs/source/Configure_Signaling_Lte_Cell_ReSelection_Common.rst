Common
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RESelection:COMMon

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RESelection:COMMon



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.ReSelection.Common.CommonCls
	:members:
	:undoc-members:
	:noindex: