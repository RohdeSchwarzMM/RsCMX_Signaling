Uplink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Dmrs.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.dmrs.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Uplink_Mta.rst
	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Uplink_Mtb.rst
	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Uplink_Ptrs.rst