PdcchOrder
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:NRADio:PDCChorder:ACTivate

.. code-block:: python

	PROCedure:SIGNaling:NRADio:PDCChorder:ACTivate



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Nradio.PdcchOrder.PdcchOrderCls
	:members:
	:undoc-members:
	:noindex: