Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTA:LENGth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTA:LENGth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.Mta.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: