Tranmission
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:CMAS:TRANmission

.. code-block:: python

	[CONFigure]:SIGNaling:CMAS:TRANmission



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Cmas.Tranmission.TranmissionCls
	:members:
	:undoc-members:
	:noindex: