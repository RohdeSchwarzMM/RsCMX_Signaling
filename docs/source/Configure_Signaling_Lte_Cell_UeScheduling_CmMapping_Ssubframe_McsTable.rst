McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:SSUBframe:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:SSUBframe:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.CmMapping.Ssubframe.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: