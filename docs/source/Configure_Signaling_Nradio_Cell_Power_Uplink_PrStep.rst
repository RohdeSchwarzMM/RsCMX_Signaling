PrStep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:PRSTep

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:PRSTep



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.PrStep.PrStepCls
	:members:
	:undoc-members:
	:noindex: