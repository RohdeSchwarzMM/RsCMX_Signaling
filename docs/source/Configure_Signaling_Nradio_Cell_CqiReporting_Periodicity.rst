Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: