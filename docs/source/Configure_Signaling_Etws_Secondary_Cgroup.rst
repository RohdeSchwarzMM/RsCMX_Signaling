Cgroup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:CGRoup

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.Cgroup.CgroupCls
	:members:
	:undoc-members:
	:noindex: