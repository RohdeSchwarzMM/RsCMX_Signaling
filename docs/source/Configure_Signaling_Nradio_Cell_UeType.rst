UeType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UETYpe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UETYpe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeType.UeTypeCls
	:members:
	:undoc-members:
	:noindex: