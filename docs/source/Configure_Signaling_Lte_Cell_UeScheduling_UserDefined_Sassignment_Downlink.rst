Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.userDefined.sassignment.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_All.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Cword.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_DciFormat.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Enable.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_McsTable.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_PdcchFormat.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Rb.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Riv.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Tmode.rst