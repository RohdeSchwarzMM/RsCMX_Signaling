Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:RB

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.BsrConfig.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: