RelPref
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio:RELPref

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio:RELPref



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.RelPref.RelPrefCls
	:members:
	:undoc-members:
	:noindex: