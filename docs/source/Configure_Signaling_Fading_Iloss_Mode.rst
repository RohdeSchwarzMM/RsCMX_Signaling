Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:FADing:ILOSs:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:FADing:ILOSs:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fading.Iloss.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: