Cmatrix
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PROCedure:SIGNaling:NRADio:CELL:CMATrix:HADamard
	single: PROCedure:SIGNaling:NRADio:CELL:CMATrix:TGPP

.. code-block:: python

	PROCedure:SIGNaling:NRADio:CELL:CMATrix:HADamard
	PROCedure:SIGNaling:NRADio:CELL:CMATrix:TGPP



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Nradio.Cell.Cmatrix.CmatrixCls
	:members:
	:undoc-members:
	:noindex: