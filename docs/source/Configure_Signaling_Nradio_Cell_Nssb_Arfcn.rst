Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:NSSB:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:NSSB:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Nssb.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: