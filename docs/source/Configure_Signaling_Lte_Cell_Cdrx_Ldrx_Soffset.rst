Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:LDRX:SOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:LDRX:SOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Ldrx.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: