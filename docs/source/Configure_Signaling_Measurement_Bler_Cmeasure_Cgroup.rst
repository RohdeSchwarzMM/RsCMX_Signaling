Cgroup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:BLER:CMEasure:CGRoup

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:BLER:CMEasure:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.Bler.Cmeasure.Cgroup.CgroupCls
	:members:
	:undoc-members:
	:noindex: