TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTA:TYPE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTA:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.Mta.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: