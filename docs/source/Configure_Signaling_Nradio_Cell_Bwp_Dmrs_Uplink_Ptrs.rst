Ptrs
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Dmrs.Uplink.Ptrs.PtrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.dmrs.uplink.ptrs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Uplink_Ptrs_Enable.rst
	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Uplink_Ptrs_TpDisable.rst
	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Uplink_Ptrs_TpEnable.rst