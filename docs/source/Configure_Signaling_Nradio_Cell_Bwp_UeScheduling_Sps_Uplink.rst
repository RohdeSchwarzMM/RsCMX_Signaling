Uplink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.UeScheduling.Sps.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.ueScheduling.sps.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_Alevel.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_All.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_CgTimer.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_DmrsPosition.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_McsTable.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_Nohp.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_Periodicity.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_RaType.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_RbgSize.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_RrVersion.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_Ssid.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink_TpEnable.rst