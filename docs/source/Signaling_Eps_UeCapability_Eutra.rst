Eutra
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Eps.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.eps.ueCapability.eutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Eps_UeCapability_Eutra_Bands.rst