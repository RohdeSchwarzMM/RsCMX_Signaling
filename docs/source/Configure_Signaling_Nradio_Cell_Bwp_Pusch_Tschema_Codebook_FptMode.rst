FptMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:PUSCh:TSCHema:CODebook:FPTMode

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:PUSCh:TSCHema:CODebook:FPTMode



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Pusch.Tschema.Codebook.FptMode.FptModeCls
	:members:
	:undoc-members:
	:noindex: