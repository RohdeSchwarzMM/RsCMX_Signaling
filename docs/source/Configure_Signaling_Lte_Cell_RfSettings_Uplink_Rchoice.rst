Rchoice
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:RCHoice

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:RCHoice



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Uplink.Rchoice.RchoiceCls
	:members:
	:undoc-members:
	:noindex: