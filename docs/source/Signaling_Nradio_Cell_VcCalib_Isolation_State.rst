State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:NRADio:CELL:VCCalib:ISOLation:STATe

.. code-block:: python

	FETCh:SIGNaling:NRADio:CELL:VCCalib:ISOLation:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.VcCalib.Isolation.State.StateCls
	:members:
	:undoc-members:
	:noindex: