Fgs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:TOPology:FGS

.. code-block:: python

	DELete:SIGNaling:TOPology:FGS



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Topology.Fgs.FgsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.topology.fgs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Topology_Fgs_Ue.rst