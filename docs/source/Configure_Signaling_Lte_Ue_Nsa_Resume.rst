Resume
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:UE:NSA:RESume

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:UE:NSA:RESume



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ue.Nsa.Resume.ResumeCls
	:members:
	:undoc-members:
	:noindex: