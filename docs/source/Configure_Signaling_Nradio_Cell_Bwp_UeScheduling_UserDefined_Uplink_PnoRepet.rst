PnoRepet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:UESCheduling:UDEFined:UL:PNORepet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:UESCheduling:UDEFined:UL:PNORepet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.UeScheduling.UserDefined.Uplink.PnoRepet.PnoRepetCls
	:members:
	:undoc-members:
	:noindex: