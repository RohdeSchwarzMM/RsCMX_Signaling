LbWidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UL:LBWidth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UL:LBWidth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Uplink.LbWidth.LbWidthCls
	:members:
	:undoc-members:
	:noindex: