Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:UL:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:UL:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Uplink.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: