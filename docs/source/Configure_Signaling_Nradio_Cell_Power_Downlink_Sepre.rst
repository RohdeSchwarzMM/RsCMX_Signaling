Sepre
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:SEPRe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:SEPRe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Sepre.SepreCls
	:members:
	:undoc-members:
	:noindex: