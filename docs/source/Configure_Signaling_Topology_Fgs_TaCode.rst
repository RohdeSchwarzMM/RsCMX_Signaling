TaCode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:TOPology:FGS:TACode

.. code-block:: python

	[CONFigure]:SIGNaling:TOPology:FGS:TACode



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Topology.Fgs.TaCode.TaCodeCls
	:members:
	:undoc-members:
	:noindex: