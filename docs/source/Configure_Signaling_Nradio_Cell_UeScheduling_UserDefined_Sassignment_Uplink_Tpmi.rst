Tpmi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:SASSignment:UL:TPMI

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:SASSignment:UL:TPMI



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Sassignment.Uplink.Tpmi.TpmiCls
	:members:
	:undoc-members:
	:noindex: