FoSymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:FOSYmbol

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:FOSYmbol



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Resource.FoSymbol.FoSymbolCls
	:members:
	:undoc-members:
	:noindex: