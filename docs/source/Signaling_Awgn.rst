Awgn
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Awgn.AwgnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.awgn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Awgn_Advanced.rst