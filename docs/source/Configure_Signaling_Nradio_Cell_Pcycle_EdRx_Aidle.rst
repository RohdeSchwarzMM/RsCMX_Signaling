Aidle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PCYCle:EDRX:AIDLe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PCYCle:EDRX:AIDLe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pcycle.EdRx.Aidle.AidleCls
	:members:
	:undoc-members:
	:noindex: