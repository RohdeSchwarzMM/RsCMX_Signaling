IpPreambles
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:IPPReambles

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:IPPReambles



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.IpPreambles.IpPreamblesCls
	:members:
	:undoc-members:
	:noindex: