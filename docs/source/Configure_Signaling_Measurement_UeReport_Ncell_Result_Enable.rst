Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.UeReport.Ncell.Result.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: