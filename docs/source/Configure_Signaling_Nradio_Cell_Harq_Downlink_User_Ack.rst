Ack
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:ACK

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:ACK



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Downlink.User.Ack.AckCls
	:members:
	:undoc-members:
	:noindex: