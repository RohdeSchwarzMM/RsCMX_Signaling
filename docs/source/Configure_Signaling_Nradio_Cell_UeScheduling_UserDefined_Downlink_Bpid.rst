Bpid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:DL:BPID

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:DL:BPID



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Downlink.Bpid.BpidCls
	:members:
	:undoc-members:
	:noindex: