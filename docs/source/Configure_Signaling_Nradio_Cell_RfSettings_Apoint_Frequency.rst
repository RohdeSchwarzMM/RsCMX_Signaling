Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:APOint:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:APOint:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Apoint.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: