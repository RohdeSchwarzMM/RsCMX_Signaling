WoLanguage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:CMAS:WOLanguage

.. code-block:: python

	[CONFigure]:SIGNaling:CMAS:WOLanguage



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Cmas.WoLanguage.WoLanguageCls
	:members:
	:undoc-members:
	:noindex: