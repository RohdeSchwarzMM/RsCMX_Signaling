SfInterval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:SFINterval

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:SFINterval



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Sps.Sassignment.Uplink.SfInterval.SfIntervalCls
	:members:
	:undoc-members:
	:noindex: