Trace
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.measurement.cqiReporting.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Measurement_CqiReporting_Trace_Lte.rst
	Signaling_Measurement_CqiReporting_Trace_Nradio.rst