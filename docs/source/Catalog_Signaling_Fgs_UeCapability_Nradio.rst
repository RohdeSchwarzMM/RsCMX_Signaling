Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:FGS:UECapability:NRADio:BANDs

.. code-block:: python

	CATalog:SIGNaling:FGS:UECapability:NRADio:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Fgs.UeCapability.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex: