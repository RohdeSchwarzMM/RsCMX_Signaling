Confidence
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:CONFidence

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:CONFidence



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Confidence.ConfidenceCls
	:members:
	:undoc-members:
	:noindex: