Cell
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Wlan.Measurement.Network.Cell.CellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.wlan.measurement.network.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Wlan_Measurement_Network_Cell_Uplinks.rst