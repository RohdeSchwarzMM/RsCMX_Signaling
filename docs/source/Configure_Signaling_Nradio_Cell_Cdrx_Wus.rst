Wus
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Wus.WusCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.cdrx.wus.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Cdrx_Wus_All.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Wus_Enable.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Wus_Mode.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Wus_Ratio.rst