Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BWP<bwpid>:ID

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BWP<bwpid>:ID



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.Id.IdCls
	:members:
	:undoc-members:
	:noindex: