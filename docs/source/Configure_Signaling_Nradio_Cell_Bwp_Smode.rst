Smode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP:SMODe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP:SMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Smode.SmodeCls
	:members:
	:undoc-members:
	:noindex: