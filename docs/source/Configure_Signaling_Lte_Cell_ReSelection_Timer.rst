Timer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RESelection:TIMer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RESelection:TIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.ReSelection.Timer.TimerCls
	:members:
	:undoc-members:
	:noindex: