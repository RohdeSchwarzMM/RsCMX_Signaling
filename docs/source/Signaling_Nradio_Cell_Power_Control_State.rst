State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:NRADio:CELL:POWer:CONTrol:STATe

.. code-block:: python

	FETCh:SIGNaling:NRADio:CELL:POWer:CONTrol:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Power.Control.State.StateCls
	:members:
	:undoc-members:
	:noindex: