Bwp<BwParts>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.sense.signaling.nradio.cell.bwp.repcap_bwParts_get()
	driver.sense.signaling.nradio.cell.bwp.repcap_bwParts_set(repcap.BwParts.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.BwpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.signaling.nradio.cell.bwp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Signaling_Nradio_Cell_Bwp_CqiReporting.rst
	Sense_Signaling_Nradio_Cell_Bwp_Harq.rst
	Sense_Signaling_Nradio_Cell_Bwp_Id.rst
	Sense_Signaling_Nradio_Cell_Bwp_Power.rst
	Sense_Signaling_Nradio_Cell_Bwp_Pucch.rst
	Sense_Signaling_Nradio_Cell_Bwp_UeScheduling.rst