Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OCNG:PDCCh:POFFset

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OCNG:PDCCh:POFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Ocng.Pdcch.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: