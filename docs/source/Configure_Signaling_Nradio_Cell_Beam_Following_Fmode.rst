Fmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAM:FOLLowing:FMODe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAM:FOLLowing:FMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Following.Fmode.FmodeCls
	:members:
	:undoc-members:
	:noindex: