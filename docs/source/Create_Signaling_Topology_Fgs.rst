Fgs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:TOPology:FGS

.. code-block:: python

	CREate:SIGNaling:TOPology:FGS



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Topology.Fgs.FgsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.signaling.topology.fgs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_Signaling_Topology_Fgs_Ue.rst