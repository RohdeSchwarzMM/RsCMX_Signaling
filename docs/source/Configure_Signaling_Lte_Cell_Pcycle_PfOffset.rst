PfOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:PCYCle:PFOFfset

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:PCYCle:PFOFfset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Pcycle.PfOffset.PfOffsetCls
	:members:
	:undoc-members:
	:noindex: