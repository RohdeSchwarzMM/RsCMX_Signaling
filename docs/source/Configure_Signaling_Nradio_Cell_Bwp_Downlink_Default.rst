Default
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:DL:DEFault

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:DL:DEFault



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Downlink.Default.DefaultCls
	:members:
	:undoc-members:
	:noindex: