Uplinks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:WLAN:MEASurement<Instance>:NETWork:CELL:UPLinks

.. code-block:: python

	CATalog:WLAN:MEASurement<Instance>:NETWork:CELL:UPLinks



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Wlan.Measurement.Network.Cell.Uplinks.UplinksCls
	:members:
	:undoc-members:
	:noindex: