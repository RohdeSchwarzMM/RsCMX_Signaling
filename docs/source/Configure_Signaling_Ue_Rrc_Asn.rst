Asn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:UE:RRC:ASN:SETup
	single: [CONFigure]:SIGNaling:UE:RRC:ASN:REConfig
	single: [CONFigure]:SIGNaling:UE:RRC:ASN:RELease

.. code-block:: python

	[CONFigure]:SIGNaling:UE:RRC:ASN:SETup
	[CONFigure]:SIGNaling:UE:RRC:ASN:REConfig
	[CONFigure]:SIGNaling:UE:RRC:ASN:RELease



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Ue.Rrc.Asn.AsnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.ue.rrc.asn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Ue_Rrc_Asn_Lte.rst