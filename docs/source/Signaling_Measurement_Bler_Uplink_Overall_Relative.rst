Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:UL:OVERall:RELative

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:UL:OVERall:RELative



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Uplink.Overall.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: