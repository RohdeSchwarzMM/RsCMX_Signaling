RepCaps
=========



BwParts
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BwParts.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

Cword
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Cword.Nr1
	# Values (2x):
	Nr1 | Nr2

MeasInstance
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MeasInstance.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

Nnum
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Nnum.Nr310
	# Values (2x):
	Nr310 | Nr311

Pattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Pattern.Nr1
	# Values (2x):
	Nr1 | Nr2

QamOrder
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.QamOrder.Order64
	# Values (2x):
	Order64 | Order256

Tnum
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Tnum.Nr300
	# Range:
	Nr300 .. Nr319
	# All values (5x):
	Nr300 | Nr301 | Nr310 | Nr311 | Nr319

