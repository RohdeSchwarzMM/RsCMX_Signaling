Cword<Cword>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.signaling.measurement.bler.cword.repcap_cword_get()
	driver.signaling.measurement.bler.cword.repcap_cword_set(repcap.Cword.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Cword.CwordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.measurement.bler.cword.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Measurement_Bler_Cword_Absolute.rst
	Signaling_Measurement_Bler_Cword_Relative.rst
	Signaling_Measurement_Bler_Cword_Throughput.rst