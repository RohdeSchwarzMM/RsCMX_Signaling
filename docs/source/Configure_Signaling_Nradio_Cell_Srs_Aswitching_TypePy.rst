TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:TYPE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Aswitching.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: