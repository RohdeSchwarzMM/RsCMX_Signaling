Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:TPControl:PATTern:UDEFined:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:TPControl:PATTern:UDEFined:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Control.TpControl.Pattern.UserDefined.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: