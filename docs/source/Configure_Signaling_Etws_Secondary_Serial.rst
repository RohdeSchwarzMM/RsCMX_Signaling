Serial
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:SERial

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:SERial



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.Serial.SerialCls
	:members:
	:undoc-members:
	:noindex: