Sancqi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CQIReporting:SANCqi

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CQIReporting:SANCqi



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.CqiReporting.Sancqi.SancqiCls
	:members:
	:undoc-members:
	:noindex: