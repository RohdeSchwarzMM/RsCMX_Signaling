Lobw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:IBWP:LOBW

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:IBWP:LOBW



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Ibwp.Lobw.LobwCls
	:members:
	:undoc-members:
	:noindex: