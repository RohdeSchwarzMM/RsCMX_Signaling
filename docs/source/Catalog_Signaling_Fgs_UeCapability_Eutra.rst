Eutra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:FGS:UECapability:EUTRa:BANDs

.. code-block:: python

	CATalog:SIGNaling:FGS:UECapability:EUTRa:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Fgs.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: