Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:LTE:CELL:HARQ:DL:RETX

.. code-block:: python

	ADD:SIGNaling:LTE:CELL:HARQ:DL:RETX



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Lte.Cell.Harq.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.signaling.lte.cell.harq.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_Signaling_Lte_Cell_Harq_Downlink_RvSequence.rst