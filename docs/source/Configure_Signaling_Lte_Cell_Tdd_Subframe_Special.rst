Special
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TDD:SUBFrame:SPECial

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TDD:SUBFrame:SPECial



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Tdd.Subframe.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex: