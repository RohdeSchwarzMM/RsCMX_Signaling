State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:TOPology:CNETwork:STATe

.. code-block:: python

	FETCh:SIGNaling:TOPology:CNETwork:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Topology.Cnetwork.State.StateCls
	:members:
	:undoc-members:
	:noindex: