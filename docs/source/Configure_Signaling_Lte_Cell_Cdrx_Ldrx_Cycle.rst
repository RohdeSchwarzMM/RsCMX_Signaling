Cycle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:LDRX:CYCLe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:LDRX:CYCLe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Ldrx.Cycle.CycleCls
	:members:
	:undoc-members:
	:noindex: