UeCapability
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Fgs.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.fgs.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Fgs_UeCapability_Eutra.rst
	Signaling_Fgs_UeCapability_Mrdc.rst
	Signaling_Fgs_UeCapability_Nradio.rst