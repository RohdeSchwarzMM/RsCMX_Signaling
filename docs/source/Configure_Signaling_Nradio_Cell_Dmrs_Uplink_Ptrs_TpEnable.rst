TpEnable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:PTRS:TPENable

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:PTRS:TPENable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Uplink.Ptrs.TpEnable.TpEnableCls
	:members:
	:undoc-members:
	:noindex: