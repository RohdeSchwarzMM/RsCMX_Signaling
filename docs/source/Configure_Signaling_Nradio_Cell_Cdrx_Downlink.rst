Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.cdrx.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Cdrx_Downlink_HrTimer.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Downlink_Rtimer.rst