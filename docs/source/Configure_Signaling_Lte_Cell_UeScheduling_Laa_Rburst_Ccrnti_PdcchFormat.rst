PdcchFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:RBURst:CCRNti:PDCChformat

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:RBURst:CCRNti:PDCChformat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.Rburst.Ccrnti.PdcchFormat.PdcchFormatCls
	:members:
	:undoc-members:
	:noindex: