Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:FADing:DSHift:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:FADing:DSHift:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fading.Dshift.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: