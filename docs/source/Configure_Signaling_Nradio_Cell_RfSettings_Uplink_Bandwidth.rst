Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: