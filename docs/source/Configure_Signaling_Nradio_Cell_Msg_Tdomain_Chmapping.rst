Chmapping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MSG<id>:TDOMain:CHMapping

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MSG<id>:TDOMain:CHMapping



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Msg.Tdomain.Chmapping.ChmappingCls
	:members:
	:undoc-members:
	:noindex: