Network
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.NrMmw.Measurement.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmw.measurement.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmw_Measurement_Network_Cell.rst