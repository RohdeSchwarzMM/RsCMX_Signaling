Lowp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RESelection:THResholds:LOWP

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RESelection:THResholds:LOWP



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.ReSelection.Thresholds.Lowp.LowpCls
	:members:
	:undoc-members:
	:noindex: