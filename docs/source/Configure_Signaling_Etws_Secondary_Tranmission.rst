Tranmission
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:TRANmission

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:TRANmission



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.Tranmission.TranmissionCls
	:members:
	:undoc-members:
	:noindex: