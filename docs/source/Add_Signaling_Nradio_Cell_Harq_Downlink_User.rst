User
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Harq.Downlink.User.UserCls
	:members:
	:undoc-members:
	:noindex: