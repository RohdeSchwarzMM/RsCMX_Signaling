Eutra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:EPS:UECapability:EUTRa:BANDs

.. code-block:: python

	ADD:SIGNaling:EPS:UECapability:EUTRa:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Eps.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: