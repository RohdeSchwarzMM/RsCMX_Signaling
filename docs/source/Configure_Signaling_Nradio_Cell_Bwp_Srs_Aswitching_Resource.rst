Resource
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Srs.Aswitching.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.srs.aswitching.resource.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Resource_Fhopping.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Resource_Resource.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Resource_Rmapping.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Resource_Rtype.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Resource_Tcomb.rst