Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Bwp.Harq.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.signaling.nradio.cell.bwp.harq.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_Signaling_Nradio_Cell_Bwp_Harq_Downlink_User.rst