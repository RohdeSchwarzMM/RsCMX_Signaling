Sue
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:SUE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:SUE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Sue.SueCls
	:members:
	:undoc-members:
	:noindex: