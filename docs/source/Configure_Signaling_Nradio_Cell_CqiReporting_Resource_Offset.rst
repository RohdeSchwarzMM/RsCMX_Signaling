Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:OFFSet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:OFFSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Resource.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: