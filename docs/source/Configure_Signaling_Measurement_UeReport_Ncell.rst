Ncell
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:ENABle
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RINTerval

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:ENABle
	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RINTerval



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.UeReport.Ncell.NcellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.measurement.ueReport.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Measurement_UeReport_Ncell_Result.rst