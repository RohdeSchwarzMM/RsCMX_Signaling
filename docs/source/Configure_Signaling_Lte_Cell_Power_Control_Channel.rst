Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:CHANnel

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:CHANnel



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Control.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: