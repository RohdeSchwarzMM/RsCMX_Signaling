State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:SIGNaling:NRADio:CELL:STATe

.. code-block:: python

	SOURce:SIGNaling:NRADio:CELL:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Source.Signaling.Nradio.Cell.State.StateCls
	:members:
	:undoc-members:
	:noindex: