Rchoice
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:RCHoice

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:RCHoice



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.Rchoice.RchoiceCls
	:members:
	:undoc-members:
	:noindex: