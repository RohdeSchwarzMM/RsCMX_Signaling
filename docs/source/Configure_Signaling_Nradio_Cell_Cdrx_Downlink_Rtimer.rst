Rtimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:DL:RTIMer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:DL:RTIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Downlink.Rtimer.RtimerCls
	:members:
	:undoc-members:
	:noindex: