PppScaling
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:PPPScaling

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:PPPScaling



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.PppScaling.PppScalingCls
	:members:
	:undoc-members:
	:noindex: