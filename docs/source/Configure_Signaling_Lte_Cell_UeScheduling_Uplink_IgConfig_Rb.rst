Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:IGConfig:RB

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:IGConfig:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.IgConfig.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: