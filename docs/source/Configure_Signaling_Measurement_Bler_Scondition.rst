Scondition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:BLER:SCONdition

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:BLER:SCONdition



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.Bler.Scondition.SconditionCls
	:members:
	:undoc-members:
	:noindex: