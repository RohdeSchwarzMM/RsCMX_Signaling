Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:CFRequency:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:CFRequency:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Cfrequency.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: