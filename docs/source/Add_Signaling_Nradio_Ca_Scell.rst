Scell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CA:SCELl

.. code-block:: python

	ADD:SIGNaling:NRADio:CA:SCELl



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Ca.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex: