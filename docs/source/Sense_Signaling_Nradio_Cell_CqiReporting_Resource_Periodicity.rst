Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:CQIReporting:RESource:PERiodicity

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:CQIReporting:RESource:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.CqiReporting.Resource.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: