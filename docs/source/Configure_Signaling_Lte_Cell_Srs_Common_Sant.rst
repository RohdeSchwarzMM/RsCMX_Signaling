Sant
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:COMMon:SANT

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:COMMon:SANT



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Common.Sant.SantCls
	:members:
	:undoc-members:
	:noindex: