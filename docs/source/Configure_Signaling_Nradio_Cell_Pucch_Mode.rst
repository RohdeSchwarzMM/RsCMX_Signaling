Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUCCh:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUCCh:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pucch.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: