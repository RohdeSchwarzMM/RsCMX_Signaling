Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:RESource:PERiodicity

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:RESource:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.CqiReporting.Resource.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: