Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CSI:TRS:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CSI:TRS:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Csi.Trs.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: