Npreambles
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:NPReambles

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:NPReambles



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.Npreambles.NpreamblesCls
	:members:
	:undoc-members:
	:noindex: