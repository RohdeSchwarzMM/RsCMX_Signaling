Location
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:APOint:LOCation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:APOint:LOCation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Apoint.Location.LocationCls
	:members:
	:undoc-members:
	:noindex: