Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource:RESource

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.CnCodebook.Resource.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: