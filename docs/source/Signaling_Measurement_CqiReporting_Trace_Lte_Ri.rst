Ri
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:LTE:RI

.. code-block:: python

	FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:LTE:RI



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.Trace.Lte.Ri.RiCls
	:members:
	:undoc-members:
	:noindex: