Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:SDRX:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:SDRX:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Sdrx.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: