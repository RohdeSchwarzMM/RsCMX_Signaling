Epre
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:EPRE

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:EPRE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.Epre.EpreCls
	:members:
	:undoc-members:
	:noindex: