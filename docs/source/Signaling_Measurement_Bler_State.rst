State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:STATe

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.State.StateCls
	:members:
	:undoc-members:
	:noindex: