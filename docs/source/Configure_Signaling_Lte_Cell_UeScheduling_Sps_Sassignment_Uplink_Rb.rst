Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:RB

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Sps.Sassignment.Uplink.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: