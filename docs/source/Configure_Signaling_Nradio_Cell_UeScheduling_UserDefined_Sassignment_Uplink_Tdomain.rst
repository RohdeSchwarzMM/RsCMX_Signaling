Tdomain
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Sassignment.Uplink.Tdomain.TdomainCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.ueScheduling.userDefined.sassignment.uplink.tdomain.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Sassignment_Uplink_Tdomain_Chmapping.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Sassignment_Uplink_Tdomain_PnoRepet.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Sassignment_Uplink_Tdomain_Soffset.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Sassignment_Uplink_Tdomain_Symbol.rst