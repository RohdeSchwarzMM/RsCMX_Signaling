SfnOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TIMing:SFNoffset

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TIMing:SFNoffset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Timing.SfnOffset.SfnOffsetCls
	:members:
	:undoc-members:
	:noindex: