Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.UeScheduling.UserDefined.Sassignment.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.ueScheduling.userDefined.sassignment.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_All.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_DciFormat.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_Enable.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_Mcs.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_Mimo.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_Rb.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment_Downlink_Tdomain.rst