Tdd
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.tdd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Tdd_Enable.rst
	Configure_Signaling_Nradio_Cell_Tdd_Pattern.rst
	Configure_Signaling_Nradio_Cell_Tdd_Uplink.rst