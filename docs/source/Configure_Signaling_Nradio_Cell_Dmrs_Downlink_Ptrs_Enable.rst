Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:PTRS:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:PTRS:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.Ptrs.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: