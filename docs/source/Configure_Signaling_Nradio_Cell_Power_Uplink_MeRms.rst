MeRms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:MERMs

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:MERMs



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.MeRms.MeRmsCls
	:members:
	:undoc-members:
	:noindex: