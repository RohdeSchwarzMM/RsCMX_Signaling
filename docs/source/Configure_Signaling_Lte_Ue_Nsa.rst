Nsa
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:LTE:UE:NSA:ACTivate
	single: [CONFigure]:SIGNaling:LTE:UE:NSA:DEACtivate

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:UE:NSA:ACTivate
	[CONFigure]:SIGNaling:LTE:UE:NSA:DEACtivate



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ue.Nsa.NsaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.ue.nsa.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Ue_Nsa_Resume.rst