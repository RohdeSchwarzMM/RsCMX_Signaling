Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.rfSettings.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_RfSettings_Downlink_Bandwidth.rst
	Configure_Signaling_Lte_Cell_RfSettings_Downlink_Earfcn.rst
	Configure_Signaling_Lte_Cell_RfSettings_Downlink_FreqError.rst
	Configure_Signaling_Lte_Cell_RfSettings_Downlink_Frequency.rst
	Configure_Signaling_Lte_Cell_RfSettings_Downlink_Rblocks.rst
	Configure_Signaling_Lte_Cell_RfSettings_Downlink_Rchoice.rst