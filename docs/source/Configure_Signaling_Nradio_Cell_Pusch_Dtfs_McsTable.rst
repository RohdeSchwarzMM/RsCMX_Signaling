McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUSCh:DTFS:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUSCh:DTFS:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pusch.Dtfs.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: