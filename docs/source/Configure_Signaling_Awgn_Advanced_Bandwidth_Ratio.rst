Ratio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:AWGN:ADVanced:BWIDth:RATio

.. code-block:: python

	[CONFigure]:SIGNaling:AWGN:ADVanced:BWIDth:RATio



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Awgn.Advanced.Bandwidth.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex: