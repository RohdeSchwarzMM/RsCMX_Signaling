UeCapability
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:FGS:UECapability:MODE
	single: [CONFigure]:SIGNaling:FGS:UECapability:SEGMentation
	single: [CONFigure]:SIGNaling:FGS:UECapability:SRSCarrier

.. code-block:: python

	[CONFigure]:SIGNaling:FGS:UECapability:MODE
	[CONFigure]:SIGNaling:FGS:UECapability:SEGMentation
	[CONFigure]:SIGNaling:FGS:UECapability:SRSCarrier



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fgs.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.fgs.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Fgs_UeCapability_Eutra.rst
	Configure_Signaling_Fgs_UeCapability_Mrdc.rst