T<Tnum>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr300 .. Nr319
	rc = driver.configure.signaling.nradio.cell.timeout.t.repcap_tnum_get()
	driver.configure.signaling.nradio.cell.timeout.t.repcap_tnum_set(repcap.Tnum.Nr300)



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TOUT:T<no>

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TOUT:T<no>



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Timeout.T.TCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.timeout.t.clone()