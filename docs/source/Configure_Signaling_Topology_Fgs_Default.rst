Default
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:TOPology:FGS:DEFault:VOICe

.. code-block:: python

	[CONFigure]:SIGNaling:TOPology:FGS:DEFault:VOICe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Topology.Fgs.Default.DefaultCls
	:members:
	:undoc-members:
	:noindex: