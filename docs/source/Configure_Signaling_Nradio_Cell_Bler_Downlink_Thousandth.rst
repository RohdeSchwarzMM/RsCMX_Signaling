Thousandth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BLER:DL:THOusandth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BLER:DL:THOusandth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bler.Downlink.Thousandth.ThousandthCls
	:members:
	:undoc-members:
	:noindex: