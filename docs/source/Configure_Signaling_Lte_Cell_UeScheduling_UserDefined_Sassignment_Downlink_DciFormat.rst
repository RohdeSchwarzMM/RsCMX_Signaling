DciFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:DL:DCIFormat

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:DL:DCIFormat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Downlink.DciFormat.DciFormatCls
	:members:
	:undoc-members:
	:noindex: