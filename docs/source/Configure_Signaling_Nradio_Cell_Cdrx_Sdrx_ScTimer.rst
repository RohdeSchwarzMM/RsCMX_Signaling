ScTimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:SDRX:SCTimer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:SDRX:SCTimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Sdrx.ScTimer.ScTimerCls
	:members:
	:undoc-members:
	:noindex: