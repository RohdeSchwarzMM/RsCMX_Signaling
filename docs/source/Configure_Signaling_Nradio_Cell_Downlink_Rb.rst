Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DL:RB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DL:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Downlink.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: