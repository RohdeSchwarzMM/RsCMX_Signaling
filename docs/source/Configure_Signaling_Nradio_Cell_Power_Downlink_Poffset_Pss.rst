Pss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:POFFset:PSS

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:POFFset:PSS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Poffset.Pss.PssCls
	:members:
	:undoc-members:
	:noindex: