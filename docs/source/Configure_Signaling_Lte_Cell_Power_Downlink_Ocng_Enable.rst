Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OCNG:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OCNG:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Ocng.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: