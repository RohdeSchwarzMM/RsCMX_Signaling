QosFlow
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:TOPology:FGS:UE:PDU:QOSFlow

.. code-block:: python

	CREate:SIGNaling:TOPology:FGS:UE:PDU:QOSFlow



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Topology.Fgs.Ue.Pdu.QosFlow.QosFlowCls
	:members:
	:undoc-members:
	:noindex: