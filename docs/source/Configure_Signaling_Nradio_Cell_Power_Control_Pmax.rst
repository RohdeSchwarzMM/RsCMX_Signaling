Pmax
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:PMAX

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:PMAX



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Control.Pmax.PmaxCls
	:members:
	:undoc-members:
	:noindex: