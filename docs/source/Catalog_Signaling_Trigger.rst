Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:TRIGger:SOURce

.. code-block:: python

	CATalog:SIGNaling:TRIGger:SOURce



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: