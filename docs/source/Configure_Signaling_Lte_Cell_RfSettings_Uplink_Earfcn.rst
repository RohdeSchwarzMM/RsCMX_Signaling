Earfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:EARFcn

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:EARFcn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Uplink.Earfcn.EarfcnCls
	:members:
	:undoc-members:
	:noindex: