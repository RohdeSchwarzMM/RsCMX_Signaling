Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Dormancy.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: