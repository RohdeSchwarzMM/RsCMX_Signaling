Configure
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Lte.rst
	Configure_NrMmw.rst
	Configure_NrSub.rst
	Configure_Signaling.rst
	Configure_Wlan.rst