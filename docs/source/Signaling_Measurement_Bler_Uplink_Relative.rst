Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:UL:RELative

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:UL:RELative



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Uplink.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: