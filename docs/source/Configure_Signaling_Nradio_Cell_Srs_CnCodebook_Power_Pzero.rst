Pzero
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:POWer:PZERo

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:POWer:PZERo



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.CnCodebook.Power.Pzero.PzeroCls
	:members:
	:undoc-members:
	:noindex: