UserDefined
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Frecovery.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.beam.frecovery.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Beam_Frecovery_UserDefined_Csirs.rst
	Configure_Signaling_Nradio_Cell_Beam_Frecovery_UserDefined_Ssb.rst