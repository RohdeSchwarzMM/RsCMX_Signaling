Cword<Cword>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.signaling.measurement.cqiReporting.trace.nradio.cword.repcap_cword_get()
	driver.signaling.measurement.cqiReporting.trace.nradio.cword.repcap_cword_set(repcap.Cword.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:NRADio:CWORd<no>

.. code-block:: python

	FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:NRADio:CWORd<no>



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.Trace.Nradio.Cword.CwordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.measurement.cqiReporting.trace.nradio.cword.clone()