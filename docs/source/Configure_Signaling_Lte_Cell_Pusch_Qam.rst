Qam<QamOrder>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Order64 .. Order256
	rc = driver.configure.signaling.lte.cell.pusch.qam.repcap_qamOrder_get()
	driver.configure.signaling.lte.cell.pusch.qam.repcap_qamOrder_set(repcap.QamOrder.Order64)



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:PUSCh:QAM<nr>

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:PUSCh:QAM<nr>



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Pusch.Qam.QamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.pusch.qam.clone()