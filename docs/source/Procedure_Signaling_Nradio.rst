Nradio
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.procedure.signaling.nradio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Procedure_Signaling_Nradio_Cell.rst
	Procedure_Signaling_Nradio_PdcchOrder.rst