Sprb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUCCh:SPRB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUCCh:SPRB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pucch.Sprb.SprbCls
	:members:
	:undoc-members:
	:noindex: