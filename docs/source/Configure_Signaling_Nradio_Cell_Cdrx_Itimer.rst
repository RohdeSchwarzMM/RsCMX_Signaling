Itimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:ITIMer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:ITIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Itimer.ItimerCls
	:members:
	:undoc-members:
	:noindex: