WoLanguage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:WOLanguage

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:WOLanguage



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.WoLanguage.WoLanguageCls
	:members:
	:undoc-members:
	:noindex: