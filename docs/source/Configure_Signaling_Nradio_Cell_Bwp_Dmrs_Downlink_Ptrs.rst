Ptrs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:DL:PTRS

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:DL:PTRS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Dmrs.Downlink.Ptrs.PtrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.dmrs.downlink.ptrs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Dmrs_Downlink_Ptrs_Enable.rst