Ri
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:NRADio:RI

.. code-block:: python

	FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:NRADio:RI



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.Trace.Nradio.Ri.RiCls
	:members:
	:undoc-members:
	:noindex: