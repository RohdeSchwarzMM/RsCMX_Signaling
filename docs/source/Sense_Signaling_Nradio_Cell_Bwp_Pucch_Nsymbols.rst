Nsymbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BWP<bpwid>:PUCCh:NSYMbols

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BWP<bpwid>:PUCCh:NSYMbols



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.Pucch.Nsymbols.NsymbolsCls
	:members:
	:undoc-members:
	:noindex: