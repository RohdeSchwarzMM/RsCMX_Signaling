DlOnly
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:MCONfig:DLONly

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:MCONfig:DLONly



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Mconfig.DlOnly.DlOnlyCls
	:members:
	:undoc-members:
	:noindex: