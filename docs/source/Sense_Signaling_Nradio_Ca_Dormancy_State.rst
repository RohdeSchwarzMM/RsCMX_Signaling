State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CA:DORMancy:STATe

.. code-block:: python

	SENSe:SIGNaling:NRADio:CA:DORMancy:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Ca.Dormancy.State.StateCls
	:members:
	:undoc-members:
	:noindex: