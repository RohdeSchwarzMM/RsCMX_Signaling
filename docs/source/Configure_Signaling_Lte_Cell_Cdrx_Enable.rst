Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: