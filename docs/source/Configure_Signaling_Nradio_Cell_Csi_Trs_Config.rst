Config
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CSI:TRS:CONFig

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CSI:TRS:CONFig



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Csi.Trs.Config.ConfigCls
	:members:
	:undoc-members:
	:noindex: