UeReport
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:ENABle
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:RINTerval

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:UEReport:ENABle
	[CONFigure]:SIGNaling:MEASurement:UEReport:RINTerval



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.UeReport.UeReportCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.measurement.ueReport.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Measurement_UeReport_Ncell.rst
	Configure_Signaling_Measurement_UeReport_Result.rst