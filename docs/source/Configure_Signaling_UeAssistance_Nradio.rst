Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.ueAssistance.nradio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_UeAssistance_Nradio_DbReport.rst
	Configure_Signaling_UeAssistance_Nradio_DrxPref.rst
	Configure_Signaling_UeAssistance_Nradio_MbwPref.rst
	Configure_Signaling_UeAssistance_Nradio_MccPref.rst
	Configure_Signaling_UeAssistance_Nradio_MmLayer.rst
	Configure_Signaling_UeAssistance_Nradio_MsOffset.rst
	Configure_Signaling_UeAssistance_Nradio_Oassistance.rst
	Configure_Signaling_UeAssistance_Nradio_RelPref.rst