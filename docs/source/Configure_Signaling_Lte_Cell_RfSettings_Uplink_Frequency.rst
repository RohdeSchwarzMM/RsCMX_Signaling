Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Uplink.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: