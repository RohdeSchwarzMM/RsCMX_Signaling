Tcomb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource:TCOMb

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource:TCOMb



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.CnCodebook.Resource.Tcomb.TcombCls
	:members:
	:undoc-members:
	:noindex: