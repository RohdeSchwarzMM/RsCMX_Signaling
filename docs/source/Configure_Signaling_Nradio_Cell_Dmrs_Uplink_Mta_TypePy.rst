TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:MTA:TYPE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:MTA:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Uplink.Mta.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: