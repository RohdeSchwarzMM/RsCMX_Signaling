Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:USER:RETRansm:MODulation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:USER:RETRansm:MODulation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Harq.Downlink.User.Retransm.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: