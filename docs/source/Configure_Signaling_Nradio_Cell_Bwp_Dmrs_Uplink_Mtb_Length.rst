Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:UL:MTB:LENGth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:UL:MTB:LENGth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Dmrs.Uplink.Mtb.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: