MccPref
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio:MCCPref

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio:MCCPref



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.MccPref.MccPrefCls
	:members:
	:undoc-members:
	:noindex: