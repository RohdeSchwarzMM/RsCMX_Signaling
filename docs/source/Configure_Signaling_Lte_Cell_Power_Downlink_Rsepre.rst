Rsepre
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:RSEPre

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:RSEPre



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Rsepre.RsepreCls
	:members:
	:undoc-members:
	:noindex: