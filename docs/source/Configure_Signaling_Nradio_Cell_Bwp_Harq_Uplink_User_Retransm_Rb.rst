Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:UL:USER:RETRansm:RB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:UL:USER:RETRansm:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Harq.Uplink.User.Retransm.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: