Earfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:EARFcn

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:EARFcn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.Earfcn.EarfcnCls
	:members:
	:undoc-members:
	:noindex: