Rs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:RS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:RS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Offset.Rs.RsCls
	:members:
	:undoc-members:
	:noindex: