Oassistance
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio:OASSistance

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio:OASSistance



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.Oassistance.OassistanceCls
	:members:
	:undoc-members:
	:noindex: