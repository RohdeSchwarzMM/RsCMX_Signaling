Barred
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BARRed

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BARRed



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Barred.BarredCls
	:members:
	:undoc-members:
	:noindex: