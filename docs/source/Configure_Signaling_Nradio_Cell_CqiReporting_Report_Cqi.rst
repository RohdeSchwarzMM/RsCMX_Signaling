Cqi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:REPort:CQI

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:REPort:CQI



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Report.Cqi.CqiCls
	:members:
	:undoc-members:
	:noindex: