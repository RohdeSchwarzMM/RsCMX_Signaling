UeCapability
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:EPS:UECapability:MODE
	single: [CONFigure]:SIGNaling:EPS:UECapability:SEGMentation

.. code-block:: python

	[CONFigure]:SIGNaling:EPS:UECapability:MODE
	[CONFigure]:SIGNaling:EPS:UECapability:SEGMentation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Eps.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.eps.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Eps_UeCapability_Eutra.rst
	Configure_Signaling_Eps_UeCapability_Mrdc.rst