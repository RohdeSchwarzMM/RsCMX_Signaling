Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: