Dormancy
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Dormancy.DormancyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.ca.scell.dormancy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Ca_Scell_Dormancy_Dbwp.rst
	Configure_Signaling_Nradio_Ca_Scell_Dormancy_Enable.rst
	Configure_Signaling_Nradio_Ca_Scell_Dormancy_NdBwp.rst