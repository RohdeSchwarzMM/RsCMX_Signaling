Switch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:DORMancy:SWITch

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:DORMancy:SWITch



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Dormancy.Switch.SwitchCls
	:members:
	:undoc-members:
	:noindex: