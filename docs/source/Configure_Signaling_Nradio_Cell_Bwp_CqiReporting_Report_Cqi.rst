Cqi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:REPort:CQI

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:REPort:CQI



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.CqiReporting.Report.Cqi.CqiCls
	:members:
	:undoc-members:
	:noindex: