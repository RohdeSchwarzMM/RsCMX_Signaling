Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: