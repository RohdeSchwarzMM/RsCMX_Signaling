Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:FGS:UECapability:MRDC:BANDs

.. code-block:: python

	DELete:SIGNaling:FGS:UECapability:MRDC:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Fgs.UeCapability.Mrdc.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: