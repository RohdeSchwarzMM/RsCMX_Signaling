Smode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SMODe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Smode.SmodeCls
	:members:
	:undoc-members:
	:noindex: