Codebook
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pusch.Tschema.Codebook.CodebookCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.pusch.tschema.codebook.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Pusch_Tschema_Codebook_FptMode.rst
	Configure_Signaling_Nradio_Cell_Pusch_Tschema_Codebook_Subset.rst