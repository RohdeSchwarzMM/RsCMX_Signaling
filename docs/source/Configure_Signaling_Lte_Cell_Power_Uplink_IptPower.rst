IptPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:IPTPower

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:IPTPower



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.IptPower.IptPowerCls
	:members:
	:undoc-members:
	:noindex: