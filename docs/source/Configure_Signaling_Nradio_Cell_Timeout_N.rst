N<Nnum>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr310 .. Nr311
	rc = driver.configure.signaling.nradio.cell.timeout.n.repcap_nnum_get()
	driver.configure.signaling.nradio.cell.timeout.n.repcap_nnum_set(repcap.Nnum.Nr310)



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TOUT:N<no>

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TOUT:N<no>



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Timeout.N.NCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.timeout.n.clone()