Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:EPS:UECapability:MRDC:BANDs

.. code-block:: python

	ADD:SIGNaling:EPS:UECapability:MRDC:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Eps.UeCapability.Mrdc.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: