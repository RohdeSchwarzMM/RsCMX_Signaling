State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:CQIReporting:STATe

.. code-block:: python

	FETCh:SIGNaling:MEASurement:CQIReporting:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.State.StateCls
	:members:
	:undoc-members:
	:noindex: