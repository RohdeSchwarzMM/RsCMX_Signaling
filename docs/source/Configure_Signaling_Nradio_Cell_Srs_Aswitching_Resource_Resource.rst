Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:RESource

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Aswitching.Resource.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: