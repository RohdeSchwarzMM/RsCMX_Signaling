Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:UL:PTRS:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:UL:PTRS:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Dmrs.Uplink.Ptrs.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: