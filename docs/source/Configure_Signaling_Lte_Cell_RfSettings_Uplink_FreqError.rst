FreqError
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:FERRor

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:FERRor



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Uplink.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: