Nsymbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:PUCCh:NSYMbols

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:PUCCh:NSYMbols



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Pucch.Nsymbols.NsymbolsCls
	:members:
	:undoc-members:
	:noindex: