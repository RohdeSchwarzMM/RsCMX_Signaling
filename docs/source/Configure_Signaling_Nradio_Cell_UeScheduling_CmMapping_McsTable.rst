McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:CMMapping:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:CMMapping:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.CmMapping.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: