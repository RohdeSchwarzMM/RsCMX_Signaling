Barred
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:BARRed

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:BARRed



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Barred.BarredCls
	:members:
	:undoc-members:
	:noindex: