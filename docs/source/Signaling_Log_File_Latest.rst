Latest
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:LOG:FILE:LATest

.. code-block:: python

	FETCh:SIGNaling:LOG:FILE:LATest



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Log.File.Latest.LatestCls
	:members:
	:undoc-members:
	:noindex: