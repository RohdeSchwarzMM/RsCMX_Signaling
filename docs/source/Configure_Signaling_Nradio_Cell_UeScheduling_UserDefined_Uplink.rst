Uplink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.ueScheduling.userDefined.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_Alevel.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_Bpid.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_McsTable.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_PaFactor.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_PnoRepet.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_Prtype.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Uplink_Ssid.rst