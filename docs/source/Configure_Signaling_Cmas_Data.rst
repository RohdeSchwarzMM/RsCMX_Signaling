Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:CMAS:DATA

.. code-block:: python

	[CONFigure]:SIGNaling:CMAS:DATA



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Cmas.Data.DataCls
	:members:
	:undoc-members:
	:noindex: