SpbPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:SPBPower

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:SPBPower



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Control.SpbPower.SpbPowerCls
	:members:
	:undoc-members:
	:noindex: