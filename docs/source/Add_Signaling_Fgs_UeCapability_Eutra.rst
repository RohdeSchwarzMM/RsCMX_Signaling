Eutra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:FGS:UECapability:EUTRa:BANDs

.. code-block:: python

	ADD:SIGNaling:FGS:UECapability:EUTRa:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Fgs.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: