CnCodebook
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Bwp.Srs.CnCodebook.CnCodebookCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.nradio.cell.bwp.srs.cnCodebook.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Resource.rst