Cgroup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:NRADio:CGRoup

.. code-block:: python

	DELete:SIGNaling:NRADio:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cgroup.CgroupCls
	:members:
	:undoc-members:
	:noindex: