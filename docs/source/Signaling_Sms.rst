Sms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CLEar:SIGNaling:SMS

.. code-block:: python

	CLEar:SIGNaling:SMS



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Sms.SmsCls
	:members:
	:undoc-members:
	:noindex: