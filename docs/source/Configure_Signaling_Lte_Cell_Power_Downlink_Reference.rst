Reference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:REFerence

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:REFerence



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex: