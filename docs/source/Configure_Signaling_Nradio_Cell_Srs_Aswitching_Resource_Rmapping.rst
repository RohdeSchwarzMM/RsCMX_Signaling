Rmapping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:RMAPping

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:RMAPping



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Aswitching.Resource.Rmapping.RmappingCls
	:members:
	:undoc-members:
	:noindex: