Retransm
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Harq.Uplink.User.Retransm.RetransmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.harq.uplink.user.retransm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Harq_Uplink_User_Retransm_Ariv.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Uplink_User_Retransm_Modulation.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Uplink_User_Retransm_Moffset.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Uplink_User_Retransm_Rb.rst
	Configure_Signaling_Nradio_Cell_Bwp_Harq_Uplink_User_Retransm_Rversion.rst