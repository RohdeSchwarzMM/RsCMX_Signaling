CsirsPorts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MCONfig:CSIRsports

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MCONfig:CSIRsports



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Mconfig.CsirsPorts.CsirsPortsCls
	:members:
	:undoc-members:
	:noindex: