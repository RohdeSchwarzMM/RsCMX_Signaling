Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:RMC:DL

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:RMC:DL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Rmc.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: