Cmeasure
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:BLER:CMEasure:CELLs
	single: [CONFigure]:SIGNaling:MEASurement:BLER:CMEasure

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:BLER:CMEasure:CELLs
	[CONFigure]:SIGNaling:MEASurement:BLER:CMEasure



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.Bler.Cmeasure.CmeasureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.measurement.bler.cmeasure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Measurement_Bler_Cmeasure_Cgroup.rst