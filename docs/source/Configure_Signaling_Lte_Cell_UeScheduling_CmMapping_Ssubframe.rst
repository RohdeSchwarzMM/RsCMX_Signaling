Ssubframe
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.CmMapping.Ssubframe.SsubframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.cmMapping.ssubframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_CmMapping_Ssubframe_Mcs.rst
	Configure_Signaling_Lte_Cell_UeScheduling_CmMapping_Ssubframe_McsTable.rst