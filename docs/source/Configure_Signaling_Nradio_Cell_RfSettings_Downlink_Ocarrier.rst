Ocarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:OCARrier

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:OCARrier



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Ocarrier.OcarrierCls
	:members:
	:undoc-members:
	:noindex: