Uplane
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:SIGNaling:FGS:LOGGing:UPLane:UL
	single: DIAGnostic:SIGNaling:FGS:LOGGing:UPLane:DL

.. code-block:: python

	DIAGnostic:SIGNaling:FGS:LOGGing:UPLane:UL
	DIAGnostic:SIGNaling:FGS:LOGGing:UPLane:DL



.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Fgs.Logging.Uplane.UplaneCls
	:members:
	:undoc-members:
	:noindex: