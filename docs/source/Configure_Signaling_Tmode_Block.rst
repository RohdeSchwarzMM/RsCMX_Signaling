Block
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:TMODe:BLOCk

.. code-block:: python

	[CONFigure]:SIGNaling:TMODe:BLOCk



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Tmode.Block.BlockCls
	:members:
	:undoc-members:
	:noindex: