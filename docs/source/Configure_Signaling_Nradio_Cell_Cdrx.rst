Cdrx
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.CdrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.cdrx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Cdrx_AaScheduler.rst
	Configure_Signaling_Nradio_Cell_Cdrx_All.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Downlink.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Enable.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Itimer.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Ldrx.rst
	Configure_Signaling_Nradio_Cell_Cdrx_OdTimer.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Sdrx.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Soffset.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Uplink.rst
	Configure_Signaling_Nradio_Cell_Cdrx_Wus.rst