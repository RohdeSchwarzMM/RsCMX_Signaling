Secondary
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.SecondaryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.etws.secondary.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Etws_Secondary_Cgroup.rst
	Configure_Signaling_Etws_Secondary_Data.rst
	Configure_Signaling_Etws_Secondary_Id.rst
	Configure_Signaling_Etws_Secondary_Language.rst
	Configure_Signaling_Etws_Secondary_Serial.rst
	Configure_Signaling_Etws_Secondary_Tranmission.rst
	Configure_Signaling_Etws_Secondary_WoLanguage.rst