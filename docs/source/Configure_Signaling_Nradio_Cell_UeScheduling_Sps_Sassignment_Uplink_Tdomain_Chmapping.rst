Chmapping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:SASSignment:UL:TDOMain:CHMapping

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:SASSignment:UL:TDOMain:CHMapping



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Sassignment.Uplink.Tdomain.Chmapping.ChmappingCls
	:members:
	:undoc-members:
	:noindex: