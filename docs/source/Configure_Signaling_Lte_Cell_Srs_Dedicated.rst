Dedicated
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Dedicated.DedicatedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.srs.dedicated.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Srs_Dedicated_Bandwidth.rst
	Configure_Signaling_Lte_Cell_Srs_Dedicated_Cindex.rst
	Configure_Signaling_Lte_Cell_Srs_Dedicated_Enable.rst
	Configure_Signaling_Lte_Cell_Srs_Dedicated_HbWidth.rst