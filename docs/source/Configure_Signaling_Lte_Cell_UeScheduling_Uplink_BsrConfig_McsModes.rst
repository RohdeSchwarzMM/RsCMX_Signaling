McsModes
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:MCSModes

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:MCSModes



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.BsrConfig.McsModes.McsModesCls
	:members:
	:undoc-members:
	:noindex: