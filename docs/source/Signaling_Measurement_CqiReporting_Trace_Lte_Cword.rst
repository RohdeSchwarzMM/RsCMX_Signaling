Cword<Cword>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.signaling.measurement.cqiReporting.trace.lte.cword.repcap_cword_get()
	driver.signaling.measurement.cqiReporting.trace.lte.cword.repcap_cword_set(repcap.Cword.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:LTE:CWORd<no>

.. code-block:: python

	FETCh:SIGNaling:MEASurement:CQIReporting:TRACe:LTE:CWORd<no>



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.Trace.Lte.Cword.CwordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.measurement.cqiReporting.trace.lte.cword.clone()