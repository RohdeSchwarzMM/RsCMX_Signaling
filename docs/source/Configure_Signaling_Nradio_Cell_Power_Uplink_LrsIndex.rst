LrsIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:LRSindex

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:LRSindex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.LrsIndex.LrsIndexCls
	:members:
	:undoc-members:
	:noindex: