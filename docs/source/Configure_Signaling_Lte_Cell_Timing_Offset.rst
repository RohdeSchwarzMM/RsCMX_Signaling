Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TIMing:OFFSet

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TIMing:OFFSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Timing.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: