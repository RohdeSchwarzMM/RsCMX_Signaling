Power
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Srs.CnCodebook.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.srs.cnCodebook.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Power_Alpha.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Power_Pzero.rst