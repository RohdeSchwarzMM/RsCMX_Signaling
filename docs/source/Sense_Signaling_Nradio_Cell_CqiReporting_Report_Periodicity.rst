Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:CQIReporting:REPort:PERiodicity

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:CQIReporting:REPort:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.CqiReporting.Report.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: