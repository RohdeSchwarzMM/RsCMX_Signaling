Tmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:TMODe:SSReport

.. code-block:: python

	SENSe:SIGNaling:TMODe:SSReport



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: