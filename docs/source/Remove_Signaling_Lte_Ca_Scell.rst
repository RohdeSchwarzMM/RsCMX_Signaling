Scell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: REMove:SIGNaling:LTE:CA:SCELl

.. code-block:: python

	REMove:SIGNaling:LTE:CA:SCELl



.. autoclass:: RsCMX_Signaling.Implementations.Remove.Signaling.Lte.Ca.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex: