Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:RESource:OFFSet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:RESource:OFFSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.CqiReporting.Resource.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: