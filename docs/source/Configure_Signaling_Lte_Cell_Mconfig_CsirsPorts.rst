CsirsPorts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:MCONfig:CSIRsports

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:MCONfig:CSIRsports



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Mconfig.CsirsPorts.CsirsPortsCls
	:members:
	:undoc-members:
	:noindex: