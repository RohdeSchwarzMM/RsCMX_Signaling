Sms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:SMS

.. code-block:: python

	PROCedure:SIGNaling:SMS



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Sms.SmsCls
	:members:
	:undoc-members:
	:noindex: