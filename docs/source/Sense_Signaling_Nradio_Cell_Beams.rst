Beams
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Beams.BeamsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.signaling.nradio.cell.beams.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Signaling_Nradio_Cell_Beams_ActiveBeam.rst