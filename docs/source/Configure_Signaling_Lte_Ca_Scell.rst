Scell
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ca.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.ca.scell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Ca_Scell_Activation.rst
	Configure_Signaling_Lte_Ca_Scell_Uplink.rst