Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm:MODulation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm:MODulation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Downlink.User.Retransm.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: