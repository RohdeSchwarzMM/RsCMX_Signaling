TpDisable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:PTRS:TPDisable

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:PTRS:TPDisable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Uplink.Ptrs.TpDisable.TpDisableCls
	:members:
	:undoc-members:
	:noindex: