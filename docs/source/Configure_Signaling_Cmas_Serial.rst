Serial
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:CMAS:SERial

.. code-block:: python

	[CONFigure]:SIGNaling:CMAS:SERial



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Cmas.Serial.SerialCls
	:members:
	:undoc-members:
	:noindex: