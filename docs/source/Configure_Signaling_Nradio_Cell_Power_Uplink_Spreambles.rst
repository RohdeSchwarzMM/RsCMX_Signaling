Spreambles
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:SPReambles

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:SPReambles



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.Spreambles.SpreamblesCls
	:members:
	:undoc-members:
	:noindex: