Trs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:CSI:TRS

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:CSI:TRS



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Bwp.Csi.Trs.TrsCls
	:members:
	:undoc-members:
	:noindex: