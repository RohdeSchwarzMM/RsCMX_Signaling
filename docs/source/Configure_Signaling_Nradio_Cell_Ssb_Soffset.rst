Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:SOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:SOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: