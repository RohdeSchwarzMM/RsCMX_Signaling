Meepre
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:MEEPre

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:MEEPre



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.Meepre.MeepreCls
	:members:
	:undoc-members:
	:noindex: