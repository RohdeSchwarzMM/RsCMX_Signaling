Retransm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:USER:RETRansm

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:USER:RETRansm



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Bwp.Harq.Downlink.User.Retransm.RetransmCls
	:members:
	:undoc-members:
	:noindex: