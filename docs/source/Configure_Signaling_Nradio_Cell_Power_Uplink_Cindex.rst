Cindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:CINDex

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:CINDex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: