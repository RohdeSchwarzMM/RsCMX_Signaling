Nbehavior
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NBEHavior:DITimer

.. code-block:: python

	[CONFigure]:SIGNaling:NBEHavior:DITimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nbehavior.NbehaviorCls
	:members:
	:undoc-members:
	:noindex: