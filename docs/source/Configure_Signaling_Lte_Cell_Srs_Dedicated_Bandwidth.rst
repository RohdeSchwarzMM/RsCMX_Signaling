Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:DEDicated:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:DEDicated:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Dedicated.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: