ReTx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:HARQ:DL:RETX

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:HARQ:DL:RETX



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Harq.Downlink.ReTx.ReTxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.harq.downlink.reTx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Harq_Downlink_ReTx_Behavior.rst
	Configure_Signaling_Lte_Cell_Harq_Downlink_ReTx_Maximum.rst