PrEnable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CQIReporting:PRENable

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CQIReporting:PRENable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.CqiReporting.PrEnable.PrEnableCls
	:members:
	:undoc-members:
	:noindex: