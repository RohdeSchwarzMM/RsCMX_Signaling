Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.CqiReporting.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: