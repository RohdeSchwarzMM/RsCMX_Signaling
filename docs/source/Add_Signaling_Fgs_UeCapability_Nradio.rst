Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:FGS:UECapability:NRADio:BANDs

.. code-block:: python

	ADD:SIGNaling:FGS:UECapability:NRADio:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Fgs.UeCapability.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex: