Sspacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSPacing

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSPacing



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Sspacing.SspacingCls
	:members:
	:undoc-members:
	:noindex: