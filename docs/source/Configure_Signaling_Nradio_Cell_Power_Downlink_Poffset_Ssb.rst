Ssb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:POFFset:SSB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:POFFset:SSB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Poffset.Ssb.SsbCls
	:members:
	:undoc-members:
	:noindex: