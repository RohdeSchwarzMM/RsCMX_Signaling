TciStates
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:BEAM:TCIStates:UPDate

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:BEAM:TCIStates:UPDate



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Beam.TciStates.TciStatesCls
	:members:
	:undoc-members:
	:noindex: