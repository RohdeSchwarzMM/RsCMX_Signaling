PdcchFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:PDCChformat

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:PDCChformat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.PdcchFormat.PdcchFormatCls
	:members:
	:undoc-members:
	:noindex: