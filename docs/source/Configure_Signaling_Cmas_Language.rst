Language
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:CMAS:LANGuage

.. code-block:: python

	[CONFigure]:SIGNaling:CMAS:LANGuage



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Cmas.Language.LanguageCls
	:members:
	:undoc-members:
	:noindex: