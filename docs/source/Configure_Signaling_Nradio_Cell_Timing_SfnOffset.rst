SfnOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TIMing:SFNoffset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TIMing:SFNoffset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Timing.SfnOffset.SfnOffsetCls
	:members:
	:undoc-members:
	:noindex: