Trs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:NRADio:CELL:BWP<bwp_id>:CSI:TRS

.. code-block:: python

	DELete:SIGNaling:NRADio:CELL:BWP<bwp_id>:CSI:TRS



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Bwp.Csi.Trs.TrsCls
	:members:
	:undoc-members:
	:noindex: