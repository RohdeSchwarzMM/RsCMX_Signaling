Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:UL:USER:RETRansm:MODulation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:UL:USER:RETRansm:MODulation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Harq.Uplink.User.Retransm.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: