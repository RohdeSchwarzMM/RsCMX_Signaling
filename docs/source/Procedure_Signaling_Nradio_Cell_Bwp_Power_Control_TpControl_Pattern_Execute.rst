Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:NRADio:CELL:BWP<bpwid>:POWer:CONTrol:TPControl:PATTern:EXECute

.. code-block:: python

	PROCedure:SIGNaling:NRADio:CELL:BWP<bpwid>:POWer:CONTrol:TPControl:PATTern:EXECute



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Nradio.Cell.Bwp.Power.Control.TpControl.Pattern.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: