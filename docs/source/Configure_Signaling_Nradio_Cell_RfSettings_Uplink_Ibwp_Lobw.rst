Lobw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:IBWP:LOBW

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:IBWP:LOBW



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Ibwp.Lobw.LobwCls
	:members:
	:undoc-members:
	:noindex: