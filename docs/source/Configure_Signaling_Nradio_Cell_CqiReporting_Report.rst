Report
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Report.ReportCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.cqiReporting.report.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_CqiReporting_Report_Cqi.rst
	Configure_Signaling_Nradio_Cell_CqiReporting_Report_Offset.rst
	Configure_Signaling_Nradio_Cell_CqiReporting_Report_Pmi.rst