Thresholds
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:NCELl:THResholds

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:NCELl:THResholds



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ncell.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: