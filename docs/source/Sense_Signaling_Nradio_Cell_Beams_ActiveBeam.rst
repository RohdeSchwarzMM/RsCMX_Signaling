ActiveBeam
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BEAMs:ACTivebeam

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BEAMs:ACTivebeam



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Beams.ActiveBeam.ActiveBeamCls
	:members:
	:undoc-members:
	:noindex: