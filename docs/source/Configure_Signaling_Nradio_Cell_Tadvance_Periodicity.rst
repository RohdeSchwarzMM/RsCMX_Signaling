Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TADVance:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TADVance:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tadvance.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: