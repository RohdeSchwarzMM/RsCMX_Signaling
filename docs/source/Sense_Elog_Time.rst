Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:ELOG:TIME

.. code-block:: python

	SENSe:ELOG:TIME



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Elog.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: