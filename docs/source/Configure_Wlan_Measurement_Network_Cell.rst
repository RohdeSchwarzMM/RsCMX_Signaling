Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:WLAN:MEASurement<Instance>:NETWork:CELL

.. code-block:: python

	[CONFigure]:WLAN:MEASurement<Instance>:NETWork:CELL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Wlan.Measurement.Network.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: