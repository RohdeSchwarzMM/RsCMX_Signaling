Thousandth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:BLER:DL:THOusandth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:BLER:DL:THOusandth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Bler.Downlink.Thousandth.ThousandthCls
	:members:
	:undoc-members:
	:noindex: