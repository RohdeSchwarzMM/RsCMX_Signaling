ScTimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:SDRX:SCTimer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:SDRX:SCTimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Sdrx.ScTimer.ScTimerCls
	:members:
	:undoc-members:
	:noindex: