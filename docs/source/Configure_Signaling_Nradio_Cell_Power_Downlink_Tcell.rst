Tcell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:TCELl

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:TCELl



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Tcell.TcellCls
	:members:
	:undoc-members:
	:noindex: