Aswitching
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Srs.Aswitching.AswitchingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.srs.aswitching.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Enable.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Power.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_Resource.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_Aswitching_TypePy.rst