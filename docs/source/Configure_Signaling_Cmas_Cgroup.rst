Cgroup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:CMAS:CGRoup

.. code-block:: python

	[CONFigure]:SIGNaling:CMAS:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Cmas.Cgroup.CgroupCls
	:members:
	:undoc-members:
	:noindex: