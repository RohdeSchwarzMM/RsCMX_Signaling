DciFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:SASSignment:DL:DCIFormat

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:SASSignment:DL:DCIFormat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Sassignment.Downlink.DciFormat.DciFormatCls
	:members:
	:undoc-members:
	:noindex: