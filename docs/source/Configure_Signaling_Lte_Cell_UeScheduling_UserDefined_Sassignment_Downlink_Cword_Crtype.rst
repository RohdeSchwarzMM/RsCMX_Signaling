Crtype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:DL:CWORd<no>:CRTYpe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:DL:CWORd<no>:CRTYpe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Downlink.Cword.Crtype.CrtypeCls
	:members:
	:undoc-members:
	:noindex: