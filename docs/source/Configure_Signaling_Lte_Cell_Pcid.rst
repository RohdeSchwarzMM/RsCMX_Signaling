Pcid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:PCID

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:PCID



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Pcid.PcidCls
	:members:
	:undoc-members:
	:noindex: