Resume
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:UE:RRC:RESume

.. code-block:: python

	PROCedure:SIGNaling:UE:RRC:RESume



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Ue.Rrc.Resume.ResumeCls
	:members:
	:undoc-members:
	:noindex: