Eutra
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:EPS:UECapability:EUTRa:RFORmat
	single: [CONFigure]:SIGNaling:EPS:UECapability:EUTRa:SFC

.. code-block:: python

	[CONFigure]:SIGNaling:EPS:UECapability:EUTRa:RFORmat
	[CONFigure]:SIGNaling:EPS:UECapability:EUTRa:SFC



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Eps.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: