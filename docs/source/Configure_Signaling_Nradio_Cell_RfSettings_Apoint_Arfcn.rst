Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:APOint:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:APOint:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Apoint.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: