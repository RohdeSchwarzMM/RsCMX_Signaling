Block
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAM:FOLLowing:BLOCk

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAM:FOLLowing:BLOCk



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Following.Block.BlockCls
	:members:
	:undoc-members:
	:noindex: