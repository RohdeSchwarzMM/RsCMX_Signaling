UeCapability
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Eps.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.signaling.eps.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Signaling_Eps_UeCapability_Eutra.rst
	Catalog_Signaling_Eps_UeCapability_Mrdc.rst
	Catalog_Signaling_Eps_UeCapability_Nradio.rst