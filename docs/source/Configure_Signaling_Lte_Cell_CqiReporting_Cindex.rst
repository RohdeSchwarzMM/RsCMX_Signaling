Cindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CQIReporting:CINDex

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CQIReporting:CINDex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.CqiReporting.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: