TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:UESCheduling:DYNamic:TYPE

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:UESCheduling:DYNamic:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.UeScheduling.Dynamic.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: