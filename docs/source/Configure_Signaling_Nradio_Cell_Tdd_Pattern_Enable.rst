Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.Pattern.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: