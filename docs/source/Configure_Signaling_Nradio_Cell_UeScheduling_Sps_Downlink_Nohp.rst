Nohp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:DL:NOHP

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:DL:NOHP



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Downlink.Nohp.NohpCls
	:members:
	:undoc-members:
	:noindex: