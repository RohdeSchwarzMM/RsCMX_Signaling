PibPsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUSCh:DTFS:PIBPsk

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUSCh:DTFS:PIBPsk



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pusch.Dtfs.PibPsk.PibPskCls
	:members:
	:undoc-members:
	:noindex: