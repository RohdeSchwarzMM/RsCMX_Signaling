SsZero
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CSSZero:SSZero

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CSSZero:SSZero



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CssZero.SsZero.SsZeroCls
	:members:
	:undoc-members:
	:noindex: