AsMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:ASMode

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:ASMode



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.AsMode.AsModeCls
	:members:
	:undoc-members:
	:noindex: