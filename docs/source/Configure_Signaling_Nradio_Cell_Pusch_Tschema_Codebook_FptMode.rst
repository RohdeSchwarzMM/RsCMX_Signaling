FptMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUSCh:TSCHema:CODebook:FPTMode

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUSCh:TSCHema:CODebook:FPTMode



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pusch.Tschema.Codebook.FptMode.FptModeCls
	:members:
	:undoc-members:
	:noindex: