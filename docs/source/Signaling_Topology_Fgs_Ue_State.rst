State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:TOPology:FGS:UE:STATe

.. code-block:: python

	FETCh:SIGNaling:TOPology:FGS:UE:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Topology.Fgs.Ue.State.StateCls
	:members:
	:undoc-members:
	:noindex: