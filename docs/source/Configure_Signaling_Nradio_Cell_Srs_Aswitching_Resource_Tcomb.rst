Tcomb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:TCOMb

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:TCOMb



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Aswitching.Resource.Tcomb.TcombCls
	:members:
	:undoc-members:
	:noindex: