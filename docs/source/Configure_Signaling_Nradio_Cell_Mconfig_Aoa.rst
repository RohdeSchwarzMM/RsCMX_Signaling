Aoa
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MCONfig:AOA

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MCONfig:AOA



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Mconfig.Aoa.AoaCls
	:members:
	:undoc-members:
	:noindex: