Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:APOint:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:APOint:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Apoint.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: