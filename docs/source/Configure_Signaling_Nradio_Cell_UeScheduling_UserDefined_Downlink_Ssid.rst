Ssid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:DL:SSID

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:DL:SSID



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Downlink.Ssid.SsidCls
	:members:
	:undoc-members:
	:noindex: