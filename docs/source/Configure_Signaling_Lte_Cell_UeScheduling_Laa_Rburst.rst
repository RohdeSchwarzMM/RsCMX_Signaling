Rburst
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.Rburst.RburstCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.laa.rburst.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_All.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_Blength.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_BtProb.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_Ccrnti.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_IpsProb.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_Pbtr.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Rburst_PlSubframe.rst