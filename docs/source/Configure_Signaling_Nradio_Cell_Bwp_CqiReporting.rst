CqiReporting
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.CqiReporting.CqiReportingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.cqiReporting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_CqiReporting_Combined.rst
	Configure_Signaling_Nradio_Cell_Bwp_CqiReporting_Enable.rst
	Configure_Signaling_Nradio_Cell_Bwp_CqiReporting_Periodicity.rst
	Configure_Signaling_Nradio_Cell_Bwp_CqiReporting_Report.rst
	Configure_Signaling_Nradio_Cell_Bwp_CqiReporting_Resource.rst