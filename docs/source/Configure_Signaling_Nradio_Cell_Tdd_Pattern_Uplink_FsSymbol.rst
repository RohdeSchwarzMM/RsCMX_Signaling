FsSymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:UL:FSSYmbol

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:UL:FSSYmbol



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.Pattern.Uplink.FsSymbol.FsSymbolCls
	:members:
	:undoc-members:
	:noindex: