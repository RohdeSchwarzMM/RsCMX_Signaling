Measurement<MeasInstance>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.nrMmw.measurement.repcap_measInstance_get()
	driver.configure.nrMmw.measurement.repcap_measInstance_set(repcap.MeasInstance.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Configure.NrMmw.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrMmw.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrMmw_Measurement_Network.rst