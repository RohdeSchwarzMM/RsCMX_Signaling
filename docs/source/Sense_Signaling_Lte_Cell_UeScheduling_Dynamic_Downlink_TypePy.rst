TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:LTE:CELL:UESCheduling:DYNamic:DL:TYPE

.. code-block:: python

	SENSe:SIGNaling:LTE:CELL:UESCheduling:DYNamic:DL:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Lte.Cell.UeScheduling.Dynamic.Downlink.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: