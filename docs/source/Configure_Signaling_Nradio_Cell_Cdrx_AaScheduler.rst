AaScheduler
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:AASCheduler

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:AASCheduler



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.AaScheduler.AaSchedulerCls
	:members:
	:undoc-members:
	:noindex: