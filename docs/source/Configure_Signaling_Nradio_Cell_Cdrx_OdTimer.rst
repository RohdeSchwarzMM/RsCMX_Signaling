OdTimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:ODTimer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:ODTimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.OdTimer.OdTimerCls
	:members:
	:undoc-members:
	:noindex: