Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:OVERall:RELative

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:OVERall:RELative



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Overall.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: