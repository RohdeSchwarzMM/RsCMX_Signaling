Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:FGS:UECapability:NRADio:BANDs

.. code-block:: python

	DELete:SIGNaling:FGS:UECapability:NRADio:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Fgs.UeCapability.Nradio.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: