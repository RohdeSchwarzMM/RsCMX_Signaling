McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:DL:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:DL:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Downlink.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: