Eutra
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:FGS:UECapability:EUTRa:RFORmat
	single: [CONFigure]:SIGNaling:FGS:UECapability:EUTRa:SFC

.. code-block:: python

	[CONFigure]:SIGNaling:FGS:UECapability:EUTRa:RFORmat
	[CONFigure]:SIGNaling:FGS:UECapability:EUTRa:SFC



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fgs.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: