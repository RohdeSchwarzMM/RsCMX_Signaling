Sspacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:SSPacing

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:SSPacing



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Sspacing.SspacingCls
	:members:
	:undoc-members:
	:noindex: