Cycle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:SDRX:CYCLe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:SDRX:CYCLe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Sdrx.Cycle.CycleCls
	:members:
	:undoc-members:
	:noindex: