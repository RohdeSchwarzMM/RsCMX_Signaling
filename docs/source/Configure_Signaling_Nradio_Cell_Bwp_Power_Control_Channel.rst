Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:POWer:CONTrol:CHANnel

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:POWer:CONTrol:CHANnel



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Power.Control.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: