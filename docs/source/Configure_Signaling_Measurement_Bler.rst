Bler
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:BLER:REPetition
	single: [CONFigure]:SIGNaling:MEASurement:BLER:SMAVerage

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:BLER:REPetition
	[CONFigure]:SIGNaling:MEASurement:BLER:SMAVerage



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.Bler.BlerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.measurement.bler.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Measurement_Bler_Cmeasure.rst
	Configure_Signaling_Measurement_Bler_Enable.rst
	Configure_Signaling_Measurement_Bler_Scondition.rst