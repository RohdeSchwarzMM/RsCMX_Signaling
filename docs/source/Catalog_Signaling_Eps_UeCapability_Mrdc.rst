Mrdc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:EPS:UECapability:MRDC:BANDs

.. code-block:: python

	CATalog:SIGNaling:EPS:UECapability:MRDC:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Eps.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex: