Tpower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:TPControl:CLOop:TPOWer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:TPControl:CLOop:TPOWer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Control.TpControl.Cloop.Tpower.TpowerCls
	:members:
	:undoc-members:
	:noindex: