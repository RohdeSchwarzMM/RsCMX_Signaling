Mnc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:TOPology:PLMN:MNC

.. code-block:: python

	[CONFigure]:SIGNaling:TOPology:PLMN:MNC



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Topology.Plmn.Mnc.MncCls
	:members:
	:undoc-members:
	:noindex: