Rb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm:RB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm:RB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Downlink.User.Retransm.Rb.RbCls
	:members:
	:undoc-members:
	:noindex: