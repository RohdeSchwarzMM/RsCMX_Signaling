McsBehavior
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:HARQ:DL:MCSBehavior

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:HARQ:DL:MCSBehavior



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Harq.Downlink.McsBehavior.McsBehaviorCls
	:members:
	:undoc-members:
	:noindex: