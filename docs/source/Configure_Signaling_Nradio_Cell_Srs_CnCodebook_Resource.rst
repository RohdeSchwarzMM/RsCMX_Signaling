Resource
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.CnCodebook.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.srs.cnCodebook.resource.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Srs_CnCodebook_Resource_Fhopping.rst
	Configure_Signaling_Nradio_Cell_Srs_CnCodebook_Resource_Resource.rst
	Configure_Signaling_Nradio_Cell_Srs_CnCodebook_Resource_Rmapping.rst
	Configure_Signaling_Nradio_Cell_Srs_CnCodebook_Resource_Rtype.rst
	Configure_Signaling_Nradio_Cell_Srs_CnCodebook_Resource_Tcomb.rst