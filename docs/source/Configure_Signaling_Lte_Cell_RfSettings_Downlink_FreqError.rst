FreqError
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:FERRor

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:FERRor



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: