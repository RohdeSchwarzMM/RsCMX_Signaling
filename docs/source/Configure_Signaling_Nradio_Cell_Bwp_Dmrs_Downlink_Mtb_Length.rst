Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:DL:MTB:LENGth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:DMRS:DL:MTB:LENGth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Dmrs.Downlink.Mtb.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: