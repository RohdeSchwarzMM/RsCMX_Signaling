T<Tnum>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr300 .. Nr319
	rc = driver.configure.signaling.topology.fgs.timer.t.repcap_tnum_get()
	driver.configure.signaling.topology.fgs.timer.t.repcap_tnum_set(repcap.Tnum.Nr300)



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:TOPology:FGS:TIMer:T<no>

.. code-block:: python

	[CONFigure]:SIGNaling:TOPology:FGS:TIMer:T<no>



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Topology.Fgs.Timer.T.TCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.topology.fgs.timer.t.clone()