Ports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:PORTs

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:PORTs



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Resource.Ports.PortsCls
	:members:
	:undoc-members:
	:noindex: