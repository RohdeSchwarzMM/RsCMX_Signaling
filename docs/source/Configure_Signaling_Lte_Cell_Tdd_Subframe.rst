Subframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TDD:SUBFrame

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TDD:SUBFrame



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Tdd.Subframe.SubframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.tdd.subframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Tdd_Subframe_Assignment.rst
	Configure_Signaling_Lte_Cell_Tdd_Subframe_Special.rst