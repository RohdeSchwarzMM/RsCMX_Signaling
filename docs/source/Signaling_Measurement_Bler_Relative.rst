Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:RELative

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:RELative



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: