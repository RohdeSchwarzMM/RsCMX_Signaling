Lte
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:LTE:CGRoup

.. code-block:: python

	CREate:SIGNaling:LTE:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Lte.LteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.signaling.lte.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_Signaling_Lte_Cell.rst
	Create_Signaling_Lte_Vcell.rst