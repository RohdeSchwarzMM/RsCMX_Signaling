Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:COMBined:CFRequency:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:COMBined:CFRequency:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Combined.Cfrequency.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: