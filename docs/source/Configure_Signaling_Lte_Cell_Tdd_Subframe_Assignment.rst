Assignment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TDD:SUBFrame:ASSignment

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TDD:SUBFrame:ASSignment



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Tdd.Subframe.Assignment.AssignmentCls
	:members:
	:undoc-members:
	:noindex: