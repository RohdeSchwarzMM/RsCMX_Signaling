LbWidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DL:LBWidth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DL:LBWidth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Downlink.LbWidth.LbWidthCls
	:members:
	:undoc-members:
	:noindex: