Antenna
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:ANTenna

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:ANTenna



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Antenna_Beamforming.rst
	Configure_Signaling_Lte_Cell_Antenna_CrSports.rst
	Configure_Signaling_Lte_Cell_Antenna_Streams.rst