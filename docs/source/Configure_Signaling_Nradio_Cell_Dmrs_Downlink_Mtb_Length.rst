Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTB:LENGth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTB:LENGth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.Mtb.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: