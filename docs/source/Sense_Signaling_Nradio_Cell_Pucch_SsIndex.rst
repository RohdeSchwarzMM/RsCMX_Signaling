SsIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:PUCCh:SSINdex

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:PUCCh:SSINdex



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Pucch.SsIndex.SsIndexCls
	:members:
	:undoc-members:
	:noindex: