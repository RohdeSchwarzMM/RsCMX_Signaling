Measurement<MeasInstance>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.lte.measurement.repcap_measInstance_get()
	driver.configure.lte.measurement.repcap_measInstance_set(repcap.MeasInstance.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Lte.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lte.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Lte_Measurement_Network.rst