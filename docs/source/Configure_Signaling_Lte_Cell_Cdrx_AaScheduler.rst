AaScheduler
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:AASCheduler

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:AASCheduler



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.AaScheduler.AaSchedulerCls
	:members:
	:undoc-members:
	:noindex: