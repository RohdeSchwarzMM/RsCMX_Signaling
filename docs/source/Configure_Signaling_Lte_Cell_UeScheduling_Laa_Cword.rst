Cword<Cword>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.signaling.lte.cell.ueScheduling.laa.cword.repcap_cword_get()
	driver.configure.signaling.lte.cell.ueScheduling.laa.cword.repcap_cword_set(repcap.Cword.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.Cword.CwordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.laa.cword.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Cword_Mcs.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Laa_Cword_Modulation.rst