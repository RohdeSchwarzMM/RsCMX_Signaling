Morder
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:MORDer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:MORDer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.BsrConfig.Morder.MorderCls
	:members:
	:undoc-members:
	:noindex: