Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Lte.Cell.UeScheduling.Dynamic.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.signaling.lte.cell.ueScheduling.dynamic.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Signaling_Lte_Cell_UeScheduling_Dynamic_Downlink_TypePy.rst