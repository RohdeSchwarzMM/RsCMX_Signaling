Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:NSSB:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:NSSB:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Nssb.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: