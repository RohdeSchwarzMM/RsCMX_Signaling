DciFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:UL:DCIFormat

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:UL:DCIFormat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Uplink.DciFormat.DciFormatCls
	:members:
	:undoc-members:
	:noindex: