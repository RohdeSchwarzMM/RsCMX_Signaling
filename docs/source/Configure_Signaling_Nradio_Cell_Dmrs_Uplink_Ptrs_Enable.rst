Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:PTRS:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:PTRS:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Uplink.Ptrs.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: