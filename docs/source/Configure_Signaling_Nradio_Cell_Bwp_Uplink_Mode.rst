Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:UL:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwpid>:UL:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Uplink.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: