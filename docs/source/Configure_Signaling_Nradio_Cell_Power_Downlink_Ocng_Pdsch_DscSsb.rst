DscSsb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDSCh:DSCSsb

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDSCh:DSCSsb



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.Pdsch.DscSsb.DscSsbCls
	:members:
	:undoc-members:
	:noindex: