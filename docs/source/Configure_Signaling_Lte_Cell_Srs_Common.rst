Common
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Common.CommonCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.srs.common.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Srs_Common_Bandwidth.rst
	Configure_Signaling_Lte_Cell_Srs_Common_Sant.rst
	Configure_Signaling_Lte_Cell_Srs_Common_Sframe.rst