DrxPref
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio:DRXPref

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio:DRXPref



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.DrxPref.DrxPrefCls
	:members:
	:undoc-members:
	:noindex: