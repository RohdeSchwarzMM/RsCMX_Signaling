Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:COMMon:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:COMMon:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Common.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: