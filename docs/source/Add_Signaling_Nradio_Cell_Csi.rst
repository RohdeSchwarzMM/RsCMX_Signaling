Csi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:CSI:TRS

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:CSI:TRS



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Csi.CsiCls
	:members:
	:undoc-members:
	:noindex: