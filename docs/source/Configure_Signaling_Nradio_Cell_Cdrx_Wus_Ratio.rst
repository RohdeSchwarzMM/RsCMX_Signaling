Ratio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:WUS:RATio

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:WUS:RATio



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Wus.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex: