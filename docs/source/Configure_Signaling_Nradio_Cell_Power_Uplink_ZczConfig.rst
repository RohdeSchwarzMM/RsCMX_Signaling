ZczConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:ZCZConfig

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:ZCZConfig



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.ZczConfig.ZczConfigCls
	:members:
	:undoc-members:
	:noindex: