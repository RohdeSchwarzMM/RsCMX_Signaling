Tranmission
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:TRANmission

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:TRANmission



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Tranmission.TranmissionCls
	:members:
	:undoc-members:
	:noindex: