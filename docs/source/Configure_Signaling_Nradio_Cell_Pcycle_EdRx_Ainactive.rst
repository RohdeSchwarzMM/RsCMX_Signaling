Ainactive
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PCYCle:EDRX:AINactive

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PCYCle:EDRX:AINactive



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pcycle.EdRx.Ainactive.AinactiveCls
	:members:
	:undoc-members:
	:noindex: