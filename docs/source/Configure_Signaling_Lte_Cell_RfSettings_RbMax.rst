RbMax
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:RBMax

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:RBMax



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.RbMax.RbMaxCls
	:members:
	:undoc-members:
	:noindex: