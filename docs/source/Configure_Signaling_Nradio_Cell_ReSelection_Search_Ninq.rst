Ninq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RESelection:SEARch:NINQ

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RESelection:SEARch:NINQ



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.ReSelection.Search.Ninq.NinqCls
	:members:
	:undoc-members:
	:noindex: