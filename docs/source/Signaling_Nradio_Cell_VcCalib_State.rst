State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:NRADio:CELL:VCCalib:STATe

.. code-block:: python

	FETCh:SIGNaling:NRADio:CELL:VCCalib:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.VcCalib.State.StateCls
	:members:
	:undoc-members:
	:noindex: