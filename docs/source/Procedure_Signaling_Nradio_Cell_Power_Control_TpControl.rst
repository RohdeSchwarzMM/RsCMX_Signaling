TpControl
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Nradio.Cell.Power.Control.TpControl.TpControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.procedure.signaling.nradio.cell.power.control.tpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Procedure_Signaling_Nradio_Cell_Power_Control_TpControl_Pattern.rst
	Procedure_Signaling_Nradio_Cell_Power_Control_TpControl_RpTolerance.rst