Dbwp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:DBWP

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:DBWP



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Dormancy.Dbwp.DbwpCls
	:members:
	:undoc-members:
	:noindex: