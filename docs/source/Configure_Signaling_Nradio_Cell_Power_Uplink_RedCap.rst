RedCap
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:REDCap

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:REDCap



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.RedCap.RedCapCls
	:members:
	:undoc-members:
	:noindex: