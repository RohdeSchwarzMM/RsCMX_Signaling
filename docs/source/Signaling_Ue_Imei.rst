Imei
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:UE:IMEI

.. code-block:: python

	FETCh:SIGNaling:UE:IMEI



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Ue.Imei.ImeiCls
	:members:
	:undoc-members:
	:noindex: