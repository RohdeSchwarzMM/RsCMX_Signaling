NbBeams
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAMs:NBBeams

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAMs:NBBeams



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beams.NbBeams.NbBeamsCls
	:members:
	:undoc-members:
	:noindex: