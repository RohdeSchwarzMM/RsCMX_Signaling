Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:SOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:SOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: