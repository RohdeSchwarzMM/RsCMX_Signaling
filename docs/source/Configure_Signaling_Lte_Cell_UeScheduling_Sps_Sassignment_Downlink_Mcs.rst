Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:DL:MCS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:DL:MCS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Sps.Sassignment.Downlink.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: