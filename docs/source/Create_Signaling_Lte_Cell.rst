Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:LTE:CELL

.. code-block:: python

	CREate:SIGNaling:LTE:CELL



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Lte.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: