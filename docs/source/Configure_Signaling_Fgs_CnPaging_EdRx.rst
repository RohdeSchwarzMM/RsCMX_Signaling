EdRx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:FGS:CNPaging:EDRX:MODE
	single: [CONFigure]:SIGNaling:FGS:CNPaging:EDRX:EPTWindow
	single: [CONFigure]:SIGNaling:FGS:CNPaging:EDRX:CYCLe

.. code-block:: python

	[CONFigure]:SIGNaling:FGS:CNPaging:EDRX:MODE
	[CONFigure]:SIGNaling:FGS:CNPaging:EDRX:EPTWindow
	[CONFigure]:SIGNaling:FGS:CNPaging:EDRX:CYCLe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fgs.CnPaging.EdRx.EdRxCls
	:members:
	:undoc-members:
	:noindex: