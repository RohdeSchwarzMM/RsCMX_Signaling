Timer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RESelection:TIMer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RESelection:TIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.ReSelection.Timer.TimerCls
	:members:
	:undoc-members:
	:noindex: