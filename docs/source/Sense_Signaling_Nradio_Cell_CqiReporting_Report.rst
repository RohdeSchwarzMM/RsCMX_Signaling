Report
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.CqiReporting.Report.ReportCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.signaling.nradio.cell.cqiReporting.report.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Signaling_Nradio_Cell_CqiReporting_Report_Periodicity.rst
	Sense_Signaling_Nradio_Cell_CqiReporting_Report_Quantity.rst