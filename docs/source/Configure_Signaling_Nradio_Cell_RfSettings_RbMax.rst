RbMax
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:RBMax

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:RBMax



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.RbMax.RbMaxCls
	:members:
	:undoc-members:
	:noindex: