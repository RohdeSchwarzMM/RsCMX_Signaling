Profile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:FADing:PROFile

.. code-block:: python

	[CONFigure]:SIGNaling:FADing:PROFile



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fading.Profile.ProfileCls
	:members:
	:undoc-members:
	:noindex: