Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:LDRX:SOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:LDRX:SOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Ldrx.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: