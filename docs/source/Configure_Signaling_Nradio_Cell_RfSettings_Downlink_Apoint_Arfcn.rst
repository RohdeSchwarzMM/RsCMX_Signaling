Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:APOint:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:APOint:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Apoint.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: