State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:TOPology:FGS:UE:PDU:STATe

.. code-block:: python

	FETCh:SIGNaling:TOPology:FGS:UE:PDU:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Topology.Fgs.Ue.Pdu.State.StateCls
	:members:
	:undoc-members:
	:noindex: