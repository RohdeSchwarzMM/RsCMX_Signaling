Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:NRADio:CELL:BWP<bwp_id>:SRS:CNCodebook:RESource

.. code-block:: python

	DELete:SIGNaling:NRADio:CELL:BWP<bwp_id>:SRS:CNCodebook:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Bwp.Srs.CnCodebook.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: