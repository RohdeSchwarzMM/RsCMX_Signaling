Cnetwork
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:TOPology:CNETwork

.. code-block:: python

	DELete:SIGNaling:TOPology:CNETwork



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Topology.Cnetwork.CnetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.topology.cnetwork.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Topology_Cnetwork_State.rst