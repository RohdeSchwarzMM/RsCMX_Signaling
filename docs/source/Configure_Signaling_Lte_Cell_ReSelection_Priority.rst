Priority
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RESelection:PRIority

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RESelection:PRIority



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.ReSelection.Priority.PriorityCls
	:members:
	:undoc-members:
	:noindex: