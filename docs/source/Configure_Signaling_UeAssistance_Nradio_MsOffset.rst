MsOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio:MSOFfset

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio:MSOFfset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.MsOffset.MsOffsetCls
	:members:
	:undoc-members:
	:noindex: