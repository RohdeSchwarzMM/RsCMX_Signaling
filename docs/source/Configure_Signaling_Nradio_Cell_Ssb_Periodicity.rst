Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: