Mrdc
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Fgs.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.signaling.fgs.ueCapability.mrdc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_Signaling_Fgs_UeCapability_Mrdc_Bands.rst