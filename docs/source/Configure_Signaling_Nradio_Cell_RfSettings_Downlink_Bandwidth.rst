Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:DL:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: