IpPreambles
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:IPPReambles

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:IPPReambles



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.IpPreambles.IpPreamblesCls
	:members:
	:undoc-members:
	:noindex: