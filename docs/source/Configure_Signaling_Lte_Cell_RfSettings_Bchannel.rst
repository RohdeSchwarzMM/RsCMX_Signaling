Bchannel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:BCHannel

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:BCHannel



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Bchannel.BchannelCls
	:members:
	:undoc-members:
	:noindex: