Instance
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:CELL:INSTance

.. code-block:: python

	SENSe:SIGNaling:CELL:INSTance



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Cell.Instance.InstanceCls
	:members:
	:undoc-members:
	:noindex: