Iloss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:FADing:ILOSs

.. code-block:: python

	[CONFigure]:SIGNaling:FADing:ILOSs



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fading.Iloss.IlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.fading.iloss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Fading_Iloss_Mode.rst