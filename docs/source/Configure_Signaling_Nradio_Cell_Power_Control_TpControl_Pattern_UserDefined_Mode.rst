Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:TPControl:PATTern:UDEFined:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:CONTrol:TPControl:PATTern:UDEFined:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Control.TpControl.Pattern.UserDefined.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: