Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:TPControl:PATTern:UDEFined:PATTern

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:CONTrol:TPControl:PATTern:UDEFined:PATTern



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Control.TpControl.Pattern.UserDefined.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex: