Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OCNG:PDSCh:MODulation

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OCNG:PDSCh:MODulation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Ocng.Pdsch.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: