Frange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:FRANge

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:FRANge



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Frange.FrangeCls
	:members:
	:undoc-members:
	:noindex: