CnCodebook
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Srs.CnCodebook.CnCodebookCls
	:members:
	:undoc-members:
	:noindex: