Ktwo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MSG<id>:TDOMain:KTWO

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MSG<id>:TDOMain:KTWO



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Msg.Tdomain.Ktwo.KtwoCls
	:members:
	:undoc-members:
	:noindex: