Ocng
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.OcngCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.power.downlink.ocng.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Enable.rst
	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Pdcch.rst
	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Pdsch.rst