Fhopping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource:FHOPping

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:RESource:FHOPping



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.CnCodebook.Resource.Fhopping.FhoppingCls
	:members:
	:undoc-members:
	:noindex: