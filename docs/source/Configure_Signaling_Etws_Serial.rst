Serial
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SERial

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SERial



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Serial.SerialCls
	:members:
	:undoc-members:
	:noindex: