IbChange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAM:FOLLowing:IBCHange

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAM:FOLLowing:IBCHange



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Following.IbChange.IbChangeCls
	:members:
	:undoc-members:
	:noindex: