Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:POWer:CONTrol:TPControl:PATTern

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:POWer:CONTrol:TPControl:PATTern



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Power.Control.TpControl.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.power.control.tpControl.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Power_Control_TpControl_Pattern_UserDefined.rst