Pcfich
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:PCFich

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:PCFich



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Offset.Pcfich.PcfichCls
	:members:
	:undoc-members:
	:noindex: