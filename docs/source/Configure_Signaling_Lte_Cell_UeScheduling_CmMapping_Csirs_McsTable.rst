McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:CSIRs:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:CSIRs:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.CmMapping.Csirs.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: