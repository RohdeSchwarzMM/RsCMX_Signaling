Pdu
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Topology.Fgs.Ue.Pdu.PduCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.signaling.topology.fgs.ue.pdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_Signaling_Topology_Fgs_Ue_Pdu_QosFlow.rst