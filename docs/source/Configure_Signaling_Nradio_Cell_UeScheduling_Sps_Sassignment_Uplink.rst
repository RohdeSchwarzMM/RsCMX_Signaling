Uplink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Sassignment.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.ueScheduling.sps.sassignment.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Sassignment_Uplink_All.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Sassignment_Uplink_Mcs.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Sassignment_Uplink_Rb.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Sassignment_Uplink_Sindex.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Sassignment_Uplink_Tdomain.rst