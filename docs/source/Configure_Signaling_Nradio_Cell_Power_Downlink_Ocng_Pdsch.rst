Pdsch
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.Pdsch.PdschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.power.downlink.ocng.pdsch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Pdsch_DscSsb.rst
	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Pdsch_Modulation.rst
	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Pdsch_Poffset.rst
	Configure_Signaling_Nradio_Cell_Power_Downlink_Ocng_Pdsch_Rb.rst