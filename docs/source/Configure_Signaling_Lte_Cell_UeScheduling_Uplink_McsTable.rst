McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: