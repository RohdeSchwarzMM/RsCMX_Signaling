Absolute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:ABSolute

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:ABSolute



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Cword.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: