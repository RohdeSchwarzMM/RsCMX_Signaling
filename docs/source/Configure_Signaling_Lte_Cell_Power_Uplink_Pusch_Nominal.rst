Nominal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:PUSCh:NOMinal

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:PUSCh:NOMinal



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.Pusch.Nominal.NominalCls
	:members:
	:undoc-members:
	:noindex: