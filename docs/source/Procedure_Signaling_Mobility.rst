Mobility
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:MOBility:REDirection

.. code-block:: python

	PROCedure:SIGNaling:MOBility:REDirection



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Mobility.MobilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.procedure.signaling.mobility.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Procedure_Signaling_Mobility_Handover.rst