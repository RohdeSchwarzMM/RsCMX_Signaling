Bler
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:SIGNaling:MEASurement:BLER
	single: STOP:SIGNaling:MEASurement:BLER
	single: INITiate:SIGNaling:MEASurement:BLER

.. code-block:: python

	ABORt:SIGNaling:MEASurement:BLER
	STOP:SIGNaling:MEASurement:BLER
	INITiate:SIGNaling:MEASurement:BLER



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.BlerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.measurement.bler.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Measurement_Bler_Absolute.rst
	Signaling_Measurement_Bler_Confidence.rst
	Signaling_Measurement_Bler_Cword.rst
	Signaling_Measurement_Bler_Overall.rst
	Signaling_Measurement_Bler_Relative.rst
	Signaling_Measurement_Bler_State.rst
	Signaling_Measurement_Bler_Throughput.rst
	Signaling_Measurement_Bler_Uplink.rst