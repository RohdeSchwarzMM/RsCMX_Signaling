Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:SDRX:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:SDRX:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Sdrx.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: