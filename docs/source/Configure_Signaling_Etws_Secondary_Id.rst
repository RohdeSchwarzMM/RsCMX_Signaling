Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:ID

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:ID



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.Id.IdCls
	:members:
	:undoc-members:
	:noindex: