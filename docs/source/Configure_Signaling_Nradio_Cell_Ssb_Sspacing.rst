Sspacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:SSPacing

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:SSPacing



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Sspacing.SspacingCls
	:members:
	:undoc-members:
	:noindex: