Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:NRMMw:MEASurement<Instance>:NETWork:CELL

.. code-block:: python

	[CONFigure]:NRMMw:MEASurement<Instance>:NETWork:CELL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.NrMmw.Measurement.Network.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: