Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CSI:TRS:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:CSI:TRS:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Csi.Trs.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: