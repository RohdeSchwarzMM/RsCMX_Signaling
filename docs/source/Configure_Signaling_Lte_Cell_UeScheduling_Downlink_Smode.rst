Smode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:DL:SMODe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:DL:SMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Downlink.Smode.SmodeCls
	:members:
	:undoc-members:
	:noindex: