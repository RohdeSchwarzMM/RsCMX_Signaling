Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.UeScheduling.Sps.Sassignment.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.ueScheduling.sps.sassignment.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Sassignment_Downlink_All.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Sassignment_Downlink_Mcs.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Sassignment_Downlink_Rb.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Sassignment_Downlink_Sindex.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Sassignment_Downlink_Tdomain.rst