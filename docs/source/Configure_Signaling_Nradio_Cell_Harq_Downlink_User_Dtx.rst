Dtx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:DTX

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:DTX



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Downlink.User.Dtx.DtxCls
	:members:
	:undoc-members:
	:noindex: