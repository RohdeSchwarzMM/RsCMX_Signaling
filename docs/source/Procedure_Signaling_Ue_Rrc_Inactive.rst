Inactive
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:UE:RRC:INACtive

.. code-block:: python

	PROCedure:SIGNaling:UE:RRC:INACtive



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Ue.Rrc.Inactive.InactiveCls
	:members:
	:undoc-members:
	:noindex: