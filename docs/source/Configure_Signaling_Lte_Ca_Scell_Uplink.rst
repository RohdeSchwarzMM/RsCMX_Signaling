Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CA:SCELl:UL

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CA:SCELl:UL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ca.Scell.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: