Arfcn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:APOint:ARFCn

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:APOint:ARFCn



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Apoint.Arfcn.ArfcnCls
	:members:
	:undoc-members:
	:noindex: