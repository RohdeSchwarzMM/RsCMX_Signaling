Rrc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:SIGNaling:UE:RRC

.. code-block:: python

	PROCedure:SIGNaling:UE:RRC



.. autoclass:: RsCMX_Signaling.Implementations.Procedure.Signaling.Ue.Rrc.RrcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.procedure.signaling.ue.rrc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Procedure_Signaling_Ue_Rrc_Inactive.rst
	Procedure_Signaling_Ue_Rrc_Resume.rst