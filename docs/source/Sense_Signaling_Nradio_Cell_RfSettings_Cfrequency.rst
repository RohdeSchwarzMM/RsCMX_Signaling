Cfrequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:RFSettings:CFRequency

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:RFSettings:CFRequency



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.RfSettings.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex: