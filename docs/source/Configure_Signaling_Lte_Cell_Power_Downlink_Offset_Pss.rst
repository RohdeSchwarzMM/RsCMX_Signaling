Pss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:PSS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:PSS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Offset.Pss.PssCls
	:members:
	:undoc-members:
	:noindex: