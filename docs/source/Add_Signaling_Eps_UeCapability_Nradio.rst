Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:EPS:UECapability:NRADio:BANDs

.. code-block:: python

	ADD:SIGNaling:EPS:UECapability:NRADio:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Eps.UeCapability.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex: