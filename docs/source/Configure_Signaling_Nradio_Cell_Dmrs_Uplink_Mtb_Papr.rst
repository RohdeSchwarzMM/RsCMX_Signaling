Papr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:MTB:PAPR

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:MTB:PAPR



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Uplink.Mtb.Papr.PaprCls
	:members:
	:undoc-members:
	:noindex: