Extended
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:TOPology:EPS:TIMer:T<no>:EXTended

.. code-block:: python

	[CONFigure]:SIGNaling:TOPology:EPS:TIMer:T<no>:EXTended



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Topology.Eps.Timer.T.Extended.ExtendedCls
	:members:
	:undoc-members:
	:noindex: