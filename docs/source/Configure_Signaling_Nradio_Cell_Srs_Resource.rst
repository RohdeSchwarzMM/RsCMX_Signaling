Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:RESource

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:RESource



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: