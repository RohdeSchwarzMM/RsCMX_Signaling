Resource
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.cqiReporting.resource.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_CqiReporting_Resource_FoSymbol.rst
	Configure_Signaling_Nradio_Cell_CqiReporting_Resource_Offset.rst
	Configure_Signaling_Nradio_Cell_CqiReporting_Resource_Ports.rst
	Configure_Signaling_Nradio_Cell_CqiReporting_Resource_PoVsss.rst