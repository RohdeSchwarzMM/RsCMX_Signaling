Rblocks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:RBLocks

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:DL:RBLocks



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Downlink.Rblocks.RblocksCls
	:members:
	:undoc-members:
	:noindex: