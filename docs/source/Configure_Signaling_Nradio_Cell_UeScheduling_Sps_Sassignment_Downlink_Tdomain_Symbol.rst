Symbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:SASSignment:DL:TDOMain:SYMBol

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:SASSignment:DL:TDOMain:SYMBol



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Sassignment.Downlink.Tdomain.Symbol.SymbolCls
	:members:
	:undoc-members:
	:noindex: