Ssb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAM:FRECovery:UDEFined:SSB

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAM:FRECovery:UDEFined:SSB



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Frecovery.UserDefined.Ssb.SsbCls
	:members:
	:undoc-members:
	:noindex: