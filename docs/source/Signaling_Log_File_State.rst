State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:LOG:FILE:STATe

.. code-block:: python

	FETCh:SIGNaling:LOG:FILE:STATe



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Log.File.State.StateCls
	:members:
	:undoc-members:
	:noindex: