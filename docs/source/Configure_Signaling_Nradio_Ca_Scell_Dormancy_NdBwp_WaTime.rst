WaTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:NDBWp:WATime

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:NDBWp:WATime



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Dormancy.NdBwp.WaTime.WaTimeCls
	:members:
	:undoc-members:
	:noindex: