MbwPref
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:UEASsistance:NRADio:MBWPref

.. code-block:: python

	[CONFigure]:SIGNaling:UEASsistance:NRADio:MBWPref



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.UeAssistance.Nradio.MbwPref.MbwPrefCls
	:members:
	:undoc-members:
	:noindex: