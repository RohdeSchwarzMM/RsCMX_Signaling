Pmi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:REPort:PMI

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:REPort:PMI



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Report.Pmi.PmiCls
	:members:
	:undoc-members:
	:noindex: