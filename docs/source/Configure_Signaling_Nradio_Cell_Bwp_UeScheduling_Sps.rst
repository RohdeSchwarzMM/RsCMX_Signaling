Sps
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.UeScheduling.Sps.SpsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.ueScheduling.sps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Downlink.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Sassignment.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_Sps_Uplink.rst