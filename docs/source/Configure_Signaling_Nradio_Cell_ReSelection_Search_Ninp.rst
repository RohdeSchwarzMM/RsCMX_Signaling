Ninp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RESelection:SEARch:NINP

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RESelection:SEARch:NINP



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.ReSelection.Search.Ninp.NinpCls
	:members:
	:undoc-members:
	:noindex: