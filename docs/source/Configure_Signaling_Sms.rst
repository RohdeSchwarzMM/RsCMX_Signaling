Sms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:SMS:SCENtre

.. code-block:: python

	[CONFigure]:SIGNaling:SMS:SCENtre



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Sms.SmsCls
	:members:
	:undoc-members:
	:noindex: