Percent
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BLER:DL:PERCent

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BLER:DL:PERCent



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bler.Downlink.Percent.PercentCls
	:members:
	:undoc-members:
	:noindex: