Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUSCh:TSCHema:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUSCh:TSCHema:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pusch.Tschema.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: