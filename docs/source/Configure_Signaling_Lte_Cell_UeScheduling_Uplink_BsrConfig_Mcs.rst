Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:MCS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:BSRConfig:MCS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.BsrConfig.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: