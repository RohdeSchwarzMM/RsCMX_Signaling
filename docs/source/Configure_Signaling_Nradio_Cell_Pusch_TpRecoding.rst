TpRecoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PUSCh:TPRecoding

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PUSCh:TPRecoding



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pusch.TpRecoding.TpRecodingCls
	:members:
	:undoc-members:
	:noindex: