PrtPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:PRTPower

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:UL:PRTPower



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Uplink.PrtPower.PrtPowerCls
	:members:
	:undoc-members:
	:noindex: