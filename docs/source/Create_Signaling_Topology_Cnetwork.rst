Cnetwork
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:TOPology:CNETwork

.. code-block:: python

	CREate:SIGNaling:TOPology:CNETwork



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Topology.Cnetwork.CnetworkCls
	:members:
	:undoc-members:
	:noindex: