Sframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:COMMon:SFRame

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:COMMon:SFRame



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Common.Sframe.SframeCls
	:members:
	:undoc-members:
	:noindex: