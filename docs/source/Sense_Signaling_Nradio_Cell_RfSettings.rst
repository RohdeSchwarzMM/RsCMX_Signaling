RfSettings
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.signaling.nradio.cell.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Signaling_Nradio_Cell_RfSettings_Cfrequency.rst