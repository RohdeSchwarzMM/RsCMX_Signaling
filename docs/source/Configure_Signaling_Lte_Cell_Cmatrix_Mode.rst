Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CMATrix:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CMATrix:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cmatrix.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: