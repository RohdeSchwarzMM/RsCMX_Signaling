Uplinks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:NRMMw:MEASurement<Instance>:NETWork:CELL:UPLinks

.. code-block:: python

	CATalog:NRMMw:MEASurement<Instance>:NETWork:CELL:UPLinks



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.NrMmw.Measurement.Network.Cell.Uplinks.UplinksCls
	:members:
	:undoc-members:
	:noindex: