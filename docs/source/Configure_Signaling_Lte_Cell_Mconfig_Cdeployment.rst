Cdeployment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:MCONfig:CDEPloyment

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:MCONfig:CDEPloyment



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Mconfig.Cdeployment.CdeploymentCls
	:members:
	:undoc-members:
	:noindex: