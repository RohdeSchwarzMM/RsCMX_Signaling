Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UL:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UL:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Uplink.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: