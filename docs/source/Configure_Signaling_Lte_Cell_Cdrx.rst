Cdrx
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.CdrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.cdrx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_Cdrx_AaScheduler.rst
	Configure_Signaling_Lte_Cell_Cdrx_Enable.rst
	Configure_Signaling_Lte_Cell_Cdrx_Itimer.rst
	Configure_Signaling_Lte_Cell_Cdrx_Ldrx.rst
	Configure_Signaling_Lte_Cell_Cdrx_OdTimer.rst
	Configure_Signaling_Lte_Cell_Cdrx_Rtimer.rst
	Configure_Signaling_Lte_Cell_Cdrx_Sdrx.rst