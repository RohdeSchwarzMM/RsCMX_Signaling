Rtype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:RTYPe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:RTYPe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Rtype.RtypeCls
	:members:
	:undoc-members:
	:noindex: