Fhopping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:FHOPping

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:ASWitching:RESource:FHOPping



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Aswitching.Resource.Fhopping.FhoppingCls
	:members:
	:undoc-members:
	:noindex: