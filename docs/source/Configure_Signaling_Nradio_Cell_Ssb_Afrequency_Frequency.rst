Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:AFRequency:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:AFRequency:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Afrequency.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: