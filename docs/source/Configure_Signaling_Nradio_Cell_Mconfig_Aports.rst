Aports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MCONfig:APORts

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MCONfig:APORts



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Mconfig.Aports.AportsCls
	:members:
	:undoc-members:
	:noindex: