Sassignment
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Sassignment.SassignmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.ueScheduling.userDefined.sassignment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Sassignment_Downlink.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_UserDefined_Sassignment_Uplink.rst