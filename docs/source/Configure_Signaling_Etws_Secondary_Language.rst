Language
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:LANGuage

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:LANGuage



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.Language.LanguageCls
	:members:
	:undoc-members:
	:noindex: