Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:CMMapping:MCS

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:CMMapping:MCS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.CmMapping.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: