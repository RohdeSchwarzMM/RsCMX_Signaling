CnCodebook
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Srs.CnCodebook.CnCodebookCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.srs.cnCodebook.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Power.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Resource.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Scheduler.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_TdBehavior.rst
	Configure_Signaling_Nradio_Cell_Bwp_Srs_CnCodebook_Usage.rst