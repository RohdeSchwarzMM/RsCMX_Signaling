OdTimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:ODTimer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:ODTimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.OdTimer.OdTimerCls
	:members:
	:undoc-members:
	:noindex: