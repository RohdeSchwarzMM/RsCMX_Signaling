Apoint
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Apoint.ApointCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.rfSettings.downlink.apoint.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_RfSettings_Downlink_Apoint_Arfcn.rst
	Configure_Signaling_Nradio_Cell_RfSettings_Downlink_Apoint_Frequency.rst
	Configure_Signaling_Nradio_Cell_RfSettings_Downlink_Apoint_Location.rst