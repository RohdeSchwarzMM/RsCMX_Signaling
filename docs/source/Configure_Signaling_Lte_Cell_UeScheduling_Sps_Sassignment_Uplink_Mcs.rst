Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:MCS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:MCS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Sps.Sassignment.Uplink.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: