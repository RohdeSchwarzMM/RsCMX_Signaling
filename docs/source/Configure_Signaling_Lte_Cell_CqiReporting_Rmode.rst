Rmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CQIReporting:RMODe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CQIReporting:RMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.CqiReporting.Rmode.RmodeCls
	:members:
	:undoc-members:
	:noindex: