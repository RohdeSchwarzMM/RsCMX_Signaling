Cells
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:NRMMw:MEASurement<Instance>:NETWork:CELLs

.. code-block:: python

	CATalog:NRMMw:MEASurement<Instance>:NETWork:CELLs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.NrMmw.Measurement.Network.Cells.CellsCls
	:members:
	:undoc-members:
	:noindex: