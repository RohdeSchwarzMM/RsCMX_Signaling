DciFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:DCIFormat

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:DCIFormat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.DciFormat.DciFormatCls
	:members:
	:undoc-members:
	:noindex: