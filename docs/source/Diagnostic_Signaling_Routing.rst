Routing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:SIGNaling:ROUTing

.. code-block:: python

	DIAGnostic:SIGNaling:ROUTing



.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Routing.RoutingCls
	:members:
	:undoc-members:
	:noindex: