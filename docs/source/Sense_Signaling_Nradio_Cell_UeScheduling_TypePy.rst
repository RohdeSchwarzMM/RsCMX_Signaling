TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:UESCheduling:TYPE

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:UESCheduling:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.UeScheduling.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: