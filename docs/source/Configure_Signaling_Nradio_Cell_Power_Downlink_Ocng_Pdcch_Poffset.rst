Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDCCh:POFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDCCh:POFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.Pdcch.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: