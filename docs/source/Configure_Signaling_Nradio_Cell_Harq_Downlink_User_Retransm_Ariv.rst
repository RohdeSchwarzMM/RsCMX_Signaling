Ariv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm:ARIV

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:DL:USER:RETRansm:ARIV



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Downlink.User.Retransm.Ariv.ArivCls
	:members:
	:undoc-members:
	:noindex: