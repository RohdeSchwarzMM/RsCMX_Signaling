TxRx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:TXRX

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:TXRX



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Uplink.TxRx.TxRxCls
	:members:
	:undoc-members:
	:noindex: