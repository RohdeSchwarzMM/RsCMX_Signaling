Rtimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:RTIMer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:RTIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Rtimer.RtimerCls
	:members:
	:undoc-members:
	:noindex: