Cdeployment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MCONfig:CDEPloyment

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MCONfig:CDEPloyment



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Mconfig.Cdeployment.CdeploymentCls
	:members:
	:undoc-members:
	:noindex: