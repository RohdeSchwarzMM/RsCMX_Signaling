Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:FGS:UECapability:EUTRa:BANDs

.. code-block:: python

	DELete:SIGNaling:FGS:UECapability:EUTRa:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Fgs.UeCapability.Eutra.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: