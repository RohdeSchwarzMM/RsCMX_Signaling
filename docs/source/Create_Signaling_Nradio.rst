Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SIGNaling:NRADio:CGRoup

.. code-block:: python

	CREate:SIGNaling:NRADio:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Create.Signaling.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.signaling.nradio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_Signaling_Nradio_Cell.rst
	Create_Signaling_Nradio_Vcell.rst