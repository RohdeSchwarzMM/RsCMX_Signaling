Subset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:PUSCh:TSCHema:CODebook:SUBSet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bpwid>:PUSCh:TSCHema:CODebook:SUBSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Pusch.Tschema.Codebook.Subset.SubsetCls
	:members:
	:undoc-members:
	:noindex: