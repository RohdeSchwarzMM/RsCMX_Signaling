Ssid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:UL:SSID

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SPS:UL:SSID



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Uplink.Ssid.SsidCls
	:members:
	:undoc-members:
	:noindex: