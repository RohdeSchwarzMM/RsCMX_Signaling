Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.Pattern.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: