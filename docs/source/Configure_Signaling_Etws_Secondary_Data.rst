Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:ETWS:SECondary:DATA

.. code-block:: python

	[CONFigure]:SIGNaling:ETWS:SECondary:DATA



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Etws.Secondary.Data.DataCls
	:members:
	:undoc-members:
	:noindex: