Cindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:DEDicated:CINDex

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:DEDicated:CINDex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Dedicated.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: