PoVsss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:POVSss

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:RESource:POVSss



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Resource.PoVsss.PoVsssCls
	:members:
	:undoc-members:
	:noindex: