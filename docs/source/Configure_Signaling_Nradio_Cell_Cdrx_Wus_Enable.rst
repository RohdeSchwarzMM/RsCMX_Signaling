Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:WUS:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:WUS:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Wus.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: