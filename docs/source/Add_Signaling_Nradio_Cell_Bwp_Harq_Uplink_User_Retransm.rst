Retransm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:UL:USER:RETRansm

.. code-block:: python

	ADD:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:UL:USER:RETRansm



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Nradio.Cell.Bwp.Harq.Uplink.User.Retransm.RetransmCls
	:members:
	:undoc-members:
	:noindex: