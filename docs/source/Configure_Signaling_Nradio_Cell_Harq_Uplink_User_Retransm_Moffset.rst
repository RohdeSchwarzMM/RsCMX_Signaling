Moffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm:MOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm:MOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Uplink.User.Retransm.Moffset.MoffsetCls
	:members:
	:undoc-members:
	:noindex: