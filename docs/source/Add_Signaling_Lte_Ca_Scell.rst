Scell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SIGNaling:LTE:CA:SCELl

.. code-block:: python

	ADD:SIGNaling:LTE:CA:SCELl



.. autoclass:: RsCMX_Signaling.Implementations.Add.Signaling.Lte.Ca.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex: