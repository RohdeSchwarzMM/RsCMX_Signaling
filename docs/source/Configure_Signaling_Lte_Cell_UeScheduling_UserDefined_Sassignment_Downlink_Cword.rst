Cword<Cword>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.signaling.lte.cell.ueScheduling.userDefined.sassignment.downlink.cword.repcap_cword_get()
	driver.configure.signaling.lte.cell.ueScheduling.userDefined.sassignment.downlink.cword.repcap_cword_set(repcap.Cword.Nr1)





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Downlink.Cword.CwordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.userDefined.sassignment.downlink.cword.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Cword_Crtype.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Cword_Mcs.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Cword_TbsBits.rst
	Configure_Signaling_Lte_Cell_UeScheduling_UserDefined_Sassignment_Downlink_Cword_TbsIndex.rst