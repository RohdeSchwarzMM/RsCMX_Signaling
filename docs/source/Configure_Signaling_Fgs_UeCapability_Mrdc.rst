Mrdc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:FGS:UECapability:MRDC:ENRonly

.. code-block:: python

	[CONFigure]:SIGNaling:FGS:UECapability:MRDC:ENRonly



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fgs.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex: