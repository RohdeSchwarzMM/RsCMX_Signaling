HbWidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:DEDicated:HBWidth

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:DEDicated:HBWidth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Dedicated.HbWidth.HbWidthCls
	:members:
	:undoc-members:
	:noindex: