Rtimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:UL:RTIMer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:UL:RTIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Uplink.Rtimer.RtimerCls
	:members:
	:undoc-members:
	:noindex: