Rmatching
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:RMATching

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:RMATching



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ibwp.Coreset.Rmatching.RmatchingCls
	:members:
	:undoc-members:
	:noindex: