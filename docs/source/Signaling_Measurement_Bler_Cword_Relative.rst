Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:RELative

.. code-block:: python

	FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:RELative



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.Bler.Cword.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: