HigHq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RESelection:THResholds:HIGHq

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RESelection:THResholds:HIGHq



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.ReSelection.Thresholds.HigHq.HigHqCls
	:members:
	:undoc-members:
	:noindex: