Smode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:SMODe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:SMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.Smode.SmodeCls
	:members:
	:undoc-members:
	:noindex: