Combined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:COMBined

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CQIReporting:COMBined



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.CqiReporting.Combined.CombinedCls
	:members:
	:undoc-members:
	:noindex: