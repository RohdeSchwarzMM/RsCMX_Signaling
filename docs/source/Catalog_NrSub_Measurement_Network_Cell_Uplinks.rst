Uplinks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:NRSub:MEASurement<Instance>:NETWork:CELL:UPLinks

.. code-block:: python

	CATalog:NRSub:MEASurement<Instance>:NETWork:CELL:UPLinks



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.NrSub.Measurement.Network.Cell.Uplinks.UplinksCls
	:members:
	:undoc-members:
	:noindex: