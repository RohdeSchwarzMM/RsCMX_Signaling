Retransm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:USER:RETRansm

.. code-block:: python

	DELete:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:USER:RETRansm



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Bwp.Harq.Downlink.User.Retransm.RetransmCls
	:members:
	:undoc-members:
	:noindex: