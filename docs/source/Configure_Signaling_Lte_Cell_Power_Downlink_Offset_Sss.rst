Sss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:SSS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:DL:OFFSet:SSS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Downlink.Offset.Sss.SssCls
	:members:
	:undoc-members:
	:noindex: