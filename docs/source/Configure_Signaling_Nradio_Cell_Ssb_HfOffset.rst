HfOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:HFOFfset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:HFOFfset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.HfOffset.HfOffsetCls
	:members:
	:undoc-members:
	:noindex: