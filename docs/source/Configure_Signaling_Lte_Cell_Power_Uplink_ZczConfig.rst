ZczConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:ZCZConfig

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:ZCZConfig



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.ZczConfig.ZczConfigCls
	:members:
	:undoc-members:
	:noindex: