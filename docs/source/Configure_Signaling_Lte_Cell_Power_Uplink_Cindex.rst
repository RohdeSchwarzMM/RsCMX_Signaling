Cindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:CINDex

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:CINDex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: