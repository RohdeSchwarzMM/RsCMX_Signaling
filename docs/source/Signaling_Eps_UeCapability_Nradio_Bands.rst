Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:EPS:UECapability:NRADio:BANDs

.. code-block:: python

	DELete:SIGNaling:EPS:UECapability:NRADio:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Eps.UeCapability.Nradio.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: