Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:EPS:UECapability:EUTRa:BANDs

.. code-block:: python

	DELete:SIGNaling:EPS:UECapability:EUTRa:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Eps.UeCapability.Eutra.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: