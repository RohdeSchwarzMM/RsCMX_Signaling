User
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Bwp.Harq.Uplink.User.UserCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.nradio.cell.bwp.harq.uplink.user.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Nradio_Cell_Bwp_Harq_Uplink_User_Retransm.rst