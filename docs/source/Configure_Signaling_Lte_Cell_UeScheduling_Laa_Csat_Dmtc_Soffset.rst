Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:CSAT:DMTC:SOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:CSAT:DMTC:SOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.Csat.Dmtc.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: