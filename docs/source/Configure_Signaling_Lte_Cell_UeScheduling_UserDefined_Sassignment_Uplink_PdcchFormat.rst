PdcchFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:UL:PDCChformat

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UDEFined:SASSignment:UL:PDCChformat



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.UserDefined.Sassignment.Uplink.PdcchFormat.PdcchFormatCls
	:members:
	:undoc-members:
	:noindex: