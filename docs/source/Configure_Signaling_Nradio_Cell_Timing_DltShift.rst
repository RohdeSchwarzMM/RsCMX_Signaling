DltShift
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TIMing:DLTShift

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TIMing:DLTShift



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Timing.DltShift.DltShiftCls
	:members:
	:undoc-members:
	:noindex: