Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:SIGNaling:REGistration:ADD

.. code-block:: python

	DIAGnostic:SIGNaling:REGistration:ADD



.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Registration.Add.AddCls
	:members:
	:undoc-members:
	:noindex: