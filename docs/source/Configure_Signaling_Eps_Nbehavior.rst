Nbehavior
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:EPS:NBEHavior:DITimer
	single: [CONFigure]:SIGNaling:EPS:NBEHavior:KRRC

.. code-block:: python

	[CONFigure]:SIGNaling:EPS:NBEHavior:DITimer
	[CONFigure]:SIGNaling:EPS:NBEHavior:KRRC



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Eps.Nbehavior.NbehaviorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.eps.nbehavior.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Eps_Nbehavior_RrcReject.rst