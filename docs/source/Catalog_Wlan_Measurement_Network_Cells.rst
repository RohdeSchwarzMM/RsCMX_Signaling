Cells
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:WLAN:MEASurement<Instance>:NETWork:CELLs

.. code-block:: python

	CATalog:WLAN:MEASurement<Instance>:NETWork:CELLs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Wlan.Measurement.Network.Cells.CellsCls
	:members:
	:undoc-members:
	:noindex: