Papr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:MTA:PAPR

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:UL:MTA:PAPR



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Uplink.Mta.Papr.PaprCls
	:members:
	:undoc-members:
	:noindex: