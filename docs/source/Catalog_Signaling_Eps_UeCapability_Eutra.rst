Eutra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:EPS:UECapability:EUTRa:BANDs

.. code-block:: python

	CATalog:SIGNaling:EPS:UECapability:EUTRa:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Eps.UeCapability.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: