Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Sps.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.ueScheduling.sps.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_Alevel.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_All.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_Mapping.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_McsTable.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_Nohp.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_Padding.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_Periodicity.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_RaType.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_RbgSize.rst
	Configure_Signaling_Nradio_Cell_UeScheduling_Sps_Downlink_Ssid.rst