BsrConfig
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.BsrConfig.BsrConfigCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.uplink.bsrConfig.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_BsrConfig_Mcs.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_BsrConfig_McsModes.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_BsrConfig_Morder.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_BsrConfig_Rb.rst