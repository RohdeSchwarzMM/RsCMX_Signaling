Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:RFSettings:UL:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.RfSettings.Uplink.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: