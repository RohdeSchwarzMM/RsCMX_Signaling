Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:UL

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:UL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: