Activation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:ACTivation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:ACTivation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Activation.ActivationCls
	:members:
	:undoc-members:
	:noindex: