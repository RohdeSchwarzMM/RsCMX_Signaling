Tcomb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:TCOMb

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:TCOMb



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.Tcomb.TcombCls
	:members:
	:undoc-members:
	:noindex: