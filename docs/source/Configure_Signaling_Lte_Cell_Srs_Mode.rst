Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:SRS:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:SRS:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Srs.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: