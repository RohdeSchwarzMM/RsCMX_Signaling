Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:CFRequency:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:CFRequency:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Cfrequency.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: