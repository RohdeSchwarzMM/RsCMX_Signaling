SrcIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:IGConfig:SRCindex

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:UL:IGConfig:SRCindex



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.IgConfig.SrcIndex.SrcIndexCls
	:members:
	:undoc-members:
	:noindex: