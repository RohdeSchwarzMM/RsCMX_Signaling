Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAM:FRECovery:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAM:FRECovery:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Frecovery.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: