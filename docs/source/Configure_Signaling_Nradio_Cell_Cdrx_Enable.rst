Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:ENABle

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:ENABle



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: