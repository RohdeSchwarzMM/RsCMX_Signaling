PopFrame
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:PCYCle:POPFrame

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:PCYCle:POPFrame



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Pcycle.PopFrame.PopFrameCls
	:members:
	:undoc-members:
	:noindex: