Bearer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:UE:BEARer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:UE:BEARer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ue.Bearer.BearerCls
	:members:
	:undoc-members:
	:noindex: