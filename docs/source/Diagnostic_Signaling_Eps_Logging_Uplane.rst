Uplane
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:SIGNaling:EPS:LOGGing:UPLane:UL
	single: DIAGnostic:SIGNaling:EPS:LOGGing:UPLane:DL

.. code-block:: python

	DIAGnostic:SIGNaling:EPS:LOGGing:UPLane:UL
	DIAGnostic:SIGNaling:EPS:LOGGing:UPLane:DL



.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Eps.Logging.Uplane.UplaneCls
	:members:
	:undoc-members:
	:noindex: