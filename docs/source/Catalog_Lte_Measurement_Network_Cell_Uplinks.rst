Uplinks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:LTE:MEASurement<Instance>:NETWork:CELL:UPLinks

.. code-block:: python

	CATalog:LTE:MEASurement<Instance>:NETWork:CELL:UPLinks



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Lte.Measurement.Network.Cell.Uplinks.UplinksCls
	:members:
	:undoc-members:
	:noindex: