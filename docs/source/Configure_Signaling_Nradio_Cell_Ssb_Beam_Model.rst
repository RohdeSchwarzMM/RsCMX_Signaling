Model
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:BEAM:MODel

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:BEAM:MODel



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.Beam.Model.ModelCls
	:members:
	:undoc-members:
	:noindex: