Duration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:DURation

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:DURation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ibwp.Coreset.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex: