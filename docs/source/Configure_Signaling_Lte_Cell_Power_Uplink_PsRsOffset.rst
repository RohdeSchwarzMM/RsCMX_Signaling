PsRsOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:PSRSoffset

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:PSRSoffset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.PsRsOffset.PsRsOffsetCls
	:members:
	:undoc-members:
	:noindex: