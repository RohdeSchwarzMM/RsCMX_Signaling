Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:SASSignment:DL:TDOMain:SOFFset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:UDEFined:SASSignment:DL:TDOMain:SOFFset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.UserDefined.Sassignment.Downlink.Tdomain.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: