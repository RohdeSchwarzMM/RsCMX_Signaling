Bearer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:LTE:UE:BEARer

.. code-block:: python

	CATalog:SIGNaling:LTE:UE:BEARer



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Lte.Ue.Bearer.BearerCls
	:members:
	:undoc-members:
	:noindex: