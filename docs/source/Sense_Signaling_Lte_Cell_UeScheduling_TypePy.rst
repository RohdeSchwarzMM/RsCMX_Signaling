TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:LTE:CELL:UESCheduling:TYPE

.. code-block:: python

	SENSe:SIGNaling:LTE:CELL:UESCheduling:TYPE



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Lte.Cell.UeScheduling.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: