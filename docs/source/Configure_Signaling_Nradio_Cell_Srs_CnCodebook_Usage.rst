Usage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:USAGe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SRS:CNCodebook:USAGe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Srs.CnCodebook.Usage.UsageCls
	:members:
	:undoc-members:
	:noindex: