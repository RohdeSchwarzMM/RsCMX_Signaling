SsIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BWP<bpwid>:PUCCh:SSINdex

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BWP<bpwid>:PUCCh:SSINdex



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.Pucch.SsIndex.SsIndexCls
	:members:
	:undoc-members:
	:noindex: