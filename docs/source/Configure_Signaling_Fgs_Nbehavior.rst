Nbehavior
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:FGS:NBEHavior:DITimer
	single: [CONFigure]:SIGNaling:FGS:NBEHavior:KRRC

.. code-block:: python

	[CONFigure]:SIGNaling:FGS:NBEHavior:DITimer
	[CONFigure]:SIGNaling:FGS:NBEHavior:KRRC



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Fgs.Nbehavior.NbehaviorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.fgs.nbehavior.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Fgs_Nbehavior_RrcReject.rst