PaOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:SSB:PAOFfset

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:SSB:PAOFfset



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ssb.PaOffset.PaOffsetCls
	:members:
	:undoc-members:
	:noindex: