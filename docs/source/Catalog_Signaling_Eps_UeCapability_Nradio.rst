Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:EPS:UECapability:NRADio:BANDs

.. code-block:: python

	CATalog:SIGNaling:EPS:UECapability:NRADio:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Eps.UeCapability.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex: