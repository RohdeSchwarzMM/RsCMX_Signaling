Rtype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CQIReporting:RTYPe

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CQIReporting:RTYPe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.CqiReporting.Rtype.RtypeCls
	:members:
	:undoc-members:
	:noindex: