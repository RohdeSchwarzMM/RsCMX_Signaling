Cells
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:NRSub:MEASurement<Instance>:NETWork:CELLs

.. code-block:: python

	CATalog:NRSub:MEASurement<Instance>:NETWork:CELLs



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.NrSub.Measurement.Network.Cells.CellsCls
	:members:
	:undoc-members:
	:noindex: