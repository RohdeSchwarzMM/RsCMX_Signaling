Imsi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SIGNaling:UE:IMSI

.. code-block:: python

	FETCh:SIGNaling:UE:IMSI



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Ue.Imsi.ImsiCls
	:members:
	:undoc-members:
	:noindex: