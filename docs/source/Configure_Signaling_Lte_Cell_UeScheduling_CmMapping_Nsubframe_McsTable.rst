McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:NSUBframe:MCSTable

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:NSUBframe:MCSTable



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.CmMapping.Nsubframe.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex: