UserDefined
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.UeScheduling.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.bwp.ueScheduling.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_CsScheduling.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Downlink.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Sassignment.rst
	Configure_Signaling_Nradio_Cell_Bwp_UeScheduling_UserDefined_Uplink.rst