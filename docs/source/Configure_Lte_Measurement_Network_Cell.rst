Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:LTE:MEASurement<Instance>:NETWork:CELL

.. code-block:: python

	[CONFigure]:LTE:MEASurement<Instance>:NETWork:CELL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Lte.Measurement.Network.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: