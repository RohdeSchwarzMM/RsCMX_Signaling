Quantity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:REPort:QUANtity

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:BWP<bwp_id>:CQIReporting:REPort:QUANtity



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Bwp.CqiReporting.Report.Quantity.QuantityCls
	:members:
	:undoc-members:
	:noindex: