Activation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CA:SCELl:ACTivation

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CA:SCELl:ACTivation



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Ca.Scell.Activation.ActivationCls
	:members:
	:undoc-members:
	:noindex: