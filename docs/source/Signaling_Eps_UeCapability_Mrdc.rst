Mrdc
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Eps.UeCapability.Mrdc.MrdcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.eps.ueCapability.mrdc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Eps_UeCapability_Mrdc_Bands.rst