Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:NRSub:MEASurement<Instance>:NETWork:CELL

.. code-block:: python

	[CONFigure]:NRSub:MEASurement<Instance>:NETWork:CELL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.NrSub.Measurement.Network.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: