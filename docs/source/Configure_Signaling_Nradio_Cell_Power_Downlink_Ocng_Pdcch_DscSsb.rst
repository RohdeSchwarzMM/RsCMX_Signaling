DscSsb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDCCh:DSCSsb

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:POWer:DL:OCNG:PDCCh:DSCSsb



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Power.Downlink.Ocng.Pdcch.DscSsb.DscSsbCls
	:members:
	:undoc-members:
	:noindex: