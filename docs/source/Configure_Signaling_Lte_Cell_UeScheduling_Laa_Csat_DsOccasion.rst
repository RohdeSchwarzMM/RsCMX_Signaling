DsOccasion
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:CSAT:DSOCcasion

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:LAA:CSAT:DSOCcasion



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Laa.Csat.DsOccasion.DsOccasionCls
	:members:
	:undoc-members:
	:noindex: