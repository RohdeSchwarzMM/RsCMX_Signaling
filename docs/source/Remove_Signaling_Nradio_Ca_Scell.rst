Scell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: REMove:SIGNaling:NRADio:CA:SCELl

.. code-block:: python

	REMove:SIGNaling:NRADio:CA:SCELl



.. autoclass:: RsCMX_Signaling.Implementations.Remove.Signaling.Nradio.Ca.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex: