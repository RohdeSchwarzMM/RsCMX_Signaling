Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CMATrix:MODE

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CMATrix:MODE



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cmatrix.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: