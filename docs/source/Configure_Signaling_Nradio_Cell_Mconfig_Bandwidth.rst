Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:MCONfig:BWIDth

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:MCONfig:BWIDth



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Mconfig.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: