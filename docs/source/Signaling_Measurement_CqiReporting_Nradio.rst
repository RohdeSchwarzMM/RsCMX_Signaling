Nradio
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Measurement.CqiReporting.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.measurement.cqiReporting.nradio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Measurement_CqiReporting_Nradio_Cword.rst