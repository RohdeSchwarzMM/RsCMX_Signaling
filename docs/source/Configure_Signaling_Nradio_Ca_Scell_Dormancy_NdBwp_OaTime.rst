OaTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:NDBWp:OATime

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CA:SCELl:DORMancy:NDBWp:OATime



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Ca.Scell.Dormancy.NdBwp.OaTime.OaTimeCls
	:members:
	:undoc-members:
	:noindex: