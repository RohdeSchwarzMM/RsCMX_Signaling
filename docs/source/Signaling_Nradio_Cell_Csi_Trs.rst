Trs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:NRADio:CELL:CSI:TRS

.. code-block:: python

	DELete:SIGNaling:NRADio:CELL:CSI:TRS



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Nradio.Cell.Csi.Trs.TrsCls
	:members:
	:undoc-members:
	:noindex: