Nradio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:NRADio:CGRoup

.. code-block:: python

	CATalog:SIGNaling:NRADio:CGRoup



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Nradio.NradioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.signaling.nradio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Signaling_Nradio_Ca.rst
	Catalog_Signaling_Nradio_Cell.rst
	Catalog_Signaling_Nradio_Ncell.rst