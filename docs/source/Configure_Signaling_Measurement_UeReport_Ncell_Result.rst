Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:TYPE
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:CNETwork
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:SIB
	single: [CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:NCList

.. code-block:: python

	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:TYPE
	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:CNETwork
	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:SIB
	[CONFigure]:SIGNaling:MEASurement:UEReport:NCELl:RESult:NCList



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Measurement.UeReport.Ncell.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.measurement.ueReport.ncell.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Measurement_UeReport_Ncell_Result_Enable.rst