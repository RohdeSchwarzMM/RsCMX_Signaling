Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Lte.Cell.Harq.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.signaling.lte.cell.harq.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Signaling_Lte_Cell_Harq_Downlink_ReTx.rst
	Signaling_Lte_Cell_Harq_Downlink_RvSequence.rst