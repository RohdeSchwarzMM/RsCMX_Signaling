Cmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:CMODe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP<bwp_id>:HARQ:DL:CMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Harq.Downlink.Cmode.CmodeCls
	:members:
	:undoc-members:
	:noindex: