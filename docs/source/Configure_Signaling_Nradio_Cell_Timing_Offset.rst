Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TIMing:OFFSet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TIMing:OFFSet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Timing.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: