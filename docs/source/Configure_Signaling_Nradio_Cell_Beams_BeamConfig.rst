BeamConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAMs:BEAMconfig

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAMs:BEAMconfig



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beams.BeamConfig.BeamConfigCls
	:members:
	:undoc-members:
	:noindex: