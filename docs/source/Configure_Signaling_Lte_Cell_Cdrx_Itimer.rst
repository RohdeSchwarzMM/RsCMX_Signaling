Itimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:CDRX:ITIMer

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:CDRX:ITIMer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Cdrx.Itimer.ItimerCls
	:members:
	:undoc-members:
	:noindex: