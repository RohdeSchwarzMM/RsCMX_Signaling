Csirs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAM:FRECovery:UDEFined:CSIRs

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAM:FRECovery:UDEFined:CSIRs



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beam.Frecovery.UserDefined.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex: