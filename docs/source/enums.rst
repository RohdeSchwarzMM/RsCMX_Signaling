Enums
=========

AckOrDtx
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AckOrDtx.CONTinue
	# All values (2x):
	CONTinue | STOP

Action
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Action.CONNect
	# All values (2x):
	CONNect | DISConnect

AggrLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AggrLevel.N0
	# All values (8x):
	N0 | N1 | N2 | N3 | N4 | N5 | N6 | N8

Algorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Algorithm.ERC1
	# All values (4x):
	ERC1 | ERC2 | ERC3 | ERC4

Alpha
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Alpha.A00
	# All values (8x):
	A00 | A04 | A05 | A06 | A07 | A08 | A09 | A10

AntennaLayout
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AntennaLayout.N121
	# Last value:
	value = enums.AntennaLayout.TX2
	# All values (14x):
	N121 | N161 | N21 | N22 | N32 | N41 | N42 | N43
	N44 | N61 | N62 | N81 | N82 | TX2

AntNoPorts
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AntNoPorts.P1
	# All values (3x):
	P1 | P2 | P4

AntNoPortsB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AntNoPortsB.P1
	# All values (4x):
	P1 | P2 | P4 | P8

Aoa
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Aoa.AOA1
	# All values (3x):
	AOA1 | AOA2 | CONDucted

AoaB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AoaB.AOA1
	# All values (5x):
	AOA1 | AOA2 | AOA3 | AOA4 | CONDucted

Asn1SignalMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Asn1SignalMode.B1
	# All values (4x):
	B1 | B2 | B4 | UECap

Assignment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Assignment.NONE
	# All values (8x):
	NONE | SA0 | SA1 | SA2 | SA3 | SA4 | SA5 | SA6

Association
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Association.CSIRs
	# All values (2x):
	CSIRs | SSBBeam

AswitchingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AswitchingType.T1R1
	# All values (6x):
	T1R1 | T1R2 | T1R4 | T2R2 | T2R4 | T4R4

AuthProcedure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AuthProcedure.EAKA
	# All values (2x):
	EAKA | FAKA

AutoMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoMode.AUTO
	# All values (3x):
	AUTO | OFF | ON

BandwidthCommon
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthCommon.BW0
	# All values (8x):
	BW0 | BW1 | BW2 | BW3 | BW4 | BW5 | BW6 | BW7

BandwidthDedicated
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthDedicated.BW0
	# All values (4x):
	BW0 | BW1 | BW2 | BW3

BandwidthHoping
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthHoping.HBW0
	# All values (4x):
	HBW0 | HBW1 | HBW2 | HBW3

BeamConfigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BeamConfigMode.ALL
	# All values (3x):
	ALL | AUTO | UDEFined

BeamNoPorts
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BeamNoPorts.NONE
	# All values (5x):
	NONE | P1 | P2 | P4 | P8

BeamsTrigger
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BeamsTrigger.ACTive
	# All values (5x):
	ACTive | ID0 | ID1 | ID2 | ID3

BlerState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BlerState.FAIL
	# All values (3x):
	FAIL | PASS | PENDing

BurstType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstType.FBURst
	# All values (2x):
	FBURst | RBURst

BwidthTotal
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BwidthTotal.B100
	# Last value:
	value = enums.BwidthTotal.B700
	# All values (13x):
	B100 | B150 | B200 | B250 | B300 | B350 | B400 | B450
	B500 | B550 | B600 | B650 | B700

BwpSwitchingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BwpSwitchingMode.DYNamic
	# All values (2x):
	DYNamic | STATic

BwSelection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BwSelection.ALL
	# All values (2x):
	ALL | RB52

CcrntisEnd
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CcrntisEnd.ASF
	# All values (5x):
	ASF | BLSF | F2SF | LSF | SASF

CellDeployment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CellDeployment.REAL
	# All values (2x):
	REAL | VIRTual

CellPucchFormatPy
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CellPucchFormatPy.F0
	# All values (5x):
	F0 | F1 | F2 | F3 | F4

CellsToMeasure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CellsToMeasure.ALL
	# All values (6x):
	ALL | LAA | LLAA | LTE | NRADio | OFF

CellsTypeToMeasure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CellsTypeToMeasure.CELLs
	# All values (2x):
	CELLs | CGRoup

CellType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CellType.LTE
	# All values (2x):
	LTE | NR

Choice
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Choice.CODebook
	# All values (3x):
	CODebook | NCODebook | SINGle

CipherAlgorithm
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CipherAlgorithm.EA0
	# Last value:
	value = enums.CipherAlgorithm.HIGHest
	# All values (9x):
	EA0 | EA1 | EA2 | EA3 | EA4 | EA5 | EA6 | EA7
	HIGHest

Class
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Class.C0
	# All values (4x):
	C0 | C1 | C2 | C3

CodebookSubset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodebookSubset.AUTO
	# All values (4x):
	AUTO | FPNC | NC | PNC

Coding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Coding.EIGHt
	# All values (3x):
	EIGHt | GSM | UCS2

CodingGroup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingGroup.G7
	# All values (3x):
	G7 | G7L | U2L

ConfigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigMode.AUTO
	# All values (2x):
	AUTO | UDEFined

ConfigType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigType.T1
	# All values (2x):
	T1 | T2

ConfigTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigTypeB.T1
	# All values (3x):
	T1 | T2 | T3

Control
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Control.CLOop
	# All values (5x):
	CLOop | KEEP | MAX | MIN | PATTern

CoreNetwork
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CoreNetwork.EPS
	# All values (2x):
	EPS | FG

Counter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Counter.N1
	# Last value:
	value = enums.Counter.N8
	# All values (9x):
	N1 | N10 | N2 | N20 | N3 | N4 | N5 | N6
	N8

DataFlow
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataFlow.MCG
	# All values (4x):
	MCG | MCGSplit | SCG | SCGSplit

DciFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DciFormat.D0
	# Last value:
	value = enums.DciFormat.D2D
	# All values (10x):
	D0 | D1 | D1A | D1B | D1C | D2 | D2A | D2B
	D2C | D2D

DciFormatB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DciFormatB.D10
	# All values (2x):
	D10 | D11

DciFormatC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DciFormatC.D00
	# All values (2x):
	D00 | D01

DcMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DcMode.ENDC
	# All values (5x):
	ENDC | LTE | NR | NRDC | OFF

DensityPreset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DensityPreset.NPResent
	# All values (2x):
	NPResent | PRESent

DiagBaseband
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DiagBaseband.BBCombining
	# All values (2x):
	BBCombining | MRFPerf

DiagCellSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DiagCellSignal.COMBining
	# All values (4x):
	COMBining | OTA | OTASep | SEParation

DisplayMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayMode.IMMediate
	# All values (2x):
	IMMediate | NORMal

DlIqDataStreams
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DlIqDataStreams.S1
	# All values (4x):
	S1 | S2 | S4 | S8

DlUlBandwidth
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DlUlBandwidth.B005
	# Last value:
	value = enums.DlUlBandwidth.M90
	# All values (30x):
	B005 | B010 | B015 | B020 | B025 | B030 | B040 | B050
	B060 | B070 | B080 | B090 | B100 | B200 | B400 | M10
	M100 | M15 | M20 | M200 | M25 | M30 | M40 | M400
	M5 | M50 | M60 | M70 | M80 | M90

DlUlLocation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DlUlLocation.HIGH
	# All values (4x):
	HIGH | LOW | MID | USER

DuplexModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DuplexModeB.FDD
	# All values (3x):
	FDD | SDL | TDD

EdRxMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EdRxMode.UERequested
	# All values (2x):
	UERequested | USER

EnableCqi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnableCqi.APERiodic
	# All values (4x):
	APERiodic | OFF | PERiodic | SPERsistant

EpreRatio
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EpreRatio.R0
	# All values (2x):
	R0 | R1

EpsRejectCause
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.EpsRejectCause.C002
	# Last value:
	value = enums.EpsRejectCause.C113
	# All values (72x):
	C002 | C003 | C005 | C006 | C007 | C008 | C009 | C010
	C011 | C012 | C013 | C014 | C015 | C016 | C017 | C018
	C019 | C020 | C021 | C022 | C023 | C024 | C025 | C026
	C027 | C028 | C029 | C030 | C031 | C032 | C033 | C034
	C035 | C036 | C037 | C038 | C039 | C040 | C041 | C042
	C043 | C044 | C045 | C046 | C047 | C049 | C050 | C051
	C052 | C053 | C054 | C055 | C056 | C057 | C058 | C059
	C060 | C061 | C065 | C066 | C078 | C081 | C095 | C096
	C097 | C098 | C099 | C100 | C101 | C111 | C112 | C113

EpsRejectProcedure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EpsRejectProcedure.ATTR
	# All values (5x):
	ATTR | BEAR | NOR | PDNR | TAUR

EsmCause
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.EsmCause.C100
	# Last value:
	value = enums.EsmCause.C99
	# All values (45x):
	C100 | C101 | C111 | C112 | C113 | C16 | C26 | C27
	C28 | C29 | C30 | C31 | C32 | C33 | C34 | C35
	C36 | C37 | C38 | C39 | C41 | C42 | C43 | C44
	C45 | C46 | C47 | C49 | C50 | C51 | C52 | C53
	C54 | C55 | C56 | C59 | C60 | C65 | C66 | C81
	C95 | C96 | C97 | C98 | C99

FadingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FadingMode.NORMal
	# All values (2x):
	NORMal | USER

FadingProfile
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FadingProfile.CTES
	# Last value:
	value = enums.FadingProfile.UMIL
	# All values (110x):
	CTES | EP5A | EP5H | EP5L | EP5M | EPAE | EPHE | EPLE
	EPME | ET1A | ET1H | ET1L | ET1M | ET3A | ET3H | ET3L
	ET3M | ET7A | ET7H | ET7L | ET7M | ETA3 | ETAE | ETHA
	ETHE | ETLA | ETLE | ETMA | ETME | EV5A | EV5H | EV5L
	EV5M | EV7A | EV7H | EV7L | EV7M | EVAE | EVHE | EVLE
	EVME | HST | HST2 | HSTS | INHL | INHN | MBSF | NH
	NHD | NHDF | NHDI | NHE | NHG | NHH | NHI | NHS
	NHSF | NHSI | NNAC | NNC5 | NONE | RMAL | RMAN | SMAL
	SMAN | TAAA | TAAM | TAAN | TAHA | TAHM | TAHN | TALA
	TALM | TALN | TAMA | TAMM | TAMN | TBAC | TBHC | TBLC
	TBMC | TCAD | TCAO | TCHD | TCHO | TCLD | TCLO | TCMD
	TCMO | TDAA | TDAM | TDAN | TDHA | TDHM | TDHN | TDLA
	TDLM | TDLN | TDMA | TDMM | TDMN | UMA3 | UMAA | UMAL
	UMAN | UMI1 | UMI2 | UMI3 | UMIA | UMIL

FgsRejectCause
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FgsRejectCause.C003
	# Last value:
	value = enums.FgsRejectCause.C111
	# All values (77x):
	C003 | C005 | C006 | C007 | C008 | C009 | C010 | C011
	C012 | C013 | C015 | C020 | C021 | C022 | C023 | C024
	C026 | C027 | C028 | C029 | C031 | C032 | C033 | C034
	C035 | C036 | C037 | C038 | C039 | C041 | C042 | C043
	C044 | C045 | C046 | C047 | C050 | C051 | C054 | C057
	C058 | C059 | C061 | C062 | C065 | C067 | C068 | C069
	C070 | C071 | C072 | C073 | C074 | C075 | C076 | C077
	C078 | C079 | C080 | C081 | C082 | C083 | C084 | C085
	C086 | C090 | C091 | C092 | C093 | C095 | C096 | C097
	C098 | C099 | C100 | C101 | C111

FgsRejectProcedure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FgsRejectProcedure.AUTR
	# All values (4x):
	AUTR | NOR | PDUR | REGR

FilterCoeff
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FilterCoeff.FC0
	# Last value:
	value = enums.FilterCoeff.FC9
	# All values (15x):
	FC0 | FC1 | FC11 | FC13 | FC15 | FC17 | FC19 | FC2
	FC3 | FC4 | FC5 | FC6 | FC7 | FC8 | FC9

FlowControl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FlowControl.GUARanteed
	# All values (2x):
	GUARanteed | NGUaranteed

FollowCqi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FollowCqi.DISabled
	# All values (5x):
	DISabled | MSB | UEBSubband | UEPSubband | WB

FollowPmi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FollowPmi.DISabled
	# All values (4x):
	DISabled | SB | WB | WBEXplicit

FollowRi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FollowRi.DISabled
	# All values (3x):
	DISabled | ENABled | RETX

FollowType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FollowType.DISabled
	# All values (2x):
	DISabled | ENABled

FormatCqi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormatCqi.SB
	# All values (2x):
	SB | WB

Frame
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Frame.T16
	# All values (5x):
	T16 | T1T | T2 | T4 | T8

FramesOffset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FramesOffset.T16
	# All values (8x):
	T16 | T1T | T2 | T2T | T32 | T4 | T4T | T8

FrequencyRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrequencyRange.FR1
	# All values (2x):
	FR1 | FR2

FtpMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FtpMode.AUTO
	# All values (5x):
	AUTO | FULL | MOD1 | MOD2 | OFF

GroupLanguage
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GroupLanguage.G7L
	# All values (2x):
	G7L | U2L

IdentityType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IdentityType.GCI
	# All values (4x):
	GCI | GLI | IMSI | NAI

IgnorePrachMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IgnorePrachMode.IALLways
	# All values (3x):
	IALLways | IXTimes | RALLways

IndicationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IndicationMode.AUTO
	# All values (4x):
	AUTO | OATime | WAT1 | WAT2

Info
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Info.ALL
	# All values (3x):
	ALL | DL | UL

InitialSfAlloc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InitialSfAlloc.S0
	# All values (2x):
	S0 | S7

IntegrityAlgorithm
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.IntegrityAlgorithm.HIGHest
	# Last value:
	value = enums.IntegrityAlgorithm.IA7
	# All values (9x):
	HIGHest | IA0 | IA1 | IA2 | IA3 | IA4 | IA5 | IA6
	IA7

Ira
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ira.E2
	# All values (4x):
	E2 | E3 | E4 | E8

ItRateUnit
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ItRateUnit.G1
	# Last value:
	value = enums.ItRateUnit.T64
	# All values (25x):
	G1 | G16 | G256 | G4 | G64 | K1 | K16 | K256
	K4 | K64 | M1 | M16 | M256 | M4 | M64 | P1
	P16 | P256 | P4 | P64 | T1 | T16 | T256 | T4
	T64

Ktc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ktc.N2
	# All values (2x):
	N2 | N4

LanguageB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LanguageB.DANish
	# Last value:
	value = enums.LanguageB.UNSPecified
	# All values (16x):
	DANish | DUTCh | ENGLish | FINNish | FRENch | GERMan | GREek | HUNGarian
	ITALian | NORWegian | POLish | PORTuguese | SPANish | SWEDish | TURKish | UNSPecified

Level
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Level.AL1
	# All values (5x):
	AL1 | AL16 | AL2 | AL4 | AL8

LimitStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LimitStatus.APRogress
	# All values (4x):
	APRogress | DPRogress | OFF | ON

Location
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Location.HIGH
	# All values (3x):
	HIGH | LOW | MID

LogFileState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogFileState.AVAilable
	# All values (3x):
	AVAilable | CREating | NERunning

LogLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogLevel.BRIef
	# All values (3x):
	BRIef | NONE | VERBose

LogType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogType.DISable
	# All values (4x):
	DISable | FULL | HEADer | PAYLoad

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

LteMimoScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LteMimoScheme.M2N
	# All values (4x):
	M2N | M4N | S1N | UDEFined

Mapping
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Mapping.A
	# All values (2x):
	A | B

MappingI
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MappingI.INT
	# All values (2x):
	INT | NINT

MaxLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MaxLength.L1
	# All values (2x):
	L1 | L2

MaxPorts
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MaxPorts.N1
	# All values (2x):
	N1 | N2

McsBehavior
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsBehavior.AUTO
	# All values (4x):
	AUTO | REPeat | REPLace | SUBStitute

McsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsMode.FIXed
	# All values (3x):
	FIXed | MAX | MMO

McsTable
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsTable.Q1K
	# All values (3x):
	Q1K | Q256 | Q64

McsTableB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsTableB.L64
	# All values (3x):
	L64 | Q256 | Q64

McsTableC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsTableC.AUTO
	# All values (3x):
	AUTO | P521 | UDEFined

McsTableD
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsTableD.Q16
	# All values (3x):
	Q16 | Q256 | Q64

Mimo
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Mimo.M22
	# All values (4x):
	M22 | M33 | M44 | SISO

MimoB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MimoB.M22
	# All values (2x):
	M22 | SISO

Mode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Mode.BINDex
	# All values (3x):
	BINDex | CSIRs | SSBBeam

ModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeB.AUTO
	# All values (2x):
	AUTO | USER

ModeBfollow
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeBfollow.AUTO
	# All values (3x):
	AUTO | BLOCk | OFF

ModeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeC.AUTO
	# All values (3x):
	AUTO | NOTC | USER

ModeD
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeD.MAX
	# All values (3x):
	MAX | MIN | UDEFined

ModeE
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ModeE.CPRI
	# Last value:
	value = enums.ModeE.UDEFined
	# All values (9x):
	CPRI | CQI | CRI | FIXed | PMI | PRI | RI | SPS
	UDEFined

ModeFrecovery
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeFrecovery.AUTO
	# All values (3x):
	AUTO | OFF | UDEFined

ModeFrecoveryB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeFrecoveryB.HADamard
	# All values (5x):
	HADamard | IDENtity | OFF | TGPP | UDEFined

ModeRvs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeRvs.AUTO
	# All values (3x):
	AUTO | S101 | UDEFined

ModeS
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeS.FIXed
	# All values (4x):
	FIXed | SPS | SRBSr | UDEFined

ModeSrs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeSrs.A508
	# All values (4x):
	A508 | A521 | OFF | UDEFined

ModeTrs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeTrs.DEF
	# All values (3x):
	DEF | OFF | UDEF

ModeUeCapability
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeUeCapability.AUTO
	# All values (3x):
	AUTO | SKIP | UDEFined

ModeUeScheduling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModeUeScheduling.BO
	# All values (7x):
	BO | CPRI | CQI | FIXed | PRI | SPS | UDEFined

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation.BPSK
	# All values (7x):
	BPSK | P2BPsk | Q1024 | Q16 | Q256 | Q64 | QPSK

ModulationB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationB.BPSK
	# All values (7x):
	BPSK | P2BPsk | Q16 | Q1K | Q256 | Q64 | QPSK

ModulationOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationOrder.Q16
	# All values (5x):
	Q16 | Q1K | Q256 | Q64 | QPSK

ModulationRetr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationRetr.AUTO
	# All values (8x):
	AUTO | BPSK | HPBP | Q16 | Q1K | Q256 | Q64 | QPSK

MtxPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MtxPosition.P0
	# All values (4x):
	P0 | P1 | P2 | P3

NameType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NameType.GUI
	# All values (2x):
	GUI | RESource

NcellsToMeasure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NcellsToMeasure.ALL
	# All values (5x):
	ALL | IAFRequency | IFRequency | IRAT | OFF

NcellType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NcellType.IAFRequency
	# All values (3x):
	IAFRequency | IFRequency | IRAT

NcoherentTpmi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NcoherentTpmi.FPARtial
	# All values (2x):
	FPARtial | NCOHerent

NeighborCellType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NeighborCellType.CNETwork
	# All values (3x):
	CNETwork | NCList | SIB

NoSymbols
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoSymbols.S1
	# All values (4x):
	S1 | S2 | S3 | S4

NoSymbolsN
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoSymbolsN.N1
	# All values (3x):
	N1 | N2 | N4

OfdmSymbols
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OfdmSymbols.ALL
	# All values (7x):
	ALL | S10 | S11 | S12 | S3 | S6 | S9

OnDurationTimer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OnDurationTimer.M1
	# Last value:
	value = enums.OnDurationTimer.M9D
	# All values (55x):
	M1 | M10 | M100 | M10D | M11D | M12D | M13D | M14D
	M15D | M16D | M17D | M18D | M19D | M1D | M1K0 | M1K2
	M1K6 | M2 | M20 | M200 | M20D | M21D | M22D | M23D
	M24D | M25D | M26D | M27D | M28D | M29D | M2D | M3
	M30 | M300 | M30D | M31D | M3D | M4 | M40 | M400
	M4D | M5 | M50 | M500 | M5D | M6 | M60 | M600
	M6D | M7D | M8 | M80 | M800 | M8D | M9D

PagingCycle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PagingCycle.P128
	# All values (4x):
	P128 | P256 | P32 | P64

PannelType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PannelType.MULTi
	# All values (2x):
	MULTi | SINGle

Pattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Pattern.D1
	# All values (4x):
	D1 | KEEP | U1 | U3

PcellNr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PcellNr.B050
	# All values (4x):
	B050 | B100 | B200 | B400

PdcchFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PdcchFormat.N1
	# All values (5x):
	N1 | N2 | N4 | N8 | NAV

PdcchFormatB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PdcchFormatB.N1
	# All values (4x):
	N1 | N2 | N4 | N8

PduState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PduState.ACTive
	# All values (6x):
	ACTive | AIP | AUIP | DIP | INACtive | MIP

Periodicity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Periodicity.P0P5
	# Last value:
	value = enums.Periodicity.P5
	# All values (10x):
	P0P5 | P0P6 | P1 | P10 | P1P2 | P2 | P2P5 | P3
	P4 | P5

PeriodicityB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeriodicityB.P10
	# All values (6x):
	P10 | P160 | P20 | P40 | P5 | P80

PeriodicityCqiReport
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PeriodicityCqiReport.P10
	# Last value:
	value = enums.PeriodicityCqiReport.UDEFined
	# All values (11x):
	P10 | P16 | P160 | P20 | P320 | P4 | P40 | P5
	P8 | P80 | UDEFined

PeriodicityRsrc
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PeriodicityRsrc.P10
	# Last value:
	value = enums.PeriodicityRsrc.P80
	# All values (13x):
	P10 | P16 | P160 | P20 | P32 | P320 | P4 | P40
	P5 | P64 | P640 | P8 | P80

Ports
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ports.P1
	# All values (8x):
	P1 | P12 | P16 | P2 | P24 | P32 | P4 | P8

Power
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Power.P100
	# Last value:
	value = enums.Power.P98
	# All values (16x):
	P100 | P102 | P104 | P106 | P108 | P110 | P112 | P114
	P116 | P118 | P120 | P90 | P92 | P94 | P96 | P98

PowerScaling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerScaling.TGPP
	# All values (2x):
	TGPP | TOPTimized

PowerStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerStatus.IRANge
	# All values (4x):
	IRANge | ODRiven | OFF | UDRiven

Predefined3Gpp
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Predefined3Gpp.M1
	# Last value:
	value = enums.Predefined3Gpp.M9
	# All values (38x):
	M1 | M10 | M11 | M11A | M11B | M12 | M12A | M12B
	M13 | M14 | M15 | M16 | M17 | M18 | M19 | M1A
	M1B | M2 | M20 | M21 | M22 | M23 | M24 | M25
	M26 | M27 | M28 | M29 | M2A | M3 | M3A | M4
	M4A | M5 | M6 | M7 | M8 | M9

PreferredNetw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PreferredNetw.AUTO
	# All values (4x):
	AUTO | EPS | FG | NONE

ProhibitTimer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ProhibitTimer.INF
	# Last value:
	value = enums.ProhibitTimer.S90
	# All values (24x):
	INF | S0 | S0D4 | S0D5 | S0D8 | S1 | S10 | S12
	S120 | S1D6 | S2 | S20 | S3 | S30 | S300 | S4
	S5 | S6 | S60 | S600 | S7 | S8 | S9 | S90

Prtype
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Prtype.OFF
	# All values (3x):
	OFF | PRTA | PRTB

PsOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PsOrder.RROBin
	# All values (2x):
	RROBin | SBOund

PtrsPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PtrsPower.P00
	# All values (2x):
	P00 | P01

PwrRampingStepA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrRampingStepA.S0
	# All values (4x):
	S0 | S2 | S4 | S6

PwrRampingStepB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrRampingStepB.S0
	# All values (4x):
	S0 | S2 | S3 | S4

Qi
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Qi.Q1
	# Last value:
	value = enums.Qi.Q9
	# All values (21x):
	Q1 | Q2 | Q3 | Q4 | Q5 | Q6 | Q65 | Q66
	Q67 | Q69 | Q7 | Q70 | Q75 | Q79 | Q8 | Q80
	Q82 | Q83 | Q84 | Q85 | Q9

Quantity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Quantity.OFF
	# Last value:
	value = enums.Quantity.Q9
	# All values (10x):
	OFF | Q1 | Q2 | Q3 | Q4 | Q5 | Q6 | Q7
	Q8 | Q9

RangeChoice
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RangeChoice.HASYmmetric
	# All values (5x):
	HASYmmetric | HIGH | LOW | MID | UDEFined

RedCapId
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RedCapId.MSG3
	# All values (4x):
	MSG3 | PRACh | UECap | UNSPecified

RegState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RegState.DREG
	# Last value:
	value = enums.RegState.RIP
	# All values (9x):
	DREG | DRIP | FREG | FRIP | NFReg | NREG | NRIP | REG
	RIP

RegStateB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RegStateB.CREG
	# Last value:
	value = enums.RegStateB.REG
	# All values (9x):
	CREG | CRIP | DREG | DRIP | EREG | ERIP | LREG | LRIP
	REG

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

Repetitions
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repetitions.N12
	# All values (8x):
	N12 | N16 | N2 | N3 | N4 | N7 | N8 | OFF

ReportCqi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReportCqi.OFF
	# All values (3x):
	OFF | SB | WB

ReportInterval
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ReportInterval.I1
	# Last value:
	value = enums.ReportInterval.I9
	# All values (14x):
	I1 | I10 | I11 | I12 | I13 | I14 | I2 | I3
	I4 | I5 | I6 | I7 | I8 | I9

ReportMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReportMode.S1
	# All values (2x):
	S1 | S2

ReportType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReportType.APERiodic
	# All values (3x):
	APERiodic | OFF | PERiodic

ResourceAllocationType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceAllocationType.DSWich
	# All values (3x):
	DSWich | T0 | T1

ResourceId
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceId.R1
	# All values (2x):
	R1 | R2

ResourceOffset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceOffset.NPResent
	# All values (4x):
	NPResent | OF01 | OF10 | OF11

ReTxBehavior
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReTxBehavior.FLUSh
	# All values (3x):
	FLUSh | NAPPlicable | RETain

ReTxBehaviorB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReTxBehaviorB.CONTinue
	# All values (4x):
	CONTinue | SDTX | SNDMimo | STOP

RgbSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RgbSize.CON1
	# All values (2x):
	CON1 | CON2

Riv
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Riv.NADaptive
	# All values (2x):
	NADaptive | NEW

RlcMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RlcMode.ACK
	# All values (4x):
	ACK | UACK | UADL | UAUL

RnauTimer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RnauTimer.M10
	# Last value:
	value = enums.RnauTimer.OFF
	# All values (9x):
	M10 | M120 | M20 | M30 | M360 | M5 | M60 | M720
	OFF

Routing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Routing.DUT
	# All values (2x):
	DUT | FIXed

RpPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RpPattern.A
	# All values (3x):
	A | B | C

RrcState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RrcState.CONNected
	# All values (3x):
	CONNected | IDLE | INACtive

RsrcPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RsrcPower.M3DB
	# All values (6x):
	M3DB | M6DB | M9DB | P3DB | P6DB | ZERO

Schema
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Schema.CODebook
	# All values (2x):
	CODebook | NCODebook

SecurityAlgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SecurityAlgorithm.AES
	# All values (4x):
	AES | OFF | SNOW | ZUC

SecurityAlgorithmB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SecurityAlgorithmB.EEA0
	# Last value:
	value = enums.SecurityAlgorithmB.HIGHest
	# All values (9x):
	EEA0 | EEA1 | EEA2 | EEA3 | EEA4 | EEA5 | EEA6 | EEA7
	HIGHest

SecurityAlgorithmC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SecurityAlgorithmC.EIA0
	# Last value:
	value = enums.SecurityAlgorithmC.HIGHest
	# All values (9x):
	EIA0 | EIA1 | EIA2 | EIA3 | EIA4 | EIA5 | EIA6 | EIA7
	HIGHest

Severity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Severity.ERRor
	# All values (3x):
	ERRor | INFO | WARNing

SpecialPattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SpecialPattern.P0
	# Last value:
	value = enums.SpecialPattern.PAV2
	# All values (12x):
	P0 | P1 | P2 | P3 | P4 | P5 | P6 | P7
	P8 | P9 | PAV1 | PAV2

Spreset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Spreset.S1
	# All values (3x):
	S1 | S2 | S3

SpsPadding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpsPadding.ALLZero
	# All values (2x):
	ALLZero | NOPadding

SpsPeriodicity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SpsPeriodicity.S1
	# Last value:
	value = enums.SpsPeriodicity.SYM7
	# All values (25x):
	S1 | S10 | S128 | S16 | S160 | S1K | S1K2 | S2
	S20 | S256 | S2K | S32 | S320 | S4 | S40 | S5
	S512 | S5K | S64 | S640 | S8 | S80 | SYM2 | SYM6
	SYM7

SpsPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpsPosition.POS0
	# All values (4x):
	POS0 | POS1 | POS2 | POS3

SrcType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SrcType.PUCC
	# All values (3x):
	PUCC | PUPU | PUSC

State
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.State.OFF
	# All values (3x):
	OFF | RDY | RUN

StateCnetwork
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.StateCnetwork.CREating
	# Last value:
	value = enums.StateCnetwork.TESTing
	# All values (10x):
	CREating | DELeting | ERRor | EXHausted | IDLE | NAV | RUNNing | STARting
	STOPping | TESTing

StatePwrControl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StatePwrControl.RDY
	# All values (2x):
	RDY | RUN

StateTest
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StateTest.ERRor
	# All values (2x):
	ERRor | SUCCess

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.CONFidence
	# All values (3x):
	CONFidence | SAMPles | TIME

SubCarrSpacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubCarrSpacing.A15
	# All values (5x):
	A15 | B30 | C30 | D120 | E240

Subframe
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Subframe.SC0
	# Last value:
	value = enums.Subframe.SC9
	# All values (16x):
	SC0 | SC1 | SC10 | SC11 | SC12 | SC13 | SC14 | SC15
	SC2 | SC3 | SC4 | SC5 | SC6 | SC7 | SC8 | SC9

SymbolPair
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SymbolPair.S04
	# Last value:
	value = enums.SymbolPair.S913
	# All values (10x):
	S04 | S15 | S26 | S37 | S48 | S59 | S610 | S711
	S812 | S913

TadvPeriodicity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TadvPeriodicity.CONTinuous
	# All values (3x):
	CONTinuous | OFF | SINGleshot

Target
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Target.ALL
	# All values (5x):
	ALL | CELL | LTE | NRADio | TOPology

TargetCellScg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetCellScg.RELease
	# All values (1x):
	RELease

Tdd
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Tdd.CP1
	# All values (3x):
	CP1 | CP2 | SEParation

TdType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TdType.APERiodic
	# All values (3x):
	APERiodic | PERiodic | PERSistent

TestFunction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestFunction.RX
	# All values (3x):
	RX | RXTX | TX

TestLoopState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestLoopState.CLOSe
	# All values (2x):
	CLOSe | OPEN

TimeOffset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeOffset.T0
	# All values (7x):
	T0 | T10 | T15 | T20 | T40 | T5 | T80

TimerUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimerUnit.DEACtivated
	# All values (8x):
	DEACtivated | H1 | H10 | H320 | M1 | M10 | S2 | S30

TimerUnitB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimerUnitB.DEACtivated
	# All values (4x):
	DEACtivated | M1 | M6 | S2

Tmode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Tmode.TM1
	# Last value:
	value = enums.Tmode.TM9
	# All values (10x):
	TM1 | TM10 | TM2 | TM3 | TM4 | TM5 | TM6 | TM7
	TM8 | TM9

TpcDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TpcDirection.ALTernating
	# All values (3x):
	ALTernating | DOWN | UP

TpControl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TpControl.CLOop
	# All values (6x):
	CLOop | KEEP | MAX | MIN | PATTern | RPTolerance

Tpmi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Tpmi.T0
	# All values (6x):
	T0 | T1 | T2 | T3 | T4 | T5

TpTimeDens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TpTimeDens.D2
	# All values (2x):
	D2 | NPResent

TrsPeriodicity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrsPeriodicity.P10
	# All values (4x):
	P10 | P20 | P40 | P80

TxRxSeparation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxRxSeparation.DEFault
	# All values (2x):
	DEFault | UDEFined

Type
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Type.DCMC
	# All values (2x):
	DCMC | GDC

TypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TypeB.UDEFined
	# All values (1x):
	UDEFined

TypeDlUl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TypeDlUl.RMC
	# All values (2x):
	RMC | UDEFined

UecState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UecState.CESTablish
	# All values (7x):
	CESTablish | CREestablish | CRELease | HANDover | OK | PAGing | SCGFailure

UeScFactor
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeScFactor.N2
	# All values (4x):
	N2 | N4 | N8 | OFF

UeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeType.NORMal
	# All values (2x):
	NORMal | RCAP

UlBandwidth
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.UlBandwidth.B014
	# Last value:
	value = enums.UlBandwidth.M5
	# All values (12x):
	B014 | B030 | B050 | B100 | B150 | B200 | M10 | M15
	M1K4 | M20 | M3 | M5

UlEnable
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UlEnable.OFF
	# All values (3x):
	OFF | ON | SRS

UlIndication
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UlIndication.AOFF
	# All values (3x):
	AOFF | AON | AUTO

UlMaxDutyCyle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UlMaxDutyCyle.D80
	# All values (7x):
	D80 | D82 | D85 | D87 | D89 | OFF | ON

VcCalibQuantity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VcCalibQuantity.CRITical
	# All values (3x):
	CRITical | GOOD | INSufficient

Version
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Version.AUTO
	# All values (5x):
	AUTO | RV0 | RV1 | RV2 | RV3

VoiceHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoiceHandling.EFHandover
	# All values (4x):
	EFHandover | EFRedirect | UECap | VONR

Waveform
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Waveform.CP
	# All values (2x):
	CP | DTFS

WusMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WusMode.RATio
	# All values (2x):
	RATio | UDEFined

