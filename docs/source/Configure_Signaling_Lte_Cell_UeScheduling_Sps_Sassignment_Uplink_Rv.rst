Rv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:RV

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:SPS:SASSignment:UL:RV



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Sps.Sassignment.Uplink.Rv.RvCls
	:members:
	:undoc-members:
	:noindex: