Cfrequency
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Downlink.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.rfSettings.downlink.cfrequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_RfSettings_Downlink_Cfrequency_Arfcn.rst
	Configure_Signaling_Nradio_Cell_RfSettings_Downlink_Cfrequency_Frequency.rst