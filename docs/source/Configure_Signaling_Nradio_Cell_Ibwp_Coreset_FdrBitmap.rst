FdrBitmap
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:FDRBitmap

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:IBWP:COReset:FDRBitmap



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ibwp.Coreset.FdrBitmap.FdrBitmapCls
	:members:
	:undoc-members:
	:noindex: