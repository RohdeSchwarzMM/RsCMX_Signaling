Target
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BWP:TARGet

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BWP:TARGet



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Bwp.Target.TargetCls
	:members:
	:undoc-members:
	:noindex: