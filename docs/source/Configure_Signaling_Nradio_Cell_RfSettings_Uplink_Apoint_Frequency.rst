Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:APOint:FREQuency

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RFSettings:UL:APOint:FREQuency



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.RfSettings.Uplink.Apoint.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: