Awgn
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Awgn.AwgnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.awgn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Awgn_Advanced.rst
	Configure_Signaling_Awgn_Enable.rst
	Configure_Signaling_Awgn_SnRatio.rst