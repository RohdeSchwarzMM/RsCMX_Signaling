Pdu
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SIGNaling:TOPology:FGS:UE:PDU

.. code-block:: python

	CATalog:SIGNaling:TOPology:FGS:UE:PDU



.. autoclass:: RsCMX_Signaling.Implementations.Catalog.Signaling.Topology.Fgs.Ue.Pdu.PduCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.signaling.topology.fgs.ue.pdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Signaling_Topology_Fgs_Ue_Pdu_QosFlow.rst