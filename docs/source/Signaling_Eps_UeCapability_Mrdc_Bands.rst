Bands
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SIGNaling:EPS:UECapability:MRDC:BANDs

.. code-block:: python

	DELete:SIGNaling:EPS:UECapability:MRDC:BANDs



.. autoclass:: RsCMX_Signaling.Implementations.Signaling.Eps.UeCapability.Mrdc.Bands.BandsCls
	:members:
	:undoc-members:
	:noindex: