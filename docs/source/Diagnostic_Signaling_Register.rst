Register
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Register.RegisterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.signaling.register.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Signaling_Register_Existing.rst