Papr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTB:PAPR

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:DMRS:DL:MTB:PAPR



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.Mtb.Papr.PaprCls
	:members:
	:undoc-members:
	:noindex: