Downlink
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Dmrs.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.nradio.cell.dmrs.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Nradio_Cell_Dmrs_Downlink_Mta.rst
	Configure_Signaling_Nradio_Cell_Dmrs_Downlink_Mtb.rst
	Configure_Signaling_Nradio_Cell_Dmrs_Downlink_Ptrs.rst