Periodicity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:TADVance:PERiodicity

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:TADVance:PERiodicity



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Tadvance.Periodicity.PeriodicityCls
	:members:
	:undoc-members:
	:noindex: