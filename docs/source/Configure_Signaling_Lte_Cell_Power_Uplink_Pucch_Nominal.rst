Nominal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:POWer:UL:PUCCh:NOMinal

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:POWer:UL:PUCCh:NOMinal



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.Power.Uplink.Pucch.Nominal.NominalCls
	:members:
	:undoc-members:
	:noindex: