Rcap
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:IBWP:RCAP

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:IBWP:RCAP



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Ibwp.Rcap.RcapCls
	:members:
	:undoc-members:
	:noindex: