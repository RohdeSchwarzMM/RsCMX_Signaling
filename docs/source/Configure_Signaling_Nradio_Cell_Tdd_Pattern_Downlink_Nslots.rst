Nslots
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:DL:NSLots

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:TDD:PATTern<PatternNo>:DL:NSLots



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Tdd.Pattern.Downlink.Nslots.NslotsCls
	:members:
	:undoc-members:
	:noindex: