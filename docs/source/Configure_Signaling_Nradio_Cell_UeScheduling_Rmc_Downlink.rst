Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:RMC:DL

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:RMC:DL



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Rmc.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: