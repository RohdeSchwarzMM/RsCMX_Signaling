Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:CSIRs:MCS

.. code-block:: python

	[CONFigure]:SIGNaling:LTE:CELL:UESCheduling:CMMapping:CSIRs:MCS



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.CmMapping.Csirs.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: