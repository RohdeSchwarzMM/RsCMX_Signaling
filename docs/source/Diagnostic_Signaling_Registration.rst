Registration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:SIGNaling:REGistration:RESet

.. code-block:: python

	DIAGnostic:SIGNaling:REGistration:RESet



.. autoclass:: RsCMX_Signaling.Implementations.Diagnostic.Signaling.Registration.RegistrationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.signaling.registration.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Signaling_Registration_Add.rst
	Diagnostic_Signaling_Registration_ListPy.rst