Ap3Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:BEAMs:AP3Trigger

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:BEAMs:AP3Trigger



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Beams.Ap3Trigger.Ap3TriggerCls
	:members:
	:undoc-members:
	:noindex: