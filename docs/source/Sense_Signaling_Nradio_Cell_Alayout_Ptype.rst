Ptype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SIGNaling:NRADio:CELL:ALAYout:PTYPe

.. code-block:: python

	SENSe:SIGNaling:NRADio:CELL:ALAYout:PTYPe



.. autoclass:: RsCMX_Signaling.Implementations.Sense.Signaling.Nradio.Cell.Alayout.Ptype.PtypeCls
	:members:
	:undoc-members:
	:noindex: