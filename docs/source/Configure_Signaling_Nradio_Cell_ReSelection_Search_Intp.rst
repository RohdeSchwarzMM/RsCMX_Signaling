Intp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:RESelection:SEARch:INTP

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:RESelection:SEARch:INTP



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.ReSelection.Search.Intp.IntpCls
	:members:
	:undoc-members:
	:noindex: