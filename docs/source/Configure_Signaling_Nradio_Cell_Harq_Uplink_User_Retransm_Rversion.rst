Rversion
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm:RVERsion

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:HARQ:UL:USER:RETRansm:RVERsion



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Harq.Uplink.User.Retransm.Rversion.RversionCls
	:members:
	:undoc-members:
	:noindex: