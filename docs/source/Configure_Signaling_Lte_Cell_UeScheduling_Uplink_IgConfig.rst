IgConfig
----------------------------------------





.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Lte.Cell.UeScheduling.Uplink.IgConfig.IgConfigCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.signaling.lte.cell.ueScheduling.uplink.igConfig.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_IgConfig_Mcs.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_IgConfig_Rb.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_IgConfig_SrcIndex.rst
	Configure_Signaling_Lte_Cell_UeScheduling_Uplink_IgConfig_SrprIndex.rst