HrTimer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:CDRX:DL:HRTimer

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:CDRX:DL:HRTimer



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.Cdrx.Downlink.HrTimer.HrTimerCls
	:members:
	:undoc-members:
	:noindex: