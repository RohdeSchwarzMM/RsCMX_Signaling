Smode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SMODe

.. code-block:: python

	[CONFigure]:SIGNaling:NRADio:CELL:UESCheduling:SMODe



.. autoclass:: RsCMX_Signaling.Implementations.Configure.Signaling.Nradio.Cell.UeScheduling.Smode.SmodeCls
	:members:
	:undoc-members:
	:noindex: