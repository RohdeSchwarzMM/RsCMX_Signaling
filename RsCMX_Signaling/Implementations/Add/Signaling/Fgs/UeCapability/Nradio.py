from typing import List

from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class NradioCls:
	"""Nradio commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("nradio", core, parent)

	def set_bands(self, fbi: List[int]) -> None:
		"""SCPI: ADD:SIGNaling:FGS:UECapability:NRADio:BANDs \n
		Snippet: driver.add.signaling.fgs.ueCapability.nradio.set_bands(fbi = [1, 2, 3]) \n
		Adds entries to the list of requested frequency bands for the container type 'UE-NR-Capability', for 5GS tracking areas. \n
			:param fbi: Comma-separated list of NR frequency band indicators
		"""
		param = Conversions.list_to_csv_str(fbi)
		self._core.io.write(f'ADD:SIGNaling:FGS:UECapability:NRADio:BANDs {param}')
