from typing import List

from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal.Types import DataType
from ......Internal.StructBase import StructBase
from ......Internal.ArgStruct import ArgStruct
from ...... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ThroughputCls:
	"""Throughput commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("throughput", core, parent)

	# noinspection PyTypeChecker
	class FetchStruct(StructBase):
		"""Response structure. Fields: \n
			- Reliability: int: See 'Reliability indicator'
			- Cell_Name: List[str]: Name of the cell providing the measured connection
			- Rel_Avg: List[float]: Average throughput as percentage of scheduled throughput
			- Abs_Ack: List[int]: Average throughput in bit/s
			- Abs_Scheduled: List[int]: Scheduled throughput in bit/s"""
		__meta_args_list = [
			ArgStruct.scalar_int('Reliability', 'Reliability'),
			ArgStruct('Cell_Name', DataType.StringList, None, False, True, 1),
			ArgStruct('Rel_Avg', DataType.FloatList, None, False, True, 1),
			ArgStruct('Abs_Ack', DataType.IntegerList, None, False, True, 1),
			ArgStruct('Abs_Scheduled', DataType.IntegerList, None, False, True, 1)]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Reliability: int = None
			self.Cell_Name: List[str] = None
			self.Rel_Avg: List[float] = None
			self.Abs_Ack: List[int] = None
			self.Abs_Scheduled: List[int] = None

	def fetch(self, cword=repcap.Cword.Default) -> FetchStruct:
		"""SCPI: FETCh:SIGNaling:MEASurement:BLER:CWORd<no>:THRoughput \n
		Snippet: value: FetchStruct = driver.signaling.measurement.bler.cword.throughput.fetch(cword = repcap.Cword.Default) \n
		Returns the DL throughput results of the BLER measurement, for code word <no>. There is one set of results {...
		} per cell: <Reliability>, {<CellName>, <RelAvg>, <AbsAck>, <AbsScheduled>}, {...}, ... \n
			:param cword: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Cword')
			:return: structure: for return value, see the help for FetchStruct structure arguments."""
		cword_cmd_val = self._cmd_group.get_repcap_cmd_value(cword, repcap.Cword)
		return self._core.io.query_struct(f'FETCh:SIGNaling:MEASurement:BLER:CWORd{cword_cmd_val}:THRoughput?', self.__class__.FetchStruct())
