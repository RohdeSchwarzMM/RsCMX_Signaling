from ...........Internal.Core import Core
from ...........Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TdomainCls:
	"""Tdomain commands group definition. 4 total commands, 4 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("tdomain", core, parent)

	@property
	def symbol(self):
		"""symbol commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_symbol'):
			from .Symbol import SymbolCls
			self._symbol = SymbolCls(self._core, self._cmd_group)
		return self._symbol

	@property
	def chmapping(self):
		"""chmapping commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_chmapping'):
			from .Chmapping import ChmappingCls
			self._chmapping = ChmappingCls(self._core, self._cmd_group)
		return self._chmapping

	@property
	def soffset(self):
		"""soffset commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_soffset'):
			from .Soffset import SoffsetCls
			self._soffset = SoffsetCls(self._core, self._cmd_group)
		return self._soffset

	@property
	def ansOffset(self):
		"""ansOffset commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_ansOffset'):
			from .AnsOffset import AnsOffsetCls
			self._ansOffset = AnsOffsetCls(self._core, self._cmd_group)
		return self._ansOffset

	def clone(self) -> 'TdomainCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = TdomainCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
